**A copy from https://gitlab.cern.ch/recast-atlas/exotics/ana-exot-2018-046/-/tree/reana_working**

To run this example on the CERN REANA instance, put the following to a `submit.sh` and source the script.
You should expect the folder structure
```
.
├── ana-exot-2018-046-mine
├── submit.sh
├── usercert.pem
└── userkey.pem
```

```
source /afs/cern.ch/user/r/reana/public/reana/bin/activate
job_name=myjob
echo "this is job name:" $job_name
example=ana-exot-2018-046-mine
git clone https://gitlab.cern.ch/zhangruihpc/reana-ana-exot-2018-046.git ${example}
cd ${example}

# connect to some REANA cloud instance
export REANA_SERVER_URL=https://reana.cern.ch/
export REANA_ACCESS_TOKEN=<your_reana_token>
# create new workflow
reana-client create -w $job_name
export REANA_WORKON=$job_name

# http://docs.reana.io/advanced-usage/access-control/voms-proxy/
echo "setup proxy"
reana-client secrets-add --overwrite --file ../userkey.pem --file ../usercert.pem --env VOMSPROXY_PASS=<your_base64_pass> --env VONAME=atlas

reana-client upload
reana-client start

# then go to reana.cern.ch to check your workflow status
```
Note:
- Put your reana token you get when applying your account to <your_reana_token>
- follow http://docs.reana.io/advanced-usage/access-control/voms-proxy/ to create `userkey.pem` and `usercert.pem` and place them next to `ana-exot-2018-046-mine` as indicated above.
- follow the same link to generate `VOMSPROXY_PASS` and replace `<your_base64_pass>` in the script.

# Current status
In https://reana.cern.ch/, you will see two steps: `rucio_download_mc16a` and `selection_stage_mc16a`.
Step 1 will download two files from rucio that were generated upstream using `pchain` and place them in a common workarea.
Step 2 will access them and run the real MonoHbb analysis code.
From the log, you can see that these two files are available at
```
/var/reana/users/<user_id_hash>/worklows/<workflow_id_hash>/rucio_download_mc16a/rucio_dr_mc16a/user.zhangr.28527328._000001.DAOD_EXOT27.pool.root.1`
/var/reana/users/<user_id_hash>/worklows/<workflow_id_hash>/rucio_download_mc16a/rucio_pr_mc16a/`
```
However, you will see see an error:
```
9b5e3/workflows/d992a0a9-1bbb-4df7-a908-6cc1500e60e3/selection_stage_mc16a/recast_signal_ntuple_mc16a.root
Signal is already included in process list
Running over -1 events for input /var/reana/users/1dc3a29d-b8d6-4b90-94ba-53cda3f9b5e3/workflows/d992a0a9-1bbb-4df7-a908-6cc1500e60e3/rucio_download_mc16a/rucio_dr_mc16a/user.zhangr.28527328._000001.DAOD_EXOT27.pool.root.1.
python /xampp/XAMPPmonoH/XAMPPmonoH/scripts/../../XAMPPmonoH/python/runAthena.py --inFile /var/reana/users/1dc3a29d-b8d6-4b90-94ba-53cda3f9b5e3/workflows/d992a0a9-1bbb-4df7-a908-6cc1500e60e3/rucio_download_mc16a/rucio_dr_mc16a/user.zhangr.28527328._000001.DAOD_EXOT27.pool.root.1 --evtMax -1 --outFile /var/reana/users/1dc3a29d-b8d6-4b90-94ba-53cda3f9b5e3/workflows/d992a0a9-1bbb-4df7-a908-6cc1500e60e3/selection_stage_mc16a/recast_signal_ntuple_mc16a.root --jobOptions /xampp/XAMPPmonoH/XAMPPmonoH/scripts/../../XAMPPmonoH/share/runMonoH.py --prwConfigFile /var/reana/users/1dc3a29d-b8d6-4b90-94ba-53cda3f9b5e3/workflows/d992a0a9-1bbb-4df7-a908-6cc1500e60e3/rucio_download_mc16a/rucio_pr_mc16a
INFO : Could not find the environment variable 'SGE_BASEFOLDER'. This variable defines the directory of your output & logs.
INFO : Will set its default value to /ptmp/mpp/none/Cluster/
Sat Apr 16 23:42:28 CEST 2022
Preloading tcmalloc_minimal.so
Py:Athena            INFO including file "AthenaCommon/Preparation.py"
Py:Athena            INFO including file "AthenaCommon/Bootstrap.py"
Py:Athena            INFO including file "AthenaCommon/Atlas.UnixStandardJob.py"
Py:Athena            INFO executing ROOT6Setup
Py:Athena            INFO including file "AthenaCommon/Execution.py"
Py:Athena            INFO including file "/xampp/XAMPPmonoH/XAMPPmonoH/scripts/../../XAMPPmonoH/runMonoH.py"
Py:Athena            INFO including file "XAMPPmonoH/MonoHToolSetup.py"
Py:Athena            INFO including file "XAMPPbase/BaseToolSetup.py"
Py:Athena            INFO including file "XAMPPbase/CPToolSetup.py"
Py:XAMPP I/O         INFO Detected the following athena args:
Py:XAMPP I/O         INFO Namespace(STConfig='', analysis='MyCutFlow', dsidBugFix=None, jobOptions='XAMPPbase/runXAMPPbase.py', noSyst=False, outFile='/var/reana/users/1dc3a29d-b8d6-4b90-94ba-53cda3f9b5e3/workflows/d992a0a9-1bbb-4df7-a908-6cc1500e60e3/selection_stage_mc16a/recast_signal_ntuple_mc16a.root', parseFilesForPRW=False, prwConfigFile='/var/reana/users/1dc3a29d-b8d6-4b90-94ba-53cda3f9b5e3/workflows/d992a0a9-1bbb-4df7-a908-6cc1500e60e3/rucio_download_mc16a/rucio_pr_mc16a', remaining_scripts=[], testJob=False, valgrind='')
Py:XAMPP I/O         INFO ****************** STARTING the job *****************
Py:ConfigurableDb    INFO Read module info for 838 configurables from 8 genConfDb files
Py:ConfigurableDb    INFO No duplicates have been found: that's good !
Py:XAMPP I/O         INFO Set the access mode to 0. The access mode determines how athena reads out the xAOD.
Py:XAMPP I/O         INFO Thereby the numbering scheme is as follows
Py:XAMPP I/O         INFO   -2 = kTreeAccess (direct access), -1 = kPoolAccess (horrible slow), 0 = kBranchAccess, 1 = kClassAccess(please try in case of problems).
Py:XAMPP I/O         INFO Will save the job's output to /var/reana/users/1dc3a29d-b8d6-4b90-94ba-53cda3f9b5e3/workflows/d992a0a9-1bbb-4df7-a908-6cc1500e60e3/selection_stage_mc16a/recast_signal_ntuple_mc16a.root
Py:AthFile           INFO opening [/var/reana/users/1dc3a29d-b8d6-4b90-94ba-53cda3f9b5e3/workflows/d992a0a9-1bbb-4df7-a908-6cc1500e60e3/rucio_download_mc16a/rucio_dr_mc16a/user.zhangr.28527328._000001.DAOD_EXOT27.pool.root.1]...
Py:XAMPP I/O         INFO Fullsimulation. Make sure that you wear your augmented reality glasses
Py:Athena           ERROR No such file or directory dev/PileupReweighting/share//DSID511xxx/pileup_mc16a_dsid511459_FS.root
Py:XAMPP BaseToolSetup    INFO The following lumi calc files will be used for pile up reweighting
Py:XAMPP BaseToolSetup    INFO    ++++ GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root
Py:XAMPP BaseToolSetup    INFO    ++++ GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root
Py:XAMPP BaseToolSetup    INFO The following prw config files will be used for pile up reweighting:
Py:XAMPP BaseToolSetup    INFO    ++++ /var/reana/users/1dc3a29d-b8d6-4b90-94ba-53cda3f9b5e3/workflows/d992a0a9-1bbb-4df7-a908-6cc1500e60e3/rucio_download_mc16a/rucio_pr_mc16a/user.zhangr.28528381._000001.NTUP_PILEUP.pool.root.1
Py:XAMPP BaseToolSetup    INFO We are running over p-tag 3992 today
Py:XAMPP BaseToolSetup    INFO The p-tag has timestamped b-tagging containers. Configure the selector to AntiKt4EMPFlowJets_BTagging201903
ERROR: Given MC channel number 511459 is not part of /xampp/build/x86_64-centos7-gcc8-opt/data/XAMPPmonoH/mc16_dsid_proc.txt
Py:XAMPP Algortihm    INFO Created XAMPP algorithm
MadGraph+Pythia8+EvtGen
Py:XAMPPmonoH event weight outlier strategy    INFO Detected a MadGraph+Pythia8 sample! Not applying weight outlier strategy.
Py:Athena            INFO including file "AthenaCommon/runbatch.py"
# setting LC_ALL to "C"
ApplicationMgr       INFO Updating Gaudi::PluginService::SetDebug(level) to level= 'PluginDebugLevel':0
ApplicationMgr    SUCCESS 
====================================================================================================================================
                                                   Welcome to ApplicationMgr (GaudiCoreSvc v4r1)
                                          running on reana-run-job-b21ae857-0c66-41a2-9729-924db16aad44-pz6hh on Sat Apr 16 23:42:59 2022
====================================================================================================================================
StatusCodeSvc                                                  INFO initialize
AthDictLoaderSvc                                               INFO in initialize...
AthDictLoaderSvc                                               INFO acquired Dso-registry
ClassIDSvc                                                     INFO  getRegistryEntries: read 2895 CLIDRegistry entries for module ALL
ChronoStatSvc                                                  INFO  Number of skipped events for MemStat-1
CoreDumpSvc                                                    INFO install f-a-t-a-l handler... (flag = -1)
CoreDumpSvc                                                    INFO Handling signals: 11(Segmentation fault) 7(Bus error) 4(Illegal instruction) 8(Floating point exception) 
AthenaEventLoopMgr                                             INFO Initializing AthenaEventLoopMgr - package version AthenaServices-00-00-00
AthMasterSeq                                                   INFO Member list: AthSequencer/AthAlgSeq, AthSequencer/AthOutSeq, AthSequencer/AthRegSeq

[32mXAMPPbase v1.0.0 -- xAOD analysis code from MPP Munich/ CERN / Brandeis University
                    Developed by Philipp Gadow (pgadow@cern.ch),
                                 Max Goblirsch-Kolb (goblirsc@cern.ch)
                                 Johannes Junggeburth (jojungge@cern.ch) and
                                 Nicolas Koehler (nkoehler@cern.ch)
                    Copyright (c) 2016-2019, GNU General Public License
                    https://cern.ch

[0m
AthAlgSeq                                                      INFO Member list: DecorateLRTruthLabels, XAMPP::XAMPPalgorithm/XAMPPAlgorithm
THistSvc                                                       INFO registered file [/var/reana/users/1dc3a29d-b8d6-4b90-94ba-53cda3f9b5e3/workflows/d992a0a9-1bbb-4df7-a908-6cc1500e60e3/selection_stage_mc16a/recast_signal_ntuple_mc16a.root]... [ok]
DecorateLRTruthLabels                                          INFO Initializing DecorateLRTruthLabels...
ToolSvc.LargeRJetTruthTool                                     INFO JSSWTopTaggerDNN: Initializing JSSWTopTaggerDNN tool
ToolSvc.LargeRJetTruthTool                                     INFO JSSWTopTaggerDNN: Using config file :JSSDNNTagger_AntiKt10LCTopoTrimmed_TopQuarkInclusive_MC16d_20190405_80Eff.dat
ToolSvc.LargeRJetTruthTool                                     INFO Using config file : JSSDNNTagger_AntiKt10LCTopoTrimmed_TopQuarkInclusive_MC16d_20190405_80Eff.dat
ToolSvc.LargeRJetTruthTool                                     INFO Configurations Loaded  :
ToolSvc.LargeRJetTruthTool                                     INFO tagType                : TopQuark
ToolSvc.LargeRJetTruthTool                                     INFO calibarea_keras        : BoostedJetTaggers/JSSWTopTaggerDNN/Rel21/
ToolSvc.LargeRJetTruthTool                                     INFO kerasConfigFileName    : dnn_inclusive_top_April04.json
ToolSvc.LargeRJetTruthTool                                     INFO kerasConfigOutputName  : dnn_inclusive_top
ToolSvc.LargeRJetTruthTool                                     INFO strMassCutLow          : 40.0
ToolSvc.LargeRJetTruthTool                                     INFO strMassCutHigh         : 999999999.0
ToolSvc.LargeRJetTruthTool                                     INFO pTCutLow               : 350
ToolSvc.LargeRJetTruthTool                                     INFO pTCutHigh              : 4000
ToolSvc.LargeRJetTruthTool                                     INFO strScoreCut            : (0.274254)*pow(x,0)+(0.00059366)*pow(x,1)+(-3.70438e-07)*pow(x,2)+(1.00444e-10)*pow(x,3)+(-1.00726e-14)*pow(x,4)
ToolSvc.LargeRJetTruthTool                                     INFO decorationName         : DNNTaggerTopQuarkInclusive80
ToolSvc.LargeRJetTruthTool                                     INFO Decorators that will be attached to jet :
ToolSvc.LargeRJetTruthTool                                     INFO   DNNTaggerTopQuarkInclusive80_Cut_mlow : lower mass cut for tagger choice
ToolSvc.LargeRJetTruthTool                                     INFO   DNNTaggerTopQuarkInclusive80_Cut_mhigh : upper mass cut for tagger choice
ToolSvc.LargeRJetTruthTool                                     INFO   DNNTaggerTopQuarkInclusive80_Cut_score : MVA score cut for tagger choice
ToolSvc.LargeRJetTruthTool                                     INFO   DNNTaggerTopQuarkInclusive80_Score : evaluated MVA score
ToolSvc.LargeRJetTruthTool                                     INFO DNN Tagger tool initialized
ToolSvc.LargeRJetTruthTool                                     INFO   Mass cut low   : 40.0
ToolSvc.LargeRJetTruthTool                                     INFO   Mass cut High  : 999999999.0
ToolSvc.LargeRJetTruthTool                                     INFO   Score cut low    : (0.274254)*pow(x,0)+(0.00059366)*pow(x,1)+(-3.70438e-07)*pow(x,2)+(1.00444e-10)*pow(x,3)+(-1.00726e-14)*pow(x,4)
ToolSvc.LargeRJetTruthTool                                     INFO JSSWTopTaggerDNN: Using CVMFS calibarea
ToolSvc.LargeRJetTruthTool                                     INFO JSSWTopTaggerDNN: DNN Tagger configured with: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/BoostedJetTaggers/JSSWTopTaggerDNN/Rel21/dnn_inclusive_top_April04.json
ToolSvc.LargeRJetTruthTool                                     INFO JSSWTopTaggerDNN: Keras Network NLayers : 11
ToolSvc.LargeRJetTruthTool                                     INFO After tagging, you will have access to the following cuts as a Root::TAccept : (<NCut>) <cut> : <description>)
ToolSvc.LargeRJetTruthTool                                     INFO   (0)  ValidPtRangeHigh : True if the jet is not too high pT
ToolSvc.LargeRJetTruthTool                                     INFO   (1)  ValidPtRangeLow : True if the jet is not too low pT
ToolSvc.LargeRJetTruthTool                                     INFO   (2)  ValidEtaRange : True if the jet is not too forward
ToolSvc.LargeRJetTruthTool                                     INFO   (3)  ValidJetContent : True if the jet is alright technically (e.g. all attributes necessary for tag)
ToolSvc.LargeRJetTruthTool                                     INFO   (4)  PassMassLow : mJet > mCutLow
ToolSvc.LargeRJetTruthTool                                     INFO   (5)  PassScore : ScoreJet > ScoreCut
ToolSvc.LargeRJetTruthTool                                     INFO JSSWTopTaggerDNN: DNN Tagger tool initialized
XAMPPAlgorithm                                                 INFO Initializing XAMPPAlgorithm...
ToolSvc.AnalysisHelper                                         INFO Initialising MonoH AnalysisHelper...
ToolSvc.AnalysisHelper                                         INFO Initializing...
ToolSvc.AnalysisHelper                                         INFO Starting Analysis Setup
ToolSvc.SUSYTools                                              INFO Initialising... 
ToolSvc.SUSYTools                                              INFO *****     *****     *****     *****
ToolSvc.SUSYTools                                              INFO Configuring from file /xampp/build/x86_64-centos7-gcc8-opt/data/XAMPPmonoH/SUSYTools_MonoH.conf
ToolSvc.SUSYTools                                              INFO Config file opened
ToolSvc.SUSYTools                                              INFO readConfig(): Loaded property Jet.InputType with value 9
ToolSvc.SUSYTools                                              INFO readConfig(): Loaded property Muon.Id with value 1
ToolSvc.SUSYTools                                              INFO readConfig(): Loaded property MuonBaseline.Id with value 2
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "EleBaseline.Pt" with value 7000
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "EleBaseline.Eta" with value 2.47
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "EleBaseline.Id" with value LooseAndBLayerLLH
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "EleBaseline.Iso" with value FCLoose
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "EleBaseline.Config" with value 
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "EleBaseline.CrackVeto" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Ele.ForceNoId" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Ele.Et" with value 7000
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Ele.Eta" with value 2.47
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Ele.CrackVeto" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Ele.Iso" with value FCLoose
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Ele.IsoHighPt" with value FCHighPtCaloOnly
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Ele.IsoHighPtThresh" with value 200000
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Ele.CFT" with value 
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Ele.CFTIso" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Ele.DoModifiedId" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Ele.Id" with value LooseAndBLayerLLH
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Ele.Config" with value 
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Ele.d0sig" with value 5
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Ele.z0" with value 0.5
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "EleBaseline.d0sig" with value 5
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "EleBaseline.z0" with value 0.5
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Ele.IdExpert" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Ele.EffNPcorrModel" with value TOTAL
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Ele.TriggerSFStringSingle" with value SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Ele.EffMapFilePath" with value ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/map3.txt
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Trig.Singlelep2015" with value e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose||mu20_iloose_L1MU15_OR_mu50
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Trig.Singlelep2016" with value e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0||mu26_ivarmedium_OR_mu50
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Trig.Singlelep2017" with value e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0||mu26_ivarmedium_OR_mu50
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Trig.Singlelep2018" with value e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0||mu26_ivarmedium_OR_mu50
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Trig.Dilep2015" with value e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose||mu20_iloose_L1MU15_OR_mu50||2e12_lhloose_L12EM10VH||e17_lhloose_mu14||e7_lhmedium_mu24||mu18_mu8noL1||2mu10
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Trig.Dilep2016" with value e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0||mu26_ivarmedium_OR_mu50||2e17_lhvloose_nod0||e17_lhloose_nod0_mu14||e7_lhmedium_nod0_mu24||e26_lhmedium_nod0_L1EM22VHI_mu8noL1||mu22_mu8noL1||2mu14
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Trig.Dilep2017" with value e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0||mu26_ivarmedium_OR_mu50||2e24_lhvloose_nod0||e17_lhloose_nod0_mu14||e7_lhmedium_nod0_mu24||e26_lhmedium_nod0_mu8noL1||mu22_mu8noL1||2mu14
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Trig.Dilep2018" with value e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0||mu26_ivarmedium_OR_mu50||2e24_lhvloose_nod0||e17_lhloose_nod0_mu14||e7_lhmedium_nod0_mu24||e26_lhmedium_nod0_mu8noL1||mu22_mu8noL1||2mu14
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Trig.Multi2015" with value e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose||mu20_iloose_L1MU15_OR_mu50||2e12_lhloose_L12EM10VH||e17_lhloose_2e9_lhloose||2e12_lhloose_mu10||e12_lhloose_2mu10||e17_lhloose_mu14||e7_lhmedium_mu24||mu18_mu8noL1||2mu10||3mu6
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Trig.Multi2016" with value e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0||mu26_ivarmedium_OR_mu50||2e17_lhvloose_nod0||e17_lhloose_nod0_mu14||e7_lhmedium_nod0_mu24||e26_lhmedium_nod0_L1EM22VHI_mu8noL1||e17_lhloose_nod0_2e9_lhloose_nod0||e12_lhloose_nod0_2mu10||2e12_lhloose_nod0_mu10||mu22_mu8noL1||2mu14||3mu6
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Trig.Multi2017" with value e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0||mu26_ivarmedium_OR_mu50||2e24_lhvloose_nod0||e17_lhloose_nod0_mu14||e7_lhmedium_nod0_mu24||e26_lhmedium_nod0_mu8noL1||e24_lhvloose_nod0_2e12_lhvloose_nod0_L1EM20VH_3EM10VH||e12_lhloose_nod0_2mu10||2e12_lhloose_nod0_mu10||mu22_mu8noL1||2mu14||3mu6
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Trig.Multi2018" with value e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0||mu26_ivarmedium_OR_mu50||2e24_lhvloose_nod0||e17_lhloose_nod0_mu14||e7_lhmedium_nod0_mu24||e26_lhmedium_nod0_mu8noL1||e24_lhvloose_nod0_2e12_lhvloose_nod0_L1EM20VH_3EM10VH||e12_lhloose_nod0_2mu10||2e12_lhloose_nod0_mu10||mu22_mu8noL1||2mu14||3mu6
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Trig.Diphoton2015" with value g35_loose_g25_loose
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Trig.Diphotonp2016" with value g35_loose_g25_loose
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Trig.Diphotonp2017" with value g35_medium_g25_medium_L12EM20VH
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Trig.Diphotonp2018" with value g35_medium_g25_medium_L12EM20VH
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MuonBaseline.Pt" with value 7000
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MuonBaseline.Eta" with value 2.5
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Muon.ForceNoId" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MuonBaseline.Iso" with value FCLoose
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Muon.TTVASF" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Muon.Pt" with value 7000
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Muon.Eta" with value 2.5
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Muon.Iso" with value FCTightTrackOnly
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Muon.IsoHighPt" with value FCLoose
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Muon.IsoHighPtThresh" with value 200000
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Muon.d0sig" with value 3
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Muon.z0" with value 0.5
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MuonBaseline.d0sig" with value 3
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MuonBaseline.z0" with value 0.5
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Muon.passedHighPt" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MuonCosmic.z0" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MuonCosmic.d0" with value 0.2
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "BadMuon.qoverp" with value 0.4
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Muon.CalibrationMode" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "PhotonBaseline.Pt" with value 25000
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "PhotonBaseline.Eta" with value 2.37
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "PhotonBaseline.Id" with value Tight
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "PhotonBaseline.CrackVeto" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "PhotonBaseline.Iso" with value 
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Photon.Pt" with value 130000
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Photon.Eta" with value 2.37
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Photon.Id" with value Tight
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Photon.Iso" with value FixedCutTight
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Photon.TriggerName" with value DI_PH_2015_2016_g20_tight_2016_g22_tight_2017_2018_g20_tight_icalovloose_L1EM15VHI
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Photon.CrackVeto" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Photon.AllowLate" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Tau.PrePtCut" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Tau.Pt" with value 20000
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Tau.Eta" with value 2.5
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Tau.Id" with value Medium
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Tau.ConfigPath" with value XAMPPmonoH/tau_selection_veryLoose_RNN.conf
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "TauBaseline.Id" with value Medium
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "TauBaseline.ConfigPath" with value XAMPPmonoH/tau_selection_veryLoose_RNN.conf
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Tau.DoTruthMatching" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.Pt" with value 20000
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.Eta" with value 2.5
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.JVT_WP" with value Tight
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.JvtPtMax" with value 120000
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.UncertConfig" with value rel21/Fall2018/R4_CategoryReduction_SimpleJER.config
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.UncertCalibArea" with value default
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.UncertPDsmearing" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.LargeRcollection" with value AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.LargeRuncConfig" with value rel21/Spring2019/R10_CategoryReduction.config
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.LargeRuncVars" with value default
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.TCCcollection" with value AntiKt10TrackCaloClusterTrimmedPtFrac5SmallR20Jets
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.TCCuncConfig" with value rel21/Summer2019/R10_Scale_TCC_all.config
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.WtaggerConfig" with value 
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.ZtaggerConfig" with value 
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.WZTaggerCalibArea" with value SmoothedWZTaggers/Rel21/
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.ToptaggerConfig" with value 
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.TopTaggerCalibArea" with value JSSWTopTaggerDNN/Rel21/
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.JESConfig" with value JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.JESConfigAFII" with value JES_MC16Recommendation_AFII_EMTopo_Apr2019_Rel21.config
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.JESConfigJMS" with value JES_JMS_MC16Recommendation_Consolidated_MC_only_EMTopo_July2019_Rel21.config
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.JESConfigJMSData" with value JES_JMS_MC16Recommendation_Consolidated_data_only_EMTopo_Sep2019_Rel21.config
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.JESConfigFat" with value JES_MC16recommendation_FatJet_Trimmed_JMS_comb_17Oct2018.config
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.JESConfigFatData" with value JES_MC16recommendation_FatJet_Trimmed_JMS_comb_3April2019.config
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.CalibSeq" with value JetArea_Residual_EtaJES_GSC_Insitu
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.CalibSeqJMS" with value JetArea_Residual_EtaJES_GSC
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.CalibSeqFat" with value EtaJES_JMS
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "TrackJet.Coll" with value AntiKtVR30Rmax4Rmin02TrackJets
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "TrackJet.Pt" with value 10000
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "TrackJet.Eta" with value 2.5
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "BadJet.Cut" with value TightBad
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "FwdJet.doJVT" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "FwdJet.JvtEtaMin" with value 2.5
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "FwdJet.JvtPtMax" with value 50000
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "FwdJet.JvtOp" with value Loose
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Jet.JMSCalib" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Btag.enable" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Btag.Tagger" with value DL1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Btag.WP" with value FixedCutBEff_77
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Btag.TimeStamp" with value 201810
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Btag.CalibPath" with value xAODBTaggingEfficiency/13TeV/2019-21-13TeV-MC16-CDI-2019-10-07_v1.root
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Btag.SystStrategy" with value SFEigen
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "BtagTrkJet.Tagger" with value DL1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "BtagTrkJet.WP" with value FixedCutBEff_77
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "BtagTrkJet.TimeStamp" with value 
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "BtagTrkJet.MinPt" with value 20000
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Btag.KeyOverride" with value 
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.DoBoostedElectron" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.BoostedElectronC1" with value 0.04
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.BoostedElectronC2" with value 10000
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.BoostedElectronMaxConeSize" with value 0.4
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.DoBoostedMuon" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.BoostedMuonC1" with value 0.04
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.BoostedMuonC2" with value 10000
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.BoostedMuonMaxConeSize" with value 0.4
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.DoMuonJetGhostAssociation" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.DoTau" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.DoPhoton" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.EleJet" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.ElEl" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.ElMu" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.MuonJet" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.Bjet" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.ElBjet" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.MuBjet" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.TauBjet" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.MuJetApplyRelPt" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.MuJetPtRatio" with value 0.5
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.MuJetTrkPtRatio" with value 0.7
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.RemoveCaloMuons" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.MuJetInnerDR" with value -999
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.BtagWP" with value FixedCutBEff_77
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.InputLabel" with value selected
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.BJetPtUpperThres" with value -1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.LinkOverlapObjects" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.DoFatJets" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.EleFatJetDR" with value 1.2
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "OR.JetFatJetDR" with value 1.4
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Trigger.UpstreamMatching" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "SigLep.RequireIso" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "SigEl.RequireIso" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "SigMu.RequireIso" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "SigPh.RequireIso" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "SigLepPh.UseSigLepForIsoCloseByOR" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "SigLepPh.IsoCloseByORpassLabel" with value 
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MET.EleTerm" with value RefEle
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MET.GammaTerm" with value RefGamma
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MET.TauTerm" with value RefTau
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MET.JetTerm" with value RefJet
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MET.MuonTerm" with value Muons
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MET.InputSuffix" with value 
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MET.OutputTerm" with value Final
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MET.RemoveOverlappingCaloTaggedMuons" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "Met.DoSetMuonJetEMScale" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MET.DoRemoveMuonJets" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MET.UseGhostMuons" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MET.DoMuonEloss" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MET.DoGreedyPhotons" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MET.DoVeryGreedyPhotons" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MET.DoTrkSyst" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MET.DoCaloSyst" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MET.DoTrkJetSyst" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "METSys.ConfigPrefix" with value METUtilities/data17_13TeV/prerec_Jan16
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "MET.JetSelection" with value Tight
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "METSig.SoftTermParam" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "METSig.TreatPUJets" with value 1
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "METSig.DoPhiReso" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "PRW.ActualMu2017File" with value GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "PRW.ActualMu2018File" with value GoodRunsLists/data18_13TeV/20181111/purw.actualMu.root
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "PRW.DataSF" with value 0.970874
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "PRW.DataSF_UP" with value 1.0101
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "PRW.DataSF_DW" with value 0.934579
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "PRW.UseRunDependentPrescaleWeight" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "PRW.autoconfigPRWPath" with value dev/PileupReweighting/share/
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "PRW.autoconfigPRWFile" with value 
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "PRW.autoconfigPRWCombinedmode" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "PRW.autoconfigPRWRPVmode" with value 0
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "PRW.autoconfigPRWHFFilter" with value 
ToolSvc.SUSYTools                                              INFO configFromFile(): Loaded property "StrictConfigCheck" with value 1
ToolSvc.SUSYTools                                              INFO Configured for jet collection AntiKt4EMPFlowJets
ToolSvc.SUSYTools                                              INFO Build MET with map: METAssoc_AntiKt4EMPFlow
ToolSvc.SUSYTools                                              INFO Adding ilumicalc file: GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root
ToolSvc.SUSYTools                                              INFO Adding ilumicalc file: GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root
Info in <CP::TPileupReweighting::UsePeriodConfig>: Using MC16 Period configuration
Info in <CP::TPileupReweighting::UsePeriodConfig>: Using MC16 Period configuration
Info in <CP::TPileupReweighting::UsePeriodConfig>: Using MC16 Period configuration
Info in <CP::TPileupReweighting::AddLumiCalcFile>: Adding LumiMetaData (scale factor=0.970874)...
Info in <CP::TPileupReweighting::AddLumiCalcFile>: Adding LumiMetaData (scale factor=1.010101)...
Info in <CP::TPileupReweighting::AddLumiCalcFile>: Adding LumiMetaData (scale factor=0.934579)...
Info in <CP::TPileupReweighting::AddLumiCalcFile>: Adding LumiMetaData (scale factor=0.970874)...
Info in <CP::TPileupReweighting::AddLumiCalcFile>: Adding LumiMetaData (scale factor=1.010101)...
Info in <CP::TPileupReweighting::AddLumiCalcFile>: Adding LumiMetaData (scale factor=0.934579)...
ToolSvc.myWeightTool                                           INFO AsgTool ToolSvc.myWeightTool @ 0x2265bb00
ToolSvc.JetCalibTool_AntiKt4EMPFlow                            INFO Initializing ToolSvc.JetCalibTool_AntiKt4EMPFlow...
ToolSvc.JetCalibTool_AntiKt4EMPFlow                            INFO ===================================
ToolSvc.JetCalibTool_AntiKt4EMPFlow                            INFO Initializing the xAOD Jet Calibration Tool for AntiKt4EMPFlowjets
ToolSvc.JetCalibTool_AntiKt4EMPFlow                            INFO Reading global JES settings from: JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config
ToolSvc.JetCalibTool_AntiKt4EMPFlow                            INFO resolved in: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetCalibTools/CalibArea-00-04-82/CalibrationConfigs/JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config
ToolSvc.JetCalibTool_AntiKt4EMPFlow                            INFO Initializing pileup correction.
ToolSvc.JetCalibTool_AntiKt4EMPFlow_Pileup                     INFO JetPileupCorrection: Starting scale: JetConstitScaleMomentum
ToolSvc.JetCalibTool_AntiKt4EMPFlow_Pileup                     INFO Jet area pile up correction will be applied.
ToolSvc.JetCalibTool_AntiKt4EMPFlow_Pileup_Residual            INFO Reading residual jet-area pile-up correction factors from: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetCalibTools/CalibArea-00-04-82/CalibrationFactors/MC16a_Residual_pflow.config
ToolSvc.JetCalibTool_AntiKt4EMPFlow_Pileup_Residual            INFO Description: MC16 residual mu- and N_{PV}-dependent jet pileup corrections for R=0.4 EMPFlow
ToolSvc.JetCalibTool_AntiKt4EMPFlow                            INFO Initializing JES correction.
ToolSvc.JetCalibTool_AntiKt4EMPFlow_EtaJES                     INFO Reading absolute calibration factors from: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetCalibTools/CalibArea-00-04-82/CalibrationFactors/MC16a_MCJES_4EMPFlow_Oct2017.config
ToolSvc.JetCalibTool_AntiKt4EMPFlow_EtaJES                     INFO Description: MCJES derived in October 2017 for MC16
ToolSvc.JetCalibTool_AntiKt4EMPFlow                            INFO Initializing GSC correction.
ToolSvc.JetCalibTool_AntiKt4EMPFlow_GSC                        INFO Initializing the Global Sequential Calibration tool
ToolSvc.JetCalibTool_AntiKt4EMPFlow_GSC                        INFO GSC Tool has been initialized with binning and eta fit factors from: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetCalibTools/CalibArea-00-04-82/CalibrationFactors/MC16a_GSC_R21_PF_28Nov2017.root
ToolSvc.JetCalibTool_AntiKt4EMPFlow                            INFO Initializing jet smearing correction
ToolSvc.JetCalibTool_AntiKt4EMPFlow_Smear                      INFO Initializing the jet smearing correction tool
ToolSvc.JetCalibTool_AntiKt4EMPFlow_Smear                      INFO Reading from JetGSCScaleMomentum and writing to JetSmearedMomentum
ToolSvc.JetCalibTool_AntiKt4EMPFlow                            INFO ===================================
ToolSvc.JetFatCalibTool_AntiKt10LCTopoTrimmedPtFrac5Small...   INFO Initializing ToolSvc.JetFatCalibTool_AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets...
ToolSvc.JetFatCalibTool_AntiKt10LCTopoTrimmedPtFrac5Small...   INFO ===================================
ToolSvc.JetFatCalibTool_AntiKt10LCTopoTrimmedPtFrac5Small...   INFO Initializing the xAOD Jet Calibration Tool for AntiKt10LCTopoTrimmedPtFrac5SmallR20jets
ToolSvc.JetFatCalibTool_AntiKt10LCTopoTrimmedPtFrac5Small...   INFO Reading global JES settings from: JES_MC16recommendation_FatJet_Trimmed_JMS_comb_17Oct2018.config
ToolSvc.JetFatCalibTool_AntiKt10LCTopoTrimmedPtFrac5Small...   INFO resolved in: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetCalibTools/CalibArea-00-04-82/CalibrationConfigs/JES_MC16recommendation_FatJet_Trimmed_JMS_comb_17Oct2018.config
ToolSvc.JetFatCalibTool_AntiKt10LCTopoTrimmedPtFrac5Small...   INFO Initializing JES correction.
ToolSvc.JetFatCalibTool_AntiKt10LCTopoTrimmedPtFrac5Small...   INFO Reading absolute calibration factors from: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetCalibTools/CalibArea-00-04-82/CalibrationFactors/MC16d_MCJES_akt10_Trimmed_12Oct2018.config
ToolSvc.JetFatCalibTool_AntiKt10LCTopoTrimmedPtFrac5Small...   INFO Description: JES for fat jets for MC16
ToolSvc.JetFatCalibTool_AntiKt10LCTopoTrimmedPtFrac5Small...   INFO Initializing JMS correction.
ToolSvc.JetFatCalibTool_AntiKt10LCTopoTrimmedPtFrac5Small...   INFO Initializing the JMS Calibration tool
ToolSvc.JetFatCalibTool_AntiKt10LCTopoTrimmedPtFrac5Small...   INFO JMS Tool has been initialized with binning and eta fit factors from: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetCalibTools/CalibArea-00-04-82/CalibrationFactors/MC16d_MCJMS_akt10_Trimmed_calo_12Oct2018.root
ToolSvc.JetFatCalibTool_AntiKt10LCTopoTrimmedPtFrac5Small...   INFO Track Assisted Jet Mass will be calibrated
ToolSvc.JetFatCalibTool_AntiKt10LCTopoTrimmedPtFrac5Small...   INFO JMS Tool has been initialized with binning and eta fit factors from: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetCalibTools/CalibArea-00-04-82/CalibrationFactors/MC16d_MCJMS_akt10_Trimmed_TA_12Oct2018.root
ToolSvc.JetFatCalibTool_AntiKt10LCTopoTrimmedPtFrac5Small...   INFO Mass Combination: ON
ToolSvc.JetFatCalibTool_AntiKt10LCTopoTrimmedPtFrac5Small...   INFO JMS Tool has been initialized with mass combination weights from: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetCalibTools/CalibArea-00-04-82/CalibrationFactors/QCD_Combination_Trimmed_17Oct2018.root
ToolSvc.JetFatCalibTool_AntiKt10LCTopoTrimmedPtFrac5Small...   INFO JMS Tool will use the e_LOGmOe_eta binning strategy
ToolSvc.JetFatCalibTool_AntiKt10LCTopoTrimmedPtFrac5Small...   INFO ===================================
ToolSvc.SUSYTools                                              INFO Set up Jet Uncertainty tool...
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO Preparing to initialize the JetUncertaintiesTool named 0x15eeb340#JetUncertaintiesTool/ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO ================================================
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO   Initializing the JetUncertaintiesTool named 0x15eeb340#JetUncertaintiesTool/ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO   Path is: ""
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO   CalibArea is: "CalibArea-08"
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO   IsData is: "false"
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO   Configuration file: "rel21/Fall2018/R4_CategoryReduction_SimpleJER.config"
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO     Location: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetUncertainties/CalibArea-08/rel21/Fall2018/R4_CategoryReduction_SimpleJER.config
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO   Uncertainty release: rel21_Fall2018
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO   Jet definition: AntiKt4EMPFlow
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO   MC type: MC16
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO   UncertaintyFile: "rel21/Fall2018/R4_AllComponents.root"
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO     Location: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetUncertainties/CalibArea-08/rel21/Fall2018/R4_AllComponents.root
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO   AnalysisFile: "analysisInputs/UnknownFlavourComp.root"
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO     Location: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetUncertainties/CalibArea-08/analysisInputs/UnknownFlavourComp.root
0x15eeb340#JetUncertaintiesTool/ToolSvc.JetUncertaintiesT...   INFO   NominalFourVecResData: "JER_Nominal_data,PtAbsEta,OnlyX"
0x15eeb340#JetUncertaintiesTool/ToolSvc.JetUncertaintiesT...   INFO   NominalFourVecResMC: "JER_Nominal_MCTYPE,PtAbsEta,OnlyX"
0x15eeb340#JetUncertaintiesTool/ToolSvc.JetUncertaintiesT...   INFO   ResolutionSmearOnlyMC: "true"
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO 
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO        JES uncert. comp.                        : Description
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO        -----------------                         ------------
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO     1. JET_EtaIntercalibration_Modelling        : Eta intercalibration: MC generator modelling uncertainty
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO     2. JET_EtaIntercalibration_TotalStat        : Eta intercalibration: statistical uncertainty
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO     3. JET_EtaIntercalibration_NonClosure_highE : Eta intercalibration: non-closure uncertainty, high E
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO     4. JET_EtaIntercalibration_NonClosure_negEta : Eta intercalibration: non-closure uncertainty, negative eta
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO     5. JET_EtaIntercalibration_NonClosure_posEta : Eta intercalibration: non-closure uncertainty, positive eta
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO     6. JET_Pileup_OffsetMu                      : Pileup: Offset, mu term
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO     7. JET_Pileup_OffsetNPV                     : Pileup: Offset, NPV term
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO     8. JET_Pileup_PtTerm                        : Pileup: Offset, pT term
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO     9. JET_Pileup_RhoTopology                   : Rho topology uncertainty (jet areas)
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    10. JET_Flavor_Composition                   : Flavor composition uncertainty
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    11. JET_Flavor_Response                      : Flavor response uncertainty (dominated by gluon response)
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    12. JET_PunchThrough_MC16                    : Punch-through correction uncertainty
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    13. JET_EffectiveNP_Statistical1             : Effective JES Uncertainty Statistical Component 1
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    14. JET_EffectiveNP_Statistical2             : Effective JES Uncertainty Statistical Component 2
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    15. JET_EffectiveNP_Statistical3             : Effective JES Uncertainty Statistical Component 3
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    16. JET_EffectiveNP_Statistical4             : Effective JES Uncertainty Statistical Component 4
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    17. JET_EffectiveNP_Statistical5             : Effective JES Uncertainty Statistical Component 5
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    18. JET_EffectiveNP_Statistical6             : Effective JES Uncertainty Statistical Component 6
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    19. JET_EffectiveNP_Modelling1               : Effective JES Uncertainty Modelling Component 1
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    20. JET_EffectiveNP_Modelling2               : Effective JES Uncertainty Modelling Component 2
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    21. JET_EffectiveNP_Modelling3               : Effective JES Uncertainty Modelling Component 3
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    22. JET_EffectiveNP_Modelling4               : Effective JES Uncertainty Modelling Component 4
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    23. JET_EffectiveNP_Detector1                : Effective JES Uncertainty Detector Component 1
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    24. JET_EffectiveNP_Detector2                : Effective JES Uncertainty Detector Component 2
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    25. JET_EffectiveNP_Mixed1                   : Effective JES Uncertainty Mixed Component 1
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    26. JET_EffectiveNP_Mixed2                   : Effective JES Uncertainty Mixed Component 2
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    27. JET_EffectiveNP_Mixed3                   : Effective JES Uncertainty Mixed Component 3
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    28. JET_SingleParticle_HighPt                : High pT term (r20.7 version)
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    29. JET_RelativeNonClosure_MC16              : Closure of the calibration, relative to MC12a
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    30. JET_BJES_Response                        : JES uncertainty for b jets
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    31. JET_JER_DataVsMC_MC16                    : NP covering when JER(data) < JER(MC)
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    32. JET_JER_EffectiveNP_1                    : Effective JER Uncertainty Component 1
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    33. JET_JER_EffectiveNP_2                    : Effective JER Uncertainty Component 2
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    34. JET_JER_EffectiveNP_3                    : Effective JER Uncertainty Component 3
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    35. JET_JER_EffectiveNP_4                    : Effective JER Uncertainty Component 4
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    36. JET_JER_EffectiveNP_5                    : Effective JER Uncertainty Component 5
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    37. JET_JER_EffectiveNP_6                    : Effective JER Uncertainty Component 6
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    38. JET_JER_EffectiveNP_7restTerm            : Effective JER Uncertainty Component 7
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO    Found and read in 50 individual components into 38 component groups of which 37 are recommended
ToolSvc.JetUncertaintiesTool_AntiKt4EMPFlow                    INFO ================================================
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO Preparing to initialize the JetUncertaintiesTool named 0x15eeb340#JetUncertaintiesTool/ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO ================================================
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO   Initializing the JetUncertaintiesTool named 0x15eeb340#JetUncertaintiesTool/ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO   Path is: ""
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO   CalibArea is: "CalibArea-08"
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO   IsData is: "false"
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO   Configuration file: "rel21/Spring2019/R10_CategoryReduction.config"
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO     Location: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetUncertainties/CalibArea-08/rel21/Spring2019/R10_CategoryReduction.config
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO   Uncertainty release: rel21_Spring2019LargeR
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO   Jet definition: AntiKt10LCTopoTrimmedPtFrac5SmallR20
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO   MC type: MC16
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO   UncertaintyFile: "rel21/Spring2019/R10_AllComponents.root"
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO     Location: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetUncertainties/CalibArea-08/rel21/Spring2019/R10_AllComponents.root
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO   AnalysisFile: "analysisInputs/UnknownFlavourCompR10.root"
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO     Location: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetUncertainties/CalibArea-08/analysisInputs/UnknownFlavourCompR10.root
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO   Found and loaded combined mass weight factors
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO     WeightCaloHist = CombWeight_Res_CaloMass_QCD_AntiKt10LCTopoTrimmedPtFrac5SmallR20
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO     WeightTAHist   = CombWeight_Res_TAMass_QCD_AntiKt10LCTopoTrimmedPtFrac5SmallR20
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO     WeightParam    = eLOGmOeAbsEta
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO     WeightCaloMassDef was set to Calo
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO     WeightTAMassDef was set to TA
0x15eeb340#JetUncertaintiesTool/ToolSvc.JetUncertaintiesT...   INFO   NominalMassResMCWZ: "MassRes_WZ_comb,PtMass,Full,CombQCD"
0x15eeb340#JetUncertaintiesTool/ToolSvc.JetUncertaintiesT...   INFO   NominalMassResMCHbb: "MassRes_Hbb_comb,PtMass,Full,CombQCD"
0x15eeb340#JetUncertaintiesTool/ToolSvc.JetUncertaintiesT...   INFO   NominalMassResMCTop: "MassRes_Top_comb,PtMass,Full,CombQCD"
0x15eeb340#JetUncertaintiesTool/ToolSvc.JetUncertaintiesT...   INFO   ResolutionSmearOnlyMC: "true"
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO 
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO        JES uncert. comp.                        : Description
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO        -----------------                         ------------
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO     1. JET_EtaIntercalibration_Modelling        : Eta intercalibration: MC generator modelling and method uncertainty
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO     2. JET_EtaIntercalibration_R10_TotalStat    : Eta intercalibration: statistical uncertainty
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO     3. JET_Flavor_Composition                   : Flavor composition uncertainty
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO     4. JET_Flavor_Response                      : Flavor response uncertainty (dominated by gluon response)
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO     5. JET_EffectiveNP_R10_Statistical1         : Effective JES Uncertainty Statistical Component 1
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO     6. JET_EffectiveNP_R10_Statistical2         : Effective JES Uncertainty Statistical Component 2
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO     7. JET_EffectiveNP_R10_Statistical3         : Effective JES Uncertainty Statistical Component 3
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO     8. JET_EffectiveNP_R10_Statistical4         : Effective JES Uncertainty Statistical Component 4
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO     9. JET_EffectiveNP_R10_Statistical5         : Effective JES Uncertainty Statistical Component 5
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    10. JET_EffectiveNP_R10_Statistical6         : Effective JES Uncertainty Statistical Component 6
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    11. JET_EffectiveNP_R10_Modelling1           : Effective JES Uncertainty Modelling Component 1
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    12. JET_EffectiveNP_R10_Modelling2           : Effective JES Uncertainty Modelling Component 2
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    13. JET_EffectiveNP_R10_Modelling3           : Effective JES Uncertainty Modelling Component 3
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    14. JET_EffectiveNP_R10_Modelling4           : Effective JES Uncertainty Modelling Component 4
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    15. JET_EffectiveNP_R10_Detector1            : Effective JES Uncertainty Detector Component 1
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    16. JET_EffectiveNP_R10_Detector2            : Effective JES Uncertainty Detector Component 2
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    17. JET_EffectiveNP_R10_Mixed1               : Effective JES Uncertainty Mixed Component 1
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    18. JET_EffectiveNP_R10_Mixed2               : Effective JES Uncertainty Mixed Component 2
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    19. JET_EffectiveNP_R10_Mixed3               : Effective JES Uncertainty Mixed Component 3
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    20. JET_EffectiveNP_R10_Mixed4               : Effective JES Uncertainty Mixed Component 4
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    21. JET_SingleParticle_HighPt                : High pT term (r20.7 version) for R=0.4
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    22. JET_LargeR_TopologyUncertainty_V         : Topology Uncertainty for W/Z bosons
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    23. JET_LargeR_TopologyUncertainty_top       : Topology uncertainty for top quarks
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    24. JET_CombMass_Baseline                    : 
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    25. JET_CombMass_Modelling                   : 
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    26. JET_CombMass_Tracking1                   : 
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    27. JET_CombMass_Tracking2                   : 
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    28. JET_CombMass_Tracking3                   : 
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    29. JET_CombMass_TotalStat                   : 
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    30. JET_MassRes_Top_comb                     : 
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    31. JET_MassRes_WZ_comb                      : 
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    32. JET_MassRes_Hbb_comb                     : 
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO    Found and read in 36 individual components into 32 component groups of which 32 are recommended
ToolSvc.JetUncertaintiesTool_AntiKt10LCTopoTrimmedPtFrac5...   INFO ================================================
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO Preparing to initialize the JetUncertaintiesTool named 0x15eeb340#JetUncertaintiesTool/ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrimmedPtFrac5SmallR20Jets
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO ================================================
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO   Initializing the JetUncertaintiesTool named 0x15eeb340#JetUncertaintiesTool/ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrimmedPtFrac5SmallR20Jets
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO   Path is: ""
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO   CalibArea is: "CalibArea-08"
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO   IsData is: "false"
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO   Configuration file: "rel21/Summer2019/R10_Scale_TCC_all.config"
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO     Location: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetUncertainties/CalibArea-08/rel21/Summer2019/R10_Scale_TCC_all.config
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO   Uncertainty release: rel21_Summer2019
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO   Jet definition: AntiKt10TrackCaloClusterTrimmedPtFrac5SmallR20
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO   MC type: MC16
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO   UncertaintyFile: "rel21/Summer2019/R10_Scale_TCC_AllComponents.root"
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO     Location: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetUncertainties/CalibArea-08/rel21/Summer2019/R10_Scale_TCC_AllComponents.root
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO 
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO        JES uncert. comp.                        : Description
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO        -----------------                         ------------
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO     1. JET_Rtrk_Baseline_pT                     : Rtrk measurement, rel 21.2: Baseline component, pT scale
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO     2. JET_Rtrk_Tracking_pT                     : Rtrk measurement, rel 21.2: Tracking component, pT scale
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO     3. JET_Rtrk_TotalStat_pT                    : Rtrk measurement, rel 21.2: TotalStat component, pT scale
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO     4. JET_Rtrk_Modelling_pT                    : Rtrk measurement, rel 21.2: Modelling component, pT scale
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO     5. JET_Rtrk_Closure_pT                      : Rtrk measurement, rel 21.2: Modelling component, pT scale
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO     6. JET_Rtrk_Baseline_mass                   : Rtrk measurement, rel 21.2: Baseline component, mass scale
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO     7. JET_Rtrk_Tracking_mass                   : Rtrk measurement, rel 21.2: Tracking component, mass scale
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO     8. JET_Rtrk_TotalStat_mass                  : Rtrk measurement, rel 21.2: TotalStat component, mass scale
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO     9. JET_Rtrk_Modelling_mass                  : Rtrk measurement, rel 21.2: Modelling component, mass scale
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO    10. JET_Rtrk_Closure_mass                    : Rtrk measurement, rel 21.2: Modelling component, mass scale
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO    Found and read in 14 individual components into 10 component groups of which 10 are recommended
ToolSvc.JetUncertaintiesTool_AntiKt10TrackCaloClusterTrim...   INFO ================================================
ToolSvc.JetCleaningTool                                        INFO Configured with cut level TightBad
ToolSvc.JetVertexTaggerTool                                    INFO Initializing JetVertexTaggerTool ToolSvc.JetVertexTaggerTool
ToolSvc.JetVertexTaggerTool                                    INFO   No track selector.
ToolSvc.JetVertexTaggerTool                                    INFO   Reading JVT file from:
    JetMomentTools/JVTlikelihood_20140805.root

ToolSvc.JetVertexTaggerTool                                    INFO                      resolved in  :
    /usr/AthAnalysis/21.2.108/InstallArea/x86_64-centos7-gcc8-opt/data/JetMomentTools/JVTlikelihood_20140805.root


ToolSvc.JVTEfficiencyTool                                      INFO  Path found = /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetJvtEfficiency/Moriond2018/JvtSFFile_EMPFlow.root
ToolSvc.FJVTEfficiencyTool_Tight_NOfJVT                        INFO  Path found = /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetJvtEfficiency/Nov2019/fJvtSFFile.EMtopo.root
ToolSvc.FJVTTool_Tight_NOfJVT                                  INFO Initializing ToolSvc.FJVTTool_Tight_NOfJVT...
ToolSvc.MuonCalibrationAndSmearingTool                         INFO The assignment of the calibration tools will be based on the random run number. Please make sure to call the pileup-tool before applying this tool
ToolSvc.MuonCalibrationAndSmearingTool                         INFO Data will be untouched. Instead an additional systematic will be added (recommended setup 2)
ToolSvc.MuonCalibrationAndSmearingTool                         INFO Setup the MuonMomentum calibration tool for 2015-2016 & mc16a
ToolSvc.MuonCalibrationAndSmearingTool                         INFO Setup the MuonMomentum calibration tool for 2017 & mc16c/d
ToolSvc.MuonCalibrationAndSmearingTool                         INFO Setup the MuonMomentum calibration tool for 2018 & mc16e
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_1516            INFO Initialising...
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_1516            INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data16/outqDeltamPlots_iter0/CB_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_1516            INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data16/outqDeltamPlots_iter1/CB_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_1516            INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data16/outqDeltamPlots_iter2/CB_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_1516            INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data16/outqDeltamPlots_iter3/CB_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_1516            INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data16/outqDeltamPlots_iter0/ID_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_1516            INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data16/outqDeltamPlots_iter1/ID_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_1516            INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data16/outqDeltamPlots_iter2/ID_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_1516            INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data16/outqDeltamPlots_iter3/ID_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_1516            INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data16/outqDeltamPlots_iter0/ME_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_1516            INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data16/outqDeltamPlots_iter1/ME_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_1516            INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data16/outqDeltamPlots_iter2/ME_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_1516            INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data16/outqDeltamPlots_iter3/ME_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_1516            INFO Not correcting data, only using systematic uncertainty
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_1516            INFO Using only statistical combination of sagitta bias ID and sagitta bias MS with rho 0
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_17              INFO Initialising...
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_17              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data17/outqDeltamPlots_iter0/CB_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_17              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data17/outqDeltamPlots_iter1/CB_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_17              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data17/outqDeltamPlots_iter2/CB_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_17              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data17/outqDeltamPlots_iter3/CB_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_17              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data17/outqDeltamPlots_iter0/ID_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_17              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data17/outqDeltamPlots_iter1/ID_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_17              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data17/outqDeltamPlots_iter2/ID_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_17              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data17/outqDeltamPlots_iter3/ID_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_17              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data17/outqDeltamPlots_iter0/ME_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_17              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data17/outqDeltamPlots_iter1/ME_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_17              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data17/outqDeltamPlots_iter2/ME_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_17              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data17/outqDeltamPlots_iter3/ME_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_17              INFO Not correcting data, only using systematic uncertainty
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_17              INFO Using only statistical combination of sagitta bias ID and sagitta bias MS with rho 0
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_18              INFO Initialising...
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_18              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data18/outqDeltamPlots_iter0/CB_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_18              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data18/outqDeltamPlots_iter1/CB_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_18              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data18/outqDeltamPlots_iter2/CB_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_18              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data18/outqDeltamPlots_iter3/CB_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_18              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data18/outqDeltamPlots_iter0/ID_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_18              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data18/outqDeltamPlots_iter1/ID_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_18              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data18/outqDeltamPlots_iter2/ID_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_18              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data18/outqDeltamPlots_iter3/ID_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_18              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data18/outqDeltamPlots_iter0/ME_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_18              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data18/outqDeltamPlots_iter1/ME_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_18              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data18/outqDeltamPlots_iter2/ME_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_18              INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19_Data18/outqDeltamPlots_iter3/ME_data.root
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_18              INFO Not correcting data, only using systematic uncertainty
ToolSvc.ToolSvc.MuonCalibrationAndSmearingTool_18              INFO Using only statistical combination of sagitta bias ID and sagitta bias MS with rho 0
ToolSvc.MuonSelectionTool_Baseline_Loose                       INFO Initialising...
ToolSvc.MuonSelectionTool_Baseline_Loose                       INFO Maximum eta: 2.5
ToolSvc.MuonSelectionTool_Baseline_Loose                       INFO Muon quality: 2
ToolSvc.MuonSelectionTool_Baseline_Loose                       INFO Initialising tight working point histograms...
ToolSvc.MuonSelectionTool_Baseline_Loose                       INFO Reading muon tight working point histograms from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/PreRec2016_2016-04-13/muonSelection_tightWPHisto.root
ToolSvc.MuonSelectionTool_Baseline_Loose                       INFO Successfully read tight working point histogram: tightWP_lowPt_rhoCuts
ToolSvc.MuonSelectionTool_Baseline_Loose                       INFO Successfully read tight working point histogram: tightWP_lowPt_qOverPCuts
ToolSvc.MuonSelectionTool_Baseline_Loose                       INFO Successfully read tight working point histogram: tightWP_mediumPt_rhoCuts
ToolSvc.MuonSelectionTool_Baseline_Loose                       INFO Successfully read tight working point histogram: tightWP_highPt_rhoCuts
                         : Booking "BDTG" of type "BDT" from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuidCB_EVEN.weights.xml.
                         : Reading weight file: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuidCB_EVEN.weights.xml
<HEADER> DataSetInfo              : [Default] : Added class "Signal"
<HEADER> DataSetInfo              : [Default] : Added class "Background"
                         : Booked classifier "BDTG" of type: "BDT"
                         : Booking "BDTG" of type "BDT" from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuidCB_ODD.weights.xml.
                         : Reading weight file: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuidCB_ODD.weights.xml
<HEADER> DataSetInfo              : [Default] : Added class "Signal"
<HEADER> DataSetInfo              : [Default] : Added class "Background"
                         : Booked classifier "BDTG" of type: "BDT"
                         : Booking "BDTG" of type "BDT" from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuGirl_EVEN.weights.xml.
                         : Reading weight file: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuGirl_EVEN.weights.xml
<HEADER> DataSetInfo              : [Default] : Added class "Signal"
<HEADER> DataSetInfo              : [Default] : Added class "Background"
                         : Booked classifier "BDTG" of type: "BDT"
                         : Booking "BDTG" of type "BDT" from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuGirl_ODD.weights.xml.
                         : Reading weight file: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuGirl_ODD.weights.xml
<HEADER> DataSetInfo              : [Default] : Added class "Signal"
<HEADER> DataSetInfo              : [Default] : Added class "Background"
                         : Booked classifier "BDTG" of type: "BDT"
ToolSvc.MuonSelectionTool_Medium                               INFO Initialising...
ToolSvc.MuonSelectionTool_Medium                               INFO Maximum eta: 2.5
ToolSvc.MuonSelectionTool_Medium                               INFO Muon quality: 1
ToolSvc.MuonSelectionTool_Medium                               INFO Initialising tight working point histograms...
ToolSvc.MuonSelectionTool_Medium                               INFO Reading muon tight working point histograms from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/PreRec2016_2016-04-13/muonSelection_tightWPHisto.root
ToolSvc.MuonSelectionTool_Medium                               INFO Successfully read tight working point histogram: tightWP_lowPt_rhoCuts
ToolSvc.MuonSelectionTool_Medium                               INFO Successfully read tight working point histogram: tightWP_lowPt_qOverPCuts
ToolSvc.MuonSelectionTool_Medium                               INFO Successfully read tight working point histogram: tightWP_mediumPt_rhoCuts
ToolSvc.MuonSelectionTool_Medium                               INFO Successfully read tight working point histogram: tightWP_highPt_rhoCuts
                         : Booking "BDTG" of type "BDT" from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuidCB_EVEN.weights.xml.
                         : Reading weight file: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuidCB_EVEN.weights.xml
<HEADER> DataSetInfo              : [Default] : Added class "Signal"
<HEADER> DataSetInfo              : [Default] : Added class "Background"
                         : Booked classifier "BDTG" of type: "BDT"
                         : Booking "BDTG" of type "BDT" from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuidCB_ODD.weights.xml.
                         : Reading weight file: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuidCB_ODD.weights.xml
<HEADER> DataSetInfo              : [Default] : Added class "Signal"
<HEADER> DataSetInfo              : [Default] : Added class "Background"
                         : Booked classifier "BDTG" of type: "BDT"
                         : Booking "BDTG" of type "BDT" from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuGirl_EVEN.weights.xml.
                         : Reading weight file: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuGirl_EVEN.weights.xml
<HEADER> DataSetInfo              : [Default] : Added class "Signal"
<HEADER> DataSetInfo              : [Default] : Added class "Background"
                         : Booked classifier "BDTG" of type: "BDT"
                         : Booking "BDTG" of type "BDT" from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuGirl_ODD.weights.xml.
                         : Reading weight file: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuGirl_ODD.weights.xml
<HEADER> DataSetInfo              : [Default] : Added class "Signal"
<HEADER> DataSetInfo              : [Default] : Added class "Background"
                         : Booked classifier "BDTG" of type: "BDT"
ToolSvc.MuonSelectionHighPtTool_Medium                         INFO Initialising...
ToolSvc.MuonSelectionHighPtTool_Medium                         INFO Maximum eta: 2.5
ToolSvc.MuonSelectionHighPtTool_Medium                         INFO Muon quality: 4
ToolSvc.MuonSelectionHighPtTool_Medium                         INFO Initialising tight working point histograms...
ToolSvc.MuonSelectionHighPtTool_Medium                         INFO Reading muon tight working point histograms from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/PreRec2016_2016-04-13/muonSelection_tightWPHisto.root
ToolSvc.MuonSelectionHighPtTool_Medium                         INFO Successfully read tight working point histogram: tightWP_lowPt_rhoCuts
ToolSvc.MuonSelectionHighPtTool_Medium                         INFO Successfully read tight working point histogram: tightWP_lowPt_qOverPCuts
ToolSvc.MuonSelectionHighPtTool_Medium                         INFO Successfully read tight working point histogram: tightWP_mediumPt_rhoCuts
ToolSvc.MuonSelectionHighPtTool_Medium                         INFO Successfully read tight working point histogram: tightWP_highPt_rhoCuts
                         : Booking "BDTG" of type "BDT" from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuidCB_EVEN.weights.xml.
                         : Reading weight file: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuidCB_EVEN.weights.xml
<HEADER> DataSetInfo              : [Default] : Added class "Signal"
<HEADER> DataSetInfo              : [Default] : Added class "Background"
                         : Booked classifier "BDTG" of type: "BDT"
                         : Booking "BDTG" of type "BDT" from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuidCB_ODD.weights.xml.
                         : Reading weight file: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuidCB_ODD.weights.xml
<HEADER> DataSetInfo              : [Default] : Added class "Signal"
<HEADER> DataSetInfo              : [Default] : Added class "Background"
                         : Booked classifier "BDTG" of type: "BDT"
                         : Booking "BDTG" of type "BDT" from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuGirl_EVEN.weights.xml.
                         : Reading weight file: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuGirl_EVEN.weights.xml
<HEADER> DataSetInfo              : [Default] : Added class "Signal"
<HEADER> DataSetInfo              : [Default] : Added class "Background"
                         : Booked classifier "BDTG" of type: "BDT"
                         : Booking "BDTG" of type "BDT" from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuGirl_ODD.weights.xml.
                         : Reading weight file: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonSelectorTools/190118_PrelimLowPtMVA/LowPtMVA_Weights/BDTG_9JAN2019_MuGirl_ODD.weights.xml
<HEADER> DataSetInfo              : [Default] : Added class "Signal"
<HEADER> DataSetInfo              : [Default] : Added class "Background"
                         : Booked classifier "BDTG" of type: "BDT"
ToolSvc.MuonEfficiencyScaleFactors_Medium                      INFO Efficiency type is = RECO
ToolSvc.MuonEfficiencyScaleFactors_Medium                      INFO JPsi based low pt SF will start to rock below 15 GeV!
ToolSvc.MuonEfficiencyScaleFactors_Medium                      INFO Trying to initialize, with working point Medium, using calibration release 191111_Winter_PrecisionZ
ToolSvc.MuonEfficiencyScaleFactors_Medium                      INFO Successfully initialized! 
ToolSvc.MuonEfficiencyScaleFactorsBMHighPt_Medium              INFO Efficiency type is = BADMUON
ToolSvc.MuonEfficiencyScaleFactorsBMHighPt_Medium              INFO Trying to initialize, with working point BadMuonVeto_HighPt, using calibration release 191111_Winter_PrecisionZ
ToolSvc.MuonEfficiencyScaleFactorsBMHighPt_Medium              INFO Successfully initialized! 
ToolSvc.MuonTTVAEfficiencyScaleFactors                         INFO Efficiency type is = TTVA
ToolSvc.MuonTTVAEfficiencyScaleFactors                         INFO Trying to initialize, with working point TTVA, using calibration release 191111_Winter_PrecisionZ
ToolSvc.MuonTTVAEfficiencyScaleFactors                         INFO Successfully initialized! 
ToolSvc.MuonIsolationScaleFactors_FCTightTrackOnly             INFO Efficiency type is = ISO
ToolSvc.MuonIsolationScaleFactors_FCTightTrackOnly             INFO Trying to initialize, with working point FCTightTrackOnlyIso, using calibration release 191111_Winter_PrecisionZ
ToolSvc.MuonIsolationScaleFactors_FCTightTrackOnly             INFO Successfully initialized! 
ToolSvc.MuonHighPtIsolationScaleFactors_FCLoose                INFO Efficiency type is = ISO
ToolSvc.MuonHighPtIsolationScaleFactors_FCLoose                INFO Trying to initialize, with working point FCLooseIso, using calibration release 191111_Winter_PrecisionZ
ToolSvc.MuonHighPtIsolationScaleFactors_FCLoose                INFO Successfully initialized! 
ToolSvc.MuonTriggerScaleFactors_Medium                         INFO MuonQuality = 'Medium'
ToolSvc.MuonTriggerScaleFactors_Medium                         INFO Binning = 'fine'
ToolSvc.MuonTriggerScaleFactors_Medium                         INFO CalibrationRelease = '190129_Winter_r21'
ToolSvc.MuonTriggerScaleFactors_Medium                         INFO CustomInputFolder = ''
ToolSvc.MuonTriggerScaleFactors_Medium                         INFO AllowZeroSF = 1
ToolSvc.MuonTriggerScaleFactors_Medium                         INFO experimental = 0
ToolSvc.MuonTriggerScaleFactors_Medium                         INFO useRel27 = 0
ToolSvc.MuonTriggerScaleFactors_Medium                         INFO MuonTriggerScaleFactors::initialize
ToolSvc.EleSelLikelihood_LooseAndBLayerLLH                     INFO operating point : LooseBLLHElectron
ToolSvc.EleSelLikelihoodBaseline_LooseAndBLayerLLH             INFO operating point : LooseBLLHElectron
ToolSvc.PhotonSelIsEM_Tight                                    INFO operating point : Tight
ToolSvc.PhotonSelIsEMBaseline_Tight                            INFO operating point : Tight
Will now set key "RecoKey" to value "Reconstruction" when configuring an AsgElectronEfficiencyCorrectionTool
Will now set key "IdKey" to value "LooseBLayer" when configuring an AsgElectronEfficiencyCorrectionTool
ToolSvc.SUSYTools                                           WARNING Your Electron ID WP (LooseAndBLayerLLH) is not supported for ECID SFs, falling back to MediumLLH for SF purposes
ToolSvc.SUSYTools                                           WARNING Your Electron Iso WP (FCLoose) is not supported for ECID SFs, falling back to Gradient for SF purposes
ToolSvc.SUSYTools                                           WARNING Your Electron ID WP (LooseAndBLayerLLH) is not supported for charge ID SFs, falling back to MediumLLH for SF purposes
ToolSvc.AsgPhotonEfficiencyCorrectionTool_Tight                INFO  Using path = /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/PhotonEfficiencyCorrection/2015_2017/rel21.2/Summer2018_Rec_v1/efficiencySF.offline.Tight.13TeV.rel21.25ns.con.v01.root
ToolSvc.AsgPhotonEfficiencyCorrectionTool_Tight                INFO  Using path = /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/PhotonEfficiencyCorrection/2015_2017/rel21.2/Summer2018_Rec_v1/efficiencySF.offline.Tight.13TeV.rel21.25ns.unc.v01.root
ToolSvc.AsgPhotonEfficiencyCorrectionTool_isolFixedCutTight    INFO  Using path = /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/PhotonEfficiencyCorrection/2015_2017/rel21.2/Summer2018_Rec_v1/efficiencySF.Isolation.isolFixedCutTight.13TeV.rel21.25ns.con.TrkCaloRadZTrkCaloSgPh.v03.root
ToolSvc.AsgPhotonEfficiencyCorrectionTool_isolFixedCutTight    INFO  Using path = /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/PhotonEfficiencyCorrection/2015_2017/rel21.2/Summer2018_Rec_v1/efficiencySF.Isolation.isolFixedCutTight.13TeV.rel21.25ns.unc.TrkCaloRadZTrkCaloSgPh.v03.root
ToolSvc.SUSYTools                                           WARNING No Photon trigger SF available for FixedCutTight, using TightCaloOnly instead... Use at your own risk
ToolSvc.AsgPhotonEfficiencyCorrectionTool_trigDI_PH_2015_...   INFO  Using path = /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/PhotonEfficiencyCorrection/2015_2018/rel21.2/Summer2018_Rec_v1/trigger/efficiencySF.DI_PH_2015_2016_g20_tight_2016_g22_tight_2017_2018_g20_tight_icalovloose_L1EM15VHI.v2.FixedCutTightCaloOnly.root
ToolSvc.AsgPhotonEfficiencyCorrectionTool_trigDI_PH_2015_...   INFO  Using path = /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/PhotonEfficiencyCorrection/2015_2018/rel21.2/Summer2018_Rec_v1/trigger/efficiencySF.DI_PH_2015_2016_g20_tight_2016_g22_tight_2017_2018_g20_tight_icalovloose_L1EM15VHI.v2.FixedCutTightCaloOnly.root
ToolSvc.AsgPhotonEfficiencyCorrectionTool_trigSF_asymm_di...   INFO  Using path = /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/PhotonEfficiencyCorrection/2015_2018/rel21.2/Summer2018_Rec_v1/trigger/efficiencySF.DI_PH_2015_g25_loose_2016_g25_loose_2017_g25_medium_L1EM20VH_2018_g25_medium_L1EM20VH.v2.FixedCutTightCaloOnly.root
ToolSvc.AsgPhotonEfficiencyCorrectionTool_trigSF_asymm_di...   INFO  Using path = /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/PhotonEfficiencyCorrection/2015_2018/rel21.2/Summer2018_Rec_v1/trigger/efficiencySF.DI_PH_2015_g25_loose_2016_g25_loose_2017_g25_medium_L1EM20VH_2018_g25_medium_L1EM20VH.v2.FixedCutTightCaloOnly.root
ToolSvc.AsgPhotonEfficiencyCorrectionTool_trigEff_asymm_d...   INFO  Using path = /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/PhotonEfficiencyCorrection/2015_2018/rel21.2/Summer2018_Rec_v1/trigger/efficiency.DI_PH_2015_g25_loose_2016_g25_loose_2017_g25_medium_L1EM20VH_2018_g25_medium_L1EM20VH.v2.FixedCutTightCaloOnly.root
ToolSvc.AsgPhotonEfficiencyCorrectionTool_trigEff_asymm_d...   INFO  Using path = /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/PhotonEfficiencyCorrection/2015_2018/rel21.2/Summer2018_Rec_v1/trigger/efficiency.DI_PH_2015_g25_loose_2016_g25_loose_2017_g25_medium_L1EM20VH_2018_g25_medium_L1EM20VH.v2.FixedCutTightCaloOnly.root
ToolSvc.AsgPhotonEfficiencyCorrectionTool_trigSF_asymm_di...   INFO  Using path = /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/PhotonEfficiencyCorrection/2015_2018/rel21.2/Summer2018_Rec_v1/trigger/efficiencySF.DI_PH_2015_g35_loose_2016_g35_loose_2017_g35_medium_L1EM20VH_2018_g35_medium_L1EM20VH.v2.FixedCutTightCaloOnly.root
ToolSvc.AsgPhotonEfficiencyCorrectionTool_trigSF_asymm_di...   INFO  Using path = /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/PhotonEfficiencyCorrection/2015_2018/rel21.2/Summer2018_Rec_v1/trigger/efficiencySF.DI_PH_2015_g35_loose_2016_g35_loose_2017_g35_medium_L1EM20VH_2018_g35_medium_L1EM20VH.v2.FixedCutTightCaloOnly.root
ToolSvc.AsgPhotonEfficiencyCorrectionTool_trigEff_asymm_d...   INFO  Using path = /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/PhotonEfficiencyCorrection/2015_2018/rel21.2/Summer2018_Rec_v1/trigger/efficiency.DI_PH_2015_g35_loose_2016_g35_loose_2017_g35_medium_L1EM20VH_2018_g35_medium_L1EM20VH.v2.FixedCutTightCaloOnly.root
ToolSvc.AsgPhotonEfficiencyCorrectionTool_trigEff_asymm_d...   INFO  Using path = /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/PhotonEfficiencyCorrection/2015_2018/rel21.2/Summer2018_Rec_v1/trigger/efficiency.DI_PH_2015_g35_loose_2016_g35_loose_2017_g35_medium_L1EM20VH_2018_g35_medium_L1EM20VH.v2.FixedCutTightCaloOnly.root
ToolSvc.ElectronPhotonShowerShapeFudgeTool                     INFO No config file set! Using default shift values for the electron shifter.
Info in <FudgeMCTool>: Path found = /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/ElectronPhotonShowerShapeFudgeTool/v2/PhotonFudgeFactors.root
ToolSvc.ElectronChargeIDSelectorTool_LooseBLLHElectron         INFO OP to use: loose, with cut on BDT: -0.337671, which corresponds to user specified working point.
ToolSvc.ElectronChargeIDSelectorTool_LooseBLLHElectron         INFO trainingfile to use  /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/ElectronPhotonSelectorTools/ChargeID/ECIDS_20180731rel21Summer2018.root
ToolSvc.ElectronChargeIDSelectorTool_LooseBLLHElectron         INFO ECIDS nfold configuration: 2
ToolSvc.ElectronChargeIDSelectorTool_LooseBLLHElectron         INFO Variables for ECIDS= avgCharge_SCTw,deltaphiRes,cd0,EoverP,pt,abs_eta
ToolSvc.EgammaCalibrationAndSmearingTool                       INFO Initialization
eg_resolution                                                  INFO Initialize eg_resolution
GainUncertainty                                                INFO opening file /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/ElectronPhotonFourMomentumCorrection/v14/gain_uncertainty_specialRun.root
egammaMVACalib                                                 INFO Accessing calibration from egammaMVACalib/offline/v7
egammaMVACalib                                                 INFO Reading weights from egammaMVACalib/offline/v7
egammaMVACalib                                                 INFO 117 readers created
egammaMVACalib                                                 INFO egammaMVACalib::printReadersInfo 100 eta bin(s) -- ( 0 < abs(el_cl_eta) < 2.5
egammaMVACalib                                                 INFO egammaMVACalib::printReadersInfo 100 energy bin(s) -- ( 0  < (el_rawcl_Es1 + el_rawcl_Es2 + el_rawcl_Es3)/cosh(el_cl_eta) < 50000GeV
egammaMVACalib                                                 INFO Number of variables:10
EgammaMVATool                                                  INFO Using layer correction
EgammaMVATool                                                  INFO 10 variables for electrons
MVATreeElectron                                                INFO creating tree for 10 variables: el_cl_E,el_cl_E_TileGap3,el_cl_eta,el_cl_etaCalo,el_cl_phi,el_cl_phiCalo,el_rawcl_Es0,el_rawcl_Es1,el_rawcl_Es2,el_rawcl_Es3
egammaMVACalib                                                 INFO Variable that defines particleType not set. Using default formula: ph_Rconv > 0. && ph_Rconv < 800.
egammaMVACalib                                                 INFO Accessing calibration from egammaMVACalib/offline/v7
egammaMVACalib                                                 INFO Reading weights from egammaMVACalib/offline/v7
egammaMVACalib                                                 INFO 234 readers created
egammaMVACalib                                                 INFO egammaMVACalib::printReadersInfo 100 eta bin(s) -- ( 0 < abs(ph_cl_eta) < 2.5
egammaMVACalib                                                 INFO egammaMVACalib::printReadersInfo 100 energy bin(s) -- ( 0  < (ph_rawcl_Es1 + ph_rawcl_Es2 + ph_rawcl_Es3)/cosh(ph_cl_eta) < 50000GeV
egammaMVACalib                                                 INFO Number of variables:14
EgammaMVATool                                                  INFO configuration does not contain list of variables, try to guess:
EgammaMVATool                                                  INFO 18 variables for photons
EgammaMVATool                                                  INFO   ph_Rconv
EgammaMVATool                                                  INFO   ph_cl_E
EgammaMVATool                                                  INFO   ph_cl_E_TileGap3
EgammaMVATool                                                  INFO   ph_cl_eta
EgammaMVATool                                                  INFO   ph_cl_etaCalo
EgammaMVATool                                                  INFO   ph_cl_phi
EgammaMVATool                                                  INFO   ph_cl_phiCalo
EgammaMVATool                                                  INFO   ph_convtrk1nPixHits
EgammaMVATool                                                  INFO   ph_convtrk1nSCTHits
EgammaMVATool                                                  INFO   ph_convtrk2nPixHits
EgammaMVATool                                                  INFO   ph_convtrk2nSCTHits
EgammaMVATool                                                  INFO   ph_pt1conv
EgammaMVATool                                                  INFO   ph_pt2conv
EgammaMVATool                                                  INFO   ph_ptconv
EgammaMVATool                                                  INFO   ph_rawcl_Es0
EgammaMVATool                                                  INFO   ph_rawcl_Es1
EgammaMVATool                                                  INFO   ph_rawcl_Es2
EgammaMVATool                                                  INFO   ph_rawcl_Es3
MVATreePhoton                                                  INFO creating tree for 18 variables: ph_Rconv,ph_cl_E,ph_cl_E_TileGap3,ph_cl_eta,ph_cl_etaCalo,ph_cl_phi,ph_cl_phiCalo,ph_convtrk1nPixHits,ph_convtrk1nSCTHits,ph_convtrk2nPixHits,ph_convtrk2nSCTHits,ph_pt1conv,ph_pt2conv,ph_ptconv,ph_rawcl_Es0,ph_rawcl_Es1,ph_rawcl_Es2,ph_rawcl_Es3
egammaLayerRecalibTool                                         INFO using scale es2017_21.0_v0
egammaLayerRecalibTool                                         INFO using scale run2_alt_with_layer2_r21_v0
egammaLayerRecalibTool                                         INFO using scale layer2_alt_run2_r21_v0
egammaLayerRecalibTool                                         INFO using scale ps_2016_r21_v0
ToolSvc.EgammaCalibrationAndSmearingTool                       INFO ESModel: es2018_R21_v0
ToolSvc.EgammaCalibrationAndSmearingTool                       INFO ResolutionType: SigmaEff90
ToolSvc.EgammaCalibrationAndSmearingTool                       INFO layer correction = 2
ToolSvc.EgammaCalibrationAndSmearingTool                       INFO PS correction = 2
ToolSvc.EgammaCalibrationAndSmearingTool                       INFO S12 correction = 2
ToolSvc.EgammaCalibrationAndSmearingTool                       INFO layer2 recalibration = 2
ToolSvc.EgammaCalibrationAndSmearingTool                       INFO intermodule correction = 1
ToolSvc.EgammaCalibrationAndSmearingTool                       INFO phi uniformity correction = 1
ToolSvc.EgammaCalibrationAndSmearingTool                       INFO gain correction = 0
ToolSvc.EgammaCalibrationAndSmearingTool                       INFO smearing = 2
ToolSvc.EgammaCalibrationAndSmearingTool                       INFO insitu scales = 2
ToolSvc.EgammaCalibrationAndSmearingTool                       INFO ep combination = 0
ToolSvc.EgammaCalibrationAndSmearingTool                       INFO use MVA calibration = 2
ToolSvc.EgammaCalibrationAndSmearingTool                       INFO use temperature correction 2015 = 2
ToolSvc.EgammaCalibrationAndSmearingTool                       INFO use uA2MeV correction 2015 1/2 week = 2
ToolSvc.TauSelectionTool_Medium                                INFO Initializing TauSelectionTool
ToolSvc.TauSelectionToolBaseline_Medium                        INFO Initializing TauSelectionTool
ToolSvc.TauEffTool_Medium                                      INFO Initializing TauEfficiencyCorrectionsTool
ToolSvc.TauEffTool_Medium                                   WARNING Unsupported ID working point with enum 19
ToolSvc.TauEffTool_Medium.RecoHadTauTool                       INFO Initializing CommonEfficiencyTool
ToolSvc.TauEffTool_Medium.RecoHadTauTool                       INFO data loaded from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/TauAnalysisTools/00-03-12/EfficiencyCorrections/Reco_TrueHadTau_mc16-prerec.root
ToolSvc.TauEffTool_Medium.EleOLRHadTauTool                     INFO Initializing CommonEfficiencyTool
ToolSvc.TauEffTool_Medium.EleOLRHadTauTool                     INFO data loaded from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/TauAnalysisTools/00-03-12/EfficiencyCorrections/EleOLR_TrueHadTau_2016-ichep.root
ToolSvc.TauEffTool_Medium.EleOLRElectronTool                   INFO Initializing CommonEfficiencyTool
ToolSvc.TauEffTool_Medium.EleOLRElectronTool                   INFO data loaded from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/TauAnalysisTools/00-03-12/EfficiencyCorrections/EleOLR_TrueElectron_2019-summer.root
ToolSvc.TauTrigEffTool_3_HLT_tau25_medium1_tracktwo            INFO Initializing TauEfficiencyCorrectionsTool
ToolSvc.TauTrigEffTool_3_HLT_tau25_medium1_tracktwo.Trigg...   INFO Initializing TauEfficiencyTriggerTool
ToolSvc.TauTrigEffTool_3_HLT_tau25_medium1_tracktwo.Trigg...   INFO data loaded from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/TauAnalysisTools/00-03-12/EfficiencyCorrections/Trigger/Trigger_TrueHadTau_2017-moriond_data2015_HLT_tau25_medium1_tracktwo_etainc.root
ToolSvc.TauTrigEffTool_3_HLT_tau25_medium1_tracktwo.Trigg...   INFO Initializing TauEfficiencyTriggerTool
ToolSvc.TauTrigEffTool_3_HLT_tau25_medium1_tracktwo.Trigg...   INFO data loaded from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/TauAnalysisTools/00-03-12/EfficiencyCorrections/Trigger/Trigger_TrueHadTau_2017-moriond_data2016_comb_HLT_tau25_medium1_tracktwo_etainc.root
ToolSvc.TauTrigEffTool_3_HLT_tau25_medium1_tracktwo.Trigg...   INFO Initializing TauEfficiencyTriggerTool
ToolSvc.TauTrigEffTool_3_HLT_tau25_medium1_tracktwo.Trigg...   INFO data loaded from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/TauAnalysisTools/00-03-12/EfficiencyCorrections/Trigger/Trigger_TrueHadTau_2017-moriond_data2017_comb_HLT_tau25_medium1_tracktwo_etainc.root
ToolSvc.TauTrigEffTool_3_HLT_tau35_medium1_tracktwo            INFO Initializing TauEfficiencyCorrectionsTool
ToolSvc.TauTrigEffTool_3_HLT_tau35_medium1_tracktwo.Trigg...   INFO Initializing TauEfficiencyTriggerTool
ToolSvc.TauTrigEffTool_3_HLT_tau35_medium1_tracktwo.Trigg...   INFO data loaded from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/TauAnalysisTools/00-03-12/EfficiencyCorrections/Trigger/Trigger_TrueHadTau_2017-moriond_data2015_HLT_tau35_medium1_tracktwo_etainc.root
ToolSvc.TauTrigEffTool_3_HLT_tau35_medium1_tracktwo.Trigg...   INFO Initializing TauEfficiencyTriggerTool
ToolSvc.TauTrigEffTool_3_HLT_tau35_medium1_tracktwo.Trigg...   INFO data loaded from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/TauAnalysisTools/00-03-12/EfficiencyCorrections/Trigger/Trigger_TrueHadTau_2017-moriond_data2016_comb_HLT_tau35_medium1_tracktwo_etainc.root
ToolSvc.TauTrigEffTool_3_HLT_tau35_medium1_tracktwo.Trigg...   INFO Initializing TauEfficiencyTriggerTool
ToolSvc.TauTrigEffTool_3_HLT_tau35_medium1_tracktwo.Trigg...   INFO data loaded from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/TauAnalysisTools/00-03-12/EfficiencyCorrections/Trigger/Trigger_TrueHadTau_2017-moriond_data2017_comb_HLT_tau35_medium1_tracktwo_etainc.root
ToolSvc.TauTrigEffTool_3_HLT_tau50_medium1_tracktwo_L1TAU12    INFO Initializing TauEfficiencyCorrectionsTool
ToolSvc.TauTrigEffTool_3_HLT_tau50_medium1_tracktwo_L1TAU...   INFO Initializing TauEfficiencyTriggerTool
ToolSvc.TauTrigEffTool_3_HLT_tau50_medium1_tracktwo_L1TAU...   INFO data loaded from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/TauAnalysisTools/00-03-12/EfficiencyCorrections/Trigger/Trigger_TrueHadTau_2017-moriond_data2015_HLT_tau50_medium1_tracktwo_L1TAU12_etainc.root
ToolSvc.TauTrigEffTool_3_HLT_tau50_medium1_tracktwo_L1TAU...   INFO Initializing TauEfficiencyTriggerTool
ToolSvc.TauTrigEffTool_3_HLT_tau50_medium1_tracktwo_L1TAU...   INFO data loaded from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/TauAnalysisTools/00-03-12/EfficiencyCorrections/Trigger/Trigger_TrueHadTau_2017-moriond_data2016_comb_HLT_tau50_medium1_tracktwo_L1TAU12_etainc.root
ToolSvc.TauTrigEffTool_3_HLT_tau50_medium1_tracktwo_L1TAU...   INFO Initializing TauEfficiencyTriggerTool
ToolSvc.TauTrigEffTool_3_HLT_tau50_medium1_tracktwo_L1TAU...   INFO data loaded from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/TauAnalysisTools/00-03-12/EfficiencyCorrections/Trigger/Trigger_TrueHadTau_2017-moriond_data2017_comb_HLT_tau50_medium1_tracktwo_L1TAU12_etainc.root
ToolSvc.TauTrigEffTool_3_HLT_tau80_medium1_tracktwo            INFO Initializing TauEfficiencyCorrectionsTool
ToolSvc.TauTrigEffTool_3_HLT_tau80_medium1_tracktwo.Trigg...   INFO Initializing TauEfficiencyTriggerTool
ToolSvc.TauTrigEffTool_3_HLT_tau80_medium1_tracktwo.Trigg...   INFO data loaded from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/TauAnalysisTools/00-03-12/EfficiencyCorrections/Trigger/Trigger_TrueHadTau_2017-moriond_data2015_HLT_tau80_medium1_tracktwo_etainc.root
ToolSvc.TauTrigEffTool_3_HLT_tau80_medium1_tracktwo.Trigg...   INFO Initializing TauEfficiencyTriggerTool
ToolSvc.TauTrigEffTool_3_HLT_tau80_medium1_tracktwo.Trigg...   INFO data loaded from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/TauAnalysisTools/00-03-12/EfficiencyCorrections/Trigger/Trigger_TrueHadTau_2017-moriond_data2016_comb_HLT_tau80_medium1_tracktwo_etainc.root
ToolSvc.TauTrigEffTool_3_HLT_tau80_medium1_tracktwo.Trigg...   INFO Initializing TauEfficiencyTriggerTool
ToolSvc.TauTrigEffTool_3_HLT_tau80_medium1_tracktwo.Trigg...   INFO data loaded from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/TauAnalysisTools/00-03-12/EfficiencyCorrections/Trigger/Trigger_TrueHadTau_2017-moriond_data2017_comb_HLT_tau80_medium1_tracktwo_etainc.root
ToolSvc.TauTrigEffTool_3_HLT_tau125_medium1_tracktwo           INFO Initializing TauEfficiencyCorrectionsTool
ToolSvc.TauTrigEffTool_3_HLT_tau125_medium1_tracktwo.Trig...   INFO Initializing TauEfficiencyTriggerTool
ToolSvc.TauTrigEffTool_3_HLT_tau125_medium1_tracktwo.Trig...   INFO data loaded from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/TauAnalysisTools/00-03-12/EfficiencyCorrections/Trigger/Trigger_TrueHadTau_2017-moriond_data2015_HLT_tau125_medium1_tracktwo_etainc.root
ToolSvc.TauTrigEffTool_3_HLT_tau125_medium1_tracktwo.Trig...   INFO Initializing TauEfficiencyTriggerTool
ToolSvc.TauTrigEffTool_3_HLT_tau125_medium1_tracktwo.Trig...   INFO data loaded from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/TauAnalysisTools/00-03-12/EfficiencyCorrections/Trigger/Trigger_TrueHadTau_2017-moriond_data2016_comb_HLT_tau125_medium1_tracktwo_etainc.root
ToolSvc.TauTrigEffTool_3_HLT_tau125_medium1_tracktwo.Trig...   INFO Initializing TauEfficiencyTriggerTool
ToolSvc.TauTrigEffTool_3_HLT_tau125_medium1_tracktwo.Trig...   INFO data loaded from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/TauAnalysisTools/00-03-12/EfficiencyCorrections/Trigger/Trigger_TrueHadTau_2017-moriond_data2017_comb_HLT_tau125_medium1_tracktwo_etainc.root
ToolSvc.SUSYTools                                              INFO 'TauMVACalibration' is the default procedure in R21
ToolSvc.TauSmearingTool                                        INFO Initializing TauSmearingTool
ToolSvc.TauSmearingTool.ToolSvc.TauSmearingTool_CommonSme...   INFO Initializing CommonSmearingTool
ToolSvc.TauSmearingTool.ToolSvc.TauSmearingTool_CommonSme...   INFO data loaded from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/TauAnalysisTools/00-03-12/Smearing/TES_TrueHadTau_2019-summer.root
ToolSvc.TauEleORDecorator                                      INFO Initializing TauOverlappingElectronLLHDecorator
ToolSvc.TauEleORDecorator                                      INFO Loading ele OLR cut file TauAnalysisTools/00-03-12/Selection/eveto_cutvals.root
ToolSvc.BTagEffTool_AntiKt4EMPFlowJets_BTagging201810_Fix...   INFO  Hello BTaggingEfficiencyTool user... initializing
ToolSvc.BTagEffTool_AntiKt4EMPFlowJets_BTagging201810_Fix...   INFO  TaggerName = DL1
ToolSvc.BTagEffTool_AntiKt4EMPFlowJets_BTagging201810_Fix...   INFO  OP = FixedCutBEff_77
ToolSvc.BTagEffTool_AntiKt4EMPFlowJets_BTagging201810_Fix...   INFO  b-jet     SF/eff calibration = default / 410470
ToolSvc.BTagEffTool_AntiKt4EMPFlowJets_BTagging201810_Fix...   INFO  c-jet     SF/eff calibration = default / 410470
ToolSvc.BTagEffTool_AntiKt4EMPFlowJets_BTagging201810_Fix...   INFO  tau-jet   SF/eff calibration = default / 410470
ToolSvc.BTagEffTool_AntiKt4EMPFlowJets_BTagging201810_Fix...   INFO  light-jet SF/eff calibration = default / 410470
ToolSvc.BTagEffTool_AntiKt4EMPFlowJets_BTagging201810_Fix...   INFO  JetAuthor = AntiKt4EMPFlowJets_BTagging201810
=== CalibrationDataInterfaceROOT::CalibrationDataInterfaceROOT ===
 taggerName           : DL1
 SF         file name : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/2019-21-13TeV-MC16-CDI-2019-10-07_v1.root

 CDI file build number: 5657257

 List of uncertainties to exclude:
    T:  extrapolation from charm

======= end of CalibrationDataInterfaceROOT instantiation ========
CalibrationDataInterface: retrieved container DL1/AntiKt4EMPFlowJets_BTagging201810/FixedCutBEff_77/Light/410470_Eff (with comment: 'efficiencies from sample 410470' and hadronisation setting 'Pythia8EvtGen')
CalibrationDataInterface: retrieved container DL1/AntiKt4EMPFlowJets_BTagging201810/FixedCutBEff_77/Light/default_SF (with comment: 'negative_tag_Zjet+extrap[Run2MCcalib]' and hadronisation setting 'Pythia8EvtGen')
CalibrationDataEigenVariations: Removing 294 eigenvector variations leading to sub-tolerance effects, retaining 6 variations
btag Calib: reducing number of eigenvector variations for flavour Light to 5
CalibrationDataInterface: retrieved container DL1/AntiKt4EMPFlowJets_BTagging201810/FixedCutBEff_77/C/410470_Eff (with comment: 'efficiencies from sample 410470' and hadronisation setting 'Pythia8EvtGen')
CalibrationDataInterface: retrieved container DL1/AntiKt4EMPFlowJets_BTagging201810/FixedCutBEff_77/C/default_SF (with comment: 'ttbarC+extrap[Run2MCcalib_ttC]' and hadronisation setting 'Pythia8EvtGen')
CalibrationDataEigenVariations: Removing 295 eigenvector variations leading to sub-tolerance effects, retaining 5 variations
btag Calib: reducing number of eigenvector variations for flavour C to 4
CalibrationDataInterface: retrieved container DL1/AntiKt4EMPFlowJets_BTagging201810/FixedCutBEff_77/B/410470_Eff (with comment: 'efficiencies from sample 410470' and hadronisation setting 'Pythia8EvtGen')
CalibrationDataInterface: retrieved container DL1/AntiKt4EMPFlowJets_BTagging201810/FixedCutBEff_77/B/default_SF (with comment: 'ttbar_PDF+extrap[Run2MCcalib]' and hadronisation setting 'Pythia8EvtGen')
CalibrationDataEigenVariations: Removing 291 eigenvector variations leading to sub-tolerance effects, retaining 9 variations
btag Calib: reducing number of eigenvector variations for flavour B to 3
CalibrationDataInterface: retrieved container DL1/AntiKt4EMPFlowJets_BTagging201810/FixedCutBEff_77/T/410470_Eff (with comment: 'efficiencies from sample 410470' and hadronisation setting 'Pythia8EvtGen')
CalibrationDataInterface: retrieved container DL1/AntiKt4EMPFlowJets_BTagging201810/FixedCutBEff_77/T/default_SF (with comment: 'ttbarC+addSys[newsys]+extrap[Run2MCcalib_ttC]' and hadronisation setting 'Pythia8EvtGen')
CalibrationDataEigenVariations: Removing 295 eigenvector variations leading to sub-tolerance effects, retaining 5 variations
btag Calib: reducing number of eigenvector variations for flavour T to 4
ToolSvc.BTagEffTool_AntiKt4EMPFlowJets_BTagging201810_Fix...   INFO Using systematics model SFEigen
ToolSvc.BTagEffTool_AntiKtVR30Rmax4Rmin02TrackJets_FixedC...   INFO  Hello BTaggingEfficiencyTool user... initializing
ToolSvc.BTagEffTool_AntiKtVR30Rmax4Rmin02TrackJets_FixedC...   INFO  TaggerName = DL1
ToolSvc.BTagEffTool_AntiKtVR30Rmax4Rmin02TrackJets_FixedC...   INFO  OP = FixedCutBEff_77
ToolSvc.BTagEffTool_AntiKtVR30Rmax4Rmin02TrackJets_FixedC...   INFO  b-jet     SF/eff calibration = default / 410470
ToolSvc.BTagEffTool_AntiKtVR30Rmax4Rmin02TrackJets_FixedC...   INFO  c-jet     SF/eff calibration = default / 410470
ToolSvc.BTagEffTool_AntiKtVR30Rmax4Rmin02TrackJets_FixedC...   INFO  tau-jet   SF/eff calibration = default / 410470
ToolSvc.BTagEffTool_AntiKtVR30Rmax4Rmin02TrackJets_FixedC...   INFO  light-jet SF/eff calibration = default / 410470
ToolSvc.BTagEffTool_AntiKtVR30Rmax4Rmin02TrackJets_FixedC...   INFO  JetAuthor = AntiKtVR30Rmax4Rmin02TrackJets
=== CalibrationDataInterfaceROOT::CalibrationDataInterfaceROOT ===
 taggerName           : DL1
 SF         file name : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/2019-21-13TeV-MC16-CDI-2019-10-07_v1.root

 CDI file build number: 5657257

 List of uncertainties to exclude:
    T:  extrapolation from charm

======= end of CalibrationDataInterfaceROOT instantiation ========
CalibrationDataInterface: retrieved container DL1/AntiKtVR30Rmax4Rmin02TrackJets/FixedCutBEff_77/Light/410470_Eff (with comment: 'efficiencies from sample 410470' and hadronisation setting 'Pythia8EvtGen')
CalibrationDataInterface: retrieved container DL1/AntiKtVR30Rmax4Rmin02TrackJets/FixedCutBEff_77/Light/default_SF (with comment: 'negative_tag_Zjet+extrap[Run2MCcalib]' and hadronisation setting 'Pythia8EvtGen')
CalibrationDataEigenVariations: Removing 294 eigenvector variations leading to sub-tolerance effects, retaining 6 variations
btag Calib: reducing number of eigenvector variations for flavour Light to 5
CalibrationDataInterface: retrieved container DL1/AntiKtVR30Rmax4Rmin02TrackJets/FixedCutBEff_77/C/410470_Eff (with comment: 'efficiencies from sample 410470' and hadronisation setting 'Pythia8EvtGen')
CalibrationDataInterface: retrieved container DL1/AntiKtVR30Rmax4Rmin02TrackJets/FixedCutBEff_77/C/default_SF (with comment: 'ttbarC+extrap[Run2MCcalib_ttC]' and hadronisation setting 'Pythia8EvtGen')
CalibrationDataEigenVariations: Removing 296 eigenvector variations leading to sub-tolerance effects, retaining 4 variations
btag Calib: reducing number of eigenvector variations for flavour C to 4
CalibrationDataInterface: retrieved container DL1/AntiKtVR30Rmax4Rmin02TrackJets/FixedCutBEff_77/B/410470_Eff (with comment: 'efficiencies from sample 410470' and hadronisation setting 'Pythia8EvtGen')
CalibrationDataInterface: retrieved container DL1/AntiKtVR30Rmax4Rmin02TrackJets/FixedCutBEff_77/B/default_SF (with comment: 'ttbar_PDF+extrap[Run2MCcalib]' and hadronisation setting 'Pythia8EvtGen')
CalibrationDataEigenVariations: Removing 295 eigenvector variations leading to sub-tolerance effects, retaining 5 variations
btag Calib: reducing number of eigenvector variations for flavour B to 3
CalibrationDataInterface: retrieved container DL1/AntiKtVR30Rmax4Rmin02TrackJets/FixedCutBEff_77/T/410470_Eff (with comment: 'efficiencies from sample 410470' and hadronisation setting 'Pythia8EvtGen')
CalibrationDataInterface: retrieved container DL1/AntiKtVR30Rmax4Rmin02TrackJets/FixedCutBEff_77/T/default_SF (with comment: 'ttbarC+addSys[newsys]+extrap[Run2MCcalib_ttC]' and hadronisation setting 'Pythia8EvtGen')
CalibrationDataEigenVariations: Removing 296 eigenvector variations leading to sub-tolerance effects, retaining 4 variations
btag Calib: reducing number of eigenvector variations for flavour T to 4
ToolSvc.BTagEffTool_AntiKtVR30Rmax4Rmin02TrackJets_FixedC...   INFO Using systematics model SFEigen
ToolSvc.METMaker_ST_Tight_NOfJVT                               INFO Initializing ToolSvc.METMaker_ST_Tight_NOfJVT...
ToolSvc.METMaker_ST_Tight_NOfJVT                               INFO Use jet selection criterion: Tight PFlow: 1
ToolSvc.METSystTool                                            INFO Searching for configFile: TrackSoftTerms-pflow.config
ToolSvc.METSystTool                                            INFO Configuring from file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/METUtilities/data17_13TeV/prerec_Jan16/TrackSoftTerms-pflow.config
ToolSvc.METSystTool                                            INFO Will read histograms from : METUtilities/data17_13TeV/prerec_Jan16/pre-rec_systematics_EMPFlow.root
ToolSvc.METSystTool                                            INFO Systpath :/
ToolSvc.METSystTool                                            INFO Extracted histogram path : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/METUtilities/data17_13TeV/prerec_Jan16/pre-rec_systematics_EMPFlow.root
ToolSvc.METSystTool                                            INFO AffectingSystematics are:
ToolSvc.METSystTool                                            INFO MET_SoftTrk_ResoCorr
ToolSvc.METSystTool                                            INFO MET_SoftTrk_ResoPara
ToolSvc.METSystTool                                            INFO MET_SoftTrk_ResoPerp
ToolSvc.METSystTool                                            INFO MET_SoftTrk_ScaleDown
ToolSvc.METSystTool                                            INFO MET_SoftTrk_ScaleUp
ToolSvc.metSignificance_AntiKt4EMPFlow                         INFO Initializing ToolSvc.metSignificance_AntiKt4EMPFlow...
ToolSvc.metSignificance_AntiKt4EMPFlow                         INFO Set up JER tools
ToolSvc.metSignificance_AntiKt4EMPFlow                         INFO Set up jet resolution tool
ToolSvc.jetCalibTool_AntiKt4EMPFlow                            INFO Initializing ToolSvc.jetCalibTool_AntiKt4EMPFlow...
ToolSvc.jetCalibTool_AntiKt4EMPFlow                            INFO ===================================
ToolSvc.jetCalibTool_AntiKt4EMPFlow                            INFO Initializing the xAOD Jet Calibration Tool for AntiKt4EMPFlowjets
ToolSvc.jetCalibTool_AntiKt4EMPFlow                            INFO Reading global JES settings from: JES_data2017_2016_2015_Recommendation_PFlow_Aug2018_rel21.config
ToolSvc.jetCalibTool_AntiKt4EMPFlow                            INFO resolved in: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetCalibTools/CalibArea-00-04-81/CalibrationConfigs/JES_data2017_2016_2015_Recommendation_PFlow_Aug2018_rel21.config
ToolSvc.jetCalibTool_AntiKt4EMPFlow                            INFO Initializing pileup correction.
ToolSvc.jetCalibTool_AntiKt4EMPFlow_Pileup                     INFO JetPileupCorrection: Starting scale: JetConstitScaleMomentum
ToolSvc.jetCalibTool_AntiKt4EMPFlow_Pileup                     INFO Jet area pile up correction will be applied.
ToolSvc.jetCalibTool_AntiKt4EMPFlow_Pileup_Residual            INFO Reading residual jet-area pile-up correction factors from: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetCalibTools/CalibArea-00-04-81/CalibrationFactors/MC16a_Residual_pflow.config
ToolSvc.jetCalibTool_AntiKt4EMPFlow_Pileup_Residual            INFO Description: MC16 residual mu- and N_{PV}-dependent jet pileup corrections for R=0.4 EMPFlow
ToolSvc.jetCalibTool_AntiKt4EMPFlow                            INFO Initializing JES correction.
ToolSvc.jetCalibTool_AntiKt4EMPFlow_EtaJES                     INFO Reading absolute calibration factors from: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetCalibTools/CalibArea-00-04-81/CalibrationFactors/MC16a_MCJES_4EMPFlow_Oct2017.config
ToolSvc.jetCalibTool_AntiKt4EMPFlow_EtaJES                     INFO Description: MCJES derived in October 2017 for MC16
ToolSvc.jetCalibTool_AntiKt4EMPFlow                            INFO Initializing GSC correction.
ToolSvc.jetCalibTool_AntiKt4EMPFlow_GSC                        INFO Initializing the Global Sequential Calibration tool
ToolSvc.jetCalibTool_AntiKt4EMPFlow_GSC                        INFO GSC Tool has been initialized with binning and eta fit factors from: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetCalibTools/CalibArea-00-04-81/CalibrationFactors/MC16a_GSC_R21_PF_28Nov2017.root
ToolSvc.jetCalibTool_AntiKt4EMPFlow                            INFO Initializing jet smearing correction
ToolSvc.jetCalibTool_AntiKt4EMPFlow_Smear                      INFO Initializing the jet smearing correction tool
ToolSvc.jetCalibTool_AntiKt4EMPFlow_Smear                      INFO Reading from JetGSCScaleMomentum and writing to JetSmearedMomentum
ToolSvc.jetCalibTool_AntiKt4EMPFlow                            INFO ===================================
ToolSvc.metSignificance_AntiKt4EMPFlow                         INFO Set up MuonCalibrationAndSmearing tools
ToolSvc.METSigAutoConf_MuonCalibrationAndSmearingTool          INFO Initialising...
ToolSvc.METSigAutoConf_MuonCalibrationAndSmearingTool          INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19/outqDeltamPlots_iter0/CB_data.root
ToolSvc.METSigAutoConf_MuonCalibrationAndSmearingTool          INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19/outqDeltamPlots_iter1/CB_data.root
ToolSvc.METSigAutoConf_MuonCalibrationAndSmearingTool          INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19/outqDeltamPlots_iter2/CB_data.root
ToolSvc.METSigAutoConf_MuonCalibrationAndSmearingTool          INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19/outqDeltamPlots_iter3/CB_data.root
ToolSvc.METSigAutoConf_MuonCalibrationAndSmearingTool          INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19/outqDeltamPlots_iter0/ID_data.root
ToolSvc.METSigAutoConf_MuonCalibrationAndSmearingTool          INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19/outqDeltamPlots_iter1/ID_data.root
ToolSvc.METSigAutoConf_MuonCalibrationAndSmearingTool          INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19/outqDeltamPlots_iter2/ID_data.root
ToolSvc.METSigAutoConf_MuonCalibrationAndSmearingTool          INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19/outqDeltamPlots_iter3/ID_data.root
ToolSvc.METSigAutoConf_MuonCalibrationAndSmearingTool          INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19/outqDeltamPlots_iter0/ME_data.root
ToolSvc.METSigAutoConf_MuonCalibrationAndSmearingTool          INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19/outqDeltamPlots_iter1/ME_data.root
ToolSvc.METSigAutoConf_MuonCalibrationAndSmearingTool          INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19/outqDeltamPlots_iter2/ME_data.root
ToolSvc.METSigAutoConf_MuonCalibrationAndSmearingTool          INFO Opening correction file : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/MuonMomentumCorrections/sagittaBiasDataAll_03_02_19/outqDeltamPlots_iter3/ME_data.root
ToolSvc.METSigAutoConf_MuonCalibrationAndSmearingTool          INFO Not correcting data, only using systematic uncertainty
ToolSvc.METSigAutoConf_MuonCalibrationAndSmearingTool          INFO Using only statistical combination of sagitta bias ID and sagitta bias MS with rho 0
ToolSvc.METSigAutoConf_EgammaCalibrationAndSmearingTool        INFO Initialization
eg_resolution                                                  INFO Initialize eg_resolution
GainUncertainty                                                INFO opening file /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/ElectronPhotonFourMomentumCorrection/v14/gain_uncertainty_specialRun.root
asg::ToolStore::put       WARNING Tool with name "EgammaMVATool" already registered
egammaMVACalib                                                 INFO Accessing calibration from egammaMVACalib/offline/v7
egammaMVACalib                                                 INFO Reading weights from egammaMVACalib/offline/v7
egammaMVACalib                                                 INFO 117 readers created
egammaMVACalib                                                 INFO egammaMVACalib::printReadersInfo 100 eta bin(s) -- ( 0 < abs(el_cl_eta) < 2.5
egammaMVACalib                                                 INFO egammaMVACalib::printReadersInfo 100 energy bin(s) -- ( 0  < (el_rawcl_Es1 + el_rawcl_Es2 + el_rawcl_Es3)/cosh(el_cl_eta) < 50000GeV
egammaMVACalib                                                 INFO Number of variables:10
EgammaMVATool                                                  INFO Using layer correction
EgammaMVATool                                                  INFO 10 variables for electrons
MVATreeElectron                                                INFO creating tree for 10 variables: el_cl_E,el_cl_E_TileGap3,el_cl_eta,el_cl_etaCalo,el_cl_phi,el_cl_phiCalo,el_rawcl_Es0,el_rawcl_Es1,el_rawcl_Es2,el_rawcl_Es3
egammaMVACalib                                                 INFO Variable that defines particleType not set. Using default formula: ph_Rconv > 0. && ph_Rconv < 800.
egammaMVACalib                                                 INFO Accessing calibration from egammaMVACalib/offline/v7
egammaMVACalib                                                 INFO Reading weights from egammaMVACalib/offline/v7
egammaMVACalib                                                 INFO 234 readers created
egammaMVACalib                                                 INFO egammaMVACalib::printReadersInfo 100 eta bin(s) -- ( 0 < abs(ph_cl_eta) < 2.5
egammaMVACalib                                                 INFO egammaMVACalib::printReadersInfo 100 energy bin(s) -- ( 0  < (ph_rawcl_Es1 + ph_rawcl_Es2 + ph_rawcl_Es3)/cosh(ph_cl_eta) < 50000GeV
egammaMVACalib                                                 INFO Number of variables:14
EgammaMVATool                                                  INFO configuration does not contain list of variables, try to guess:
EgammaMVATool                                                  INFO 18 variables for photons
EgammaMVATool                                                  INFO   ph_Rconv
EgammaMVATool                                                  INFO   ph_cl_E
EgammaMVATool                                                  INFO   ph_cl_E_TileGap3
EgammaMVATool                                                  INFO   ph_cl_eta
EgammaMVATool                                                  INFO   ph_cl_etaCalo
EgammaMVATool                                                  INFO   ph_cl_phi
EgammaMVATool                                                  INFO   ph_cl_phiCalo
EgammaMVATool                                                  INFO   ph_convtrk1nPixHits
EgammaMVATool                                                  INFO   ph_convtrk1nSCTHits
EgammaMVATool                                                  INFO   ph_convtrk2nPixHits
EgammaMVATool                                                  INFO   ph_convtrk2nSCTHits
EgammaMVATool                                                  INFO   ph_pt1conv
EgammaMVATool                                                  INFO   ph_pt2conv
EgammaMVATool                                                  INFO   ph_ptconv
EgammaMVATool                                                  INFO   ph_rawcl_Es0
EgammaMVATool                                                  INFO   ph_rawcl_Es1
EgammaMVATool                                                  INFO   ph_rawcl_Es2
EgammaMVATool                                                  INFO   ph_rawcl_Es3
MVATreePhoton                                                  INFO creating tree for 18 variables: ph_Rconv,ph_cl_E,ph_cl_E_TileGap3,ph_cl_eta,ph_cl_etaCalo,ph_cl_phi,ph_cl_phiCalo,ph_convtrk1nPixHits,ph_convtrk1nSCTHits,ph_convtrk2nPixHits,ph_convtrk2nSCTHits,ph_pt1conv,ph_pt2conv,ph_ptconv,ph_rawcl_Es0,ph_rawcl_Es1,ph_rawcl_Es2,ph_rawcl_Es3
egammaLayerRecalibTool                                         INFO using scale es2017_21.0_v0
egammaLayerRecalibTool                                         INFO using scale run2_alt_with_layer2_r21_v0
egammaLayerRecalibTool                                         INFO using scale layer2_alt_run2_r21_v0
egammaLayerRecalibTool                                         INFO using scale ps_2016_r21_v0
ToolSvc.METSigAutoConf_EgammaCalibrationAndSmearingTool        INFO ESModel: es2017_R21_v0
ToolSvc.METSigAutoConf_EgammaCalibrationAndSmearingTool        INFO ResolutionType: SigmaEff90
ToolSvc.METSigAutoConf_EgammaCalibrationAndSmearingTool        INFO layer correction = 2
ToolSvc.METSigAutoConf_EgammaCalibrationAndSmearingTool        INFO PS correction = 2
ToolSvc.METSigAutoConf_EgammaCalibrationAndSmearingTool        INFO S12 correction = 2
ToolSvc.METSigAutoConf_EgammaCalibrationAndSmearingTool        INFO layer2 recalibration = 2
ToolSvc.METSigAutoConf_EgammaCalibrationAndSmearingTool        INFO intermodule correction = 1
ToolSvc.METSigAutoConf_EgammaCalibrationAndSmearingTool        INFO phi uniformity correction = 1
ToolSvc.METSigAutoConf_EgammaCalibrationAndSmearingTool        INFO gain correction = 0
ToolSvc.METSigAutoConf_EgammaCalibrationAndSmearingTool        INFO smearing = 2
ToolSvc.METSigAutoConf_EgammaCalibrationAndSmearingTool        INFO insitu scales = 2
ToolSvc.METSigAutoConf_EgammaCalibrationAndSmearingTool        INFO ep combination = 0
ToolSvc.METSigAutoConf_EgammaCalibrationAndSmearingTool        INFO use MVA calibration = 2
ToolSvc.METSigAutoConf_EgammaCalibrationAndSmearingTool        INFO use temperature correction 2015 = 2
ToolSvc.METSigAutoConf_EgammaCalibrationAndSmearingTool        INFO use uA2MeV correction 2015 1/2 week = 2
ToolSvc.xAODConfigTool                                         INFO Initialising...
ToolSvc.TrigDecisionTool                                       INFO Initializing Trig::TrigDecisionTool (standalone version even for athena)
MetaDataSvc                                                    INFO Initializing MetaDataSvc - package version AthenaServices-00-00-00
AthenaPoolCnvSvc                                               INFO Initializing AthenaPoolCnvSvc - package version AthenaPoolCnvSvc-00-00-00
DBReplicaSvc                                                   INFO Read replica configuration from /usr/AthAnalysis/21.2.108/InstallArea/x86_64-centos7-gcc8-opt/share/dbreplica.config
DBReplicaSvc                                                   INFO No specific match for domain found - use default fallback
DBReplicaSvc                                                   INFO Total of 1 servers found for host reana-run-job-b21ae857-0c66-41a2-9729-924db16aad44-pz6hh [atlas_dd ]
EventSelector                                                  INFO Selector configured to read [1] file(s)...
xAOD::Init                INFO    Environment initialised for data access
EventSelector                                                  INFO Using BRANCH xAOD access mode
TClass::GetCheckSum       ERROR   Calculating the checksum for (pair<unsigned int,string>) requires the base class (__pair_base<unsigned int,string>) meta information to be available!
EventPersistencySvc                                            INFO Added successfully Conversion service:Athena::xAODCnvSvc
EventPersistencySvc                                            INFO Added successfully Conversion service:AthenaPoolCnvSvc
ProxyProviderSvc                                               INFO Initializing ProxyProviderSvc - package version SGComps-00-00-00
ToolSvc.TrigDecisionTool                                       INFO Initialized TDT
ToolSvc.TrigGlobal_diLep                                       INFO Initializing ToolSvc.TrigGlobal_diLep...
ToolSvc.TrigGlobal_diLep                                       INFO Initialization successful
ToolSvc.TrigGlobal_multiLep                                    INFO Initializing ToolSvc.TrigGlobal_multiLep...
ToolSvc.TrigGlobal_multiLep                                    INFO Initialization successful
ToolSvc.TrigGlobal_diPhoton                                    INFO Initializing ToolSvc.TrigGlobal_diPhoton...
ToolSvc.TrigGlobal_diPhoton                                    INFO Initialization successful
ToolSvc.IsoCorrTool                                            INFO in initialize of ToolSvc.IsoCorrTool...
ToolSvc.IsoCorrTool                                            INFO  Path found = /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/IsolationCorrections/v1/isolation_ptcorrections_rel20_2.root
ToolSvc.IsoCorrTool                                            INFO  Path found = /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/IsolationCorrections/v3/isolation_ddcorrection_shift.root
ToolSvc.IsoCorrTool                                            INFO  Path found = /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/IsolationCorrections/v1/isolation_ddcorrection_smearing.root
ToolSvc.IsoTool                                                INFO Initialising...
ToolSvc.IsoTool                                                INFO Reading input file IsolationSelection/v2/MC15_Z_Jpsi_cutMap.root from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/IsolationSelection/v2/MC15_Z_Jpsi_cutMap.root
ToolSvc.IsoTool                                                INFO VersionInfo:2015_Sep_29_v2a
ToolSvc.IsoBaselineTool                                        INFO Initialising...
ToolSvc.IsoBaselineTool                                        INFO Reading input file IsolationSelection/v2/MC15_Z_Jpsi_cutMap.root from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/IsolationSelection/v2/MC15_Z_Jpsi_cutMap.root
ToolSvc.IsoBaselineTool                                        INFO VersionInfo:2015_Sep_29_v2a
ToolSvc.IsoHighPtTool                                          INFO Initialising...
ToolSvc.IsoHighPtTool                                          INFO Reading input file IsolationSelection/v2/MC15_Z_Jpsi_cutMap.root from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/IsolationSelection/v2/MC15_Z_Jpsi_cutMap.root
ToolSvc.IsoHighPtTool                                          INFO VersionInfo:2015_Sep_29_v2a
ToolSvc.IsoCloseByTool                                         INFO The following isolation cones are considered for Electron
ToolSvc.IsoCloseByTool                                         INFO No TrackSelectionTool provided, so I will create and configure my own, called: TrackParticleSelectionTool
ToolSvc.TrackParticleSelectionTool                             INFO Initializing track selection tool.
ToolSvc.TrackParticleSelectionTool                             INFO Cut level set to "Loose".
ToolSvc.TrackParticleSelectionTool                             INFO This will not overwrite other cuts that have been set.
ToolSvc.TrackParticleSelectionTool                             INFO   Minimum Pt: 500 MeV
ToolSvc.TrackParticleSelectionTool                             INFO   Maximum |Eta|: 2.5
ToolSvc.TrackParticleSelectionTool                             INFO   Maximum z0*sin(theta): 3 mm
ToolSvc.TrackParticleSelectionTool                             INFO   Maximum pixel holes: 1
ToolSvc.TrackParticleSelectionTool                             INFO   Minimum silicon (pixel + SCT) hits: 7
ToolSvc.TrackParticleSelectionTool                             INFO   Maximum silicon holes: 2
ToolSvc.TrackParticleSelectionTool                             INFO   No more than 1 shared Si module(s)
ToolSvc.TrackParticleSelectionTool                             INFO     (where a "shared module" is 1 shared pixel hit or 2 shared SCT hits)
ToolSvc.ttva_selection_tool                                    INFO  Initializing TrackVertexAssociationTool
ToolSvc.ttva_selection_tool                                    INFO TVA working point 'Loose' provided - tool properties have been configured accordingly
ToolSvc.ttva_selection_tool                                    INFO Cut on d0: -1   (d0_cut)
ToolSvc.ttva_selection_tool                                    INFO Cut on Δz * sin θ: 3    (dzSinTheta_cut)
ToolSvc.ttva_selection_tool                                    INFO Allow UsedInFit MatchStatus: 1  (doUsedInFit)
ToolSvc.ttva_selection_tool                                    INFO Require VxType::PriVtx for unique match: 1  (requirePriVtx)
ToolSvc.IsoCloseByTool                                         INFO The following isolation cones are considered for Electron
ToolSvc.IsoCloseByTool                                         INFO      --- ptvarcone20_TightTTVA_pt1000
ToolSvc.IsoCloseByTool                                         INFO      --- topoetcone20
ToolSvc.IsoCloseByTool                                         INFO The following isolation cones are considered for Muon
ToolSvc.IsoCloseByTool                                         INFO      --- ptvarcone30_TightTTVA_pt1000
ToolSvc.IsoCloseByTool                                         INFO      --- topoetcone20
ToolSvc.IsoCloseByTool                                         INFO The following isolation cones are considered for Photon
ToolSvc.IsoCloseByTool                                         INFO      --- topoetcone40
ToolSvc.IsoCloseByTool                                         INFO      --- ptcone20
ToolSvc.SUSYTools                                              INFO SUSYTools: Autoconfiguring ORToolTau
asg::ToolStore::put       WARNING Tool with name "ToolSvc.SUSYTools.ORToolTau.MuPFJetORT" already registered
asg::ToolStore::put       WARNING Tool with name "ToolSvc.SUSYTools.ORToolTau.EleMuORT" already registered
asg::ToolStore::put       WARNING Tool with name "ToolSvc.SUSYTools.ORToolTau.EleJetORT" already registered
asg::ToolStore::put       WARNING Tool with name "ToolSvc.SUSYTools.ORToolTau.MuJetORT" already registered
asg::ToolStore::put       WARNING Tool with name "ToolSvc.SUSYTools.ORToolTau.TauEleORT" already registered
asg::ToolStore::put       WARNING Tool with name "ToolSvc.SUSYTools.ORToolTau.TauMuORT" already registered
asg::ToolStore::put       WARNING Tool with name "ToolSvc.SUSYTools.ORToolTau.TauJetORT" already registered
asg::ToolStore::put       WARNING Tool with name "ToolSvc.SUSYTools.ORToolTau.EleFatJetORT" already registered
asg::ToolStore::put       WARNING Tool with name "ToolSvc.SUSYTools.ORToolTau.JetFatJetORT" already registered
ToolSvc.PMGSHVjetReweighter                                    INFO Initializing ToolSvc.PMGSHVjetReweighter...
ToolSvc.PMGSHVjetReweighterWZ                                  INFO Initializing ToolSvc.PMGSHVjetReweighterWZ...
ToolSvc.SUSYTools                                              INFO Done initialising SUSYTools
ToolSvc.SystematicsTool                                        INFO Add new systematic service XAMPP.SrvSUSYTools
ToolSvc.SUSYTools                                              INFO Extracting systematics info list
ToolSvc.SUSYTools                                              INFO Returning list of 285 systematic variations
ToolSvc.SystematicsTool                                        INFO Add new systematic service MetTriggerSF
ToolSvc.EventInfoHandler                                       INFO initialize...
ToolSvc.EventInfoHandler                                       INFO Create new common event variable PassGRL
ToolSvc.EventInfoHandler                                       INFO Create new common event variable HasVtx
ToolSvc.EventInfoHandler                                       INFO Create new common event variable passLArTile
ToolSvc.EventInfoHandler                                       INFO Create new common event variable Vtx_n
ToolSvc.EventInfoHandler                                       INFO Create new common event variable eventNumber
ToolSvc.EventInfoHandler                                       INFO Create new common event variable runNumber
ToolSvc.EventInfoHandler                                       INFO Create new common event variable pixelFlags
ToolSvc.EventInfoHandler                                       INFO Create new common event variable sctFlags
ToolSvc.EventInfoHandler                                       INFO Create new common event variable trtFlags
ToolSvc.EventInfoHandler                                       INFO Create new common event variable larFlags
ToolSvc.EventInfoHandler                                       INFO Create new common event variable tileFlags
ToolSvc.EventInfoHandler                                       INFO Create new common event variable muonFlags
ToolSvc.EventInfoHandler                                       INFO Create new common event variable forwardDetFlags
ToolSvc.EventInfoHandler                                       INFO Create new common event variable coreFlags
ToolSvc.EventInfoHandler                                       INFO Create new common event variable backgroundFlags
ToolSvc.EventInfoHandler                                       INFO Create new common event variable lumiFlags
ToolSvc.EventInfoHandler                                       INFO Create new common event variable averageInteractionsPerCrossing
ToolSvc.EventInfoHandler                                       INFO Create new common event variable actualInteractionsPerCrossing
ToolSvc.EventInfoHandler                                       INFO Create new common event variable bcid
ToolSvc.EventInfoHandler                                       INFO Create new common event variable lumiBlock
ToolSvc.EventInfoHandler                                       INFO No special treatment for outlier gen weights configured
ToolSvc.EventInfoHandler                                       INFO Create new common event variable mu_density
ToolSvc.EventInfoHandler                                       INFO Create new common event variable muWeight_PRW_DATASF__1down
ToolSvc.EventInfoHandler                                       INFO Create new common event variable corr_avgIntPerX_PRW_DATASF__1down
ToolSvc.EventInfoHandler                                       INFO Create new common event variable RandomRunNumber_PRW_DATASF__1down
ToolSvc.EventInfoHandler                                       INFO Create new common event variable RandomLumiBlockNumber_PRW_DATASF__1down
ToolSvc.EventInfoHandler                                       INFO Create new common event variable muWeight_PRW_DATASF__1up
ToolSvc.EventInfoHandler                                       INFO Create new common event variable corr_avgIntPerX_PRW_DATASF__1up
ToolSvc.EventInfoHandler                                       INFO Create new common event variable RandomRunNumber_PRW_DATASF__1up
ToolSvc.EventInfoHandler                                       INFO Create new common event variable RandomLumiBlockNumber_PRW_DATASF__1up
ToolSvc.EventInfoHandler                                       INFO Create new common event variable muWeight
ToolSvc.EventInfoHandler                                       INFO Create new common event variable corr_avgIntPerX
ToolSvc.EventInfoHandler                                       INFO Create new common event variable RandomRunNumber
ToolSvc.EventInfoHandler                                       INFO Create new common event variable RandomLumiBlockNumber
ToolSvc.PMGCrossSectionTool                                    INFO Initializing ToolSvc.PMGCrossSectionTool...
ToolSvc.MetaDataTree                                           INFO Initialising...
ToolSvc.ParticleConstructor                                    INFO Initialising...
ToolSvc.MonoHTruthSelector                                     INFO Initialising...
ToolSvc.EventInfoHandler                                       INFO Create new common event variable TruthMET
ToolSvc.MonoHTruthSelector                                     INFO Load object definitions of Muon.
ToolSvc.MonoHTruthSelector                                     INFO Load object definitions of Electron.
ToolSvc.MonoHTruthSelector                                     INFO Load object definitions of Tau.
ToolSvc.MonoHTruthSelector                                     INFO Load object definitions of Jet.
ToolSvc.MonoHTruthSelector                                     INFO Load object definitions of Photon.
ToolSvc.MonoHTruthSelector                                     INFO Load object definitions of Neutrino.
ToolSvc.TriggerTool                                            INFO Initialising...
ToolSvc.TriggerTool                                            INFO Trigger decision and matching will be taken from SUSYTools
ToolSvc.EventInfoHandler                                       INFO Create new common event variable Trigger
StorageKeeper::Register() INFO    Found new Info object ToolSvc.EventInfoHandler
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrigMatching
ToolSvc.EventInfoHandler                                       INFO Create new common event variable TrigHLT_xe70_mht
TriggerInterface:::add... INFO    HLT_xe70_mht is going to be constrained to runs of data-taking 266904 - 284484
ToolSvc.EventInfoHandler                                       INFO Create new common event variable TrigHLT_xe90_mht_L1XE50
TriggerInterface:::add... INFO    HLT_xe90_mht_L1XE50 is going to be constrained to runs of data-taking 296939 - 302872
ToolSvc.EventInfoHandler                                       INFO Create new common event variable TrigHLT_xe110_pufit_L1XE55
TriggerInterface:::add... INFO    HLT_xe110_pufit_L1XE55 is going to be constrained to runs of data-taking 325713 - 331975
ToolSvc.EventInfoHandler                                       INFO Create new common event variable TrigHLT_xe110_pufit_L1XE50
TriggerInterface:::add... INFO    HLT_xe110_pufit_L1XE50 is going to be constrained to runs of data-taking 332303 - 341649
ToolSvc.EventInfoHandler                                       INFO Create new common event variable TrigHLT_xe110_mht_L1XE50
TriggerInterface:::add... INFO    HLT_xe110_mht_L1XE50 is going to be constrained to runs of data-taking 302919 - 311481
ToolSvc.EventInfoHandler                                       INFO Create new common event variable TrigHLT_xe110_pufit_xe70_L1XE50
TriggerInterface:::add... INFO    HLT_xe110_pufit_xe70_L1XE50 is going to be constrained to runs of data-taking 348885 - 364485
ToolSvc.EventInfoHandler                                       INFO Create new common event variable TrigHLT_e24_lhmedium_L1EM20VH
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrigMatchHLT_e24_lhmedium_L1EM20VH
TriggerInterface:::add... INFO    HLT_e24_lhmedium_L1EM20VH is going to be constrained to runs of data-taking 266904 - 284484
ToolSvc.EventInfoHandler                                       INFO Create new common event variable TrigHLT_e60_lhmedium
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrigMatchHLT_e60_lhmedium
TriggerInterface:::add... INFO    HLT_e60_lhmedium is going to be constrained to runs of data-taking 266904 - 284484
ToolSvc.EventInfoHandler                                       INFO Create new common event variable TrigHLT_e120_lhloose
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrigMatchHLT_e120_lhloose
TriggerInterface:::add... INFO    HLT_e120_lhloose is going to be constrained to runs of data-taking 266904 - 284484
ToolSvc.EventInfoHandler                                       INFO Create new common event variable TrigHLT_mu20_iloose_L1MU15
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrigMatchHLT_mu20_iloose_L1MU15
TriggerInterface:::add... INFO    HLT_mu20_iloose_L1MU15 is going to be constrained to runs of data-taking 266904 - 284484
ToolSvc.EventInfoHandler                                       INFO Create new common event variable TrigHLT_mu50
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrigMatchHLT_mu50
TriggerInterface:::add... INFO    HLT_mu50 is going to be constrained to runs of data-taking 305380 - 311481
TriggerInterface:::add... INFO    HLT_mu50 is going to be constrained to runs of data-taking 324320 - 341649
TriggerInterface:::add... INFO    HLT_mu50 is going to be constrained to runs of data-taking 300345 - 302872
TriggerInterface:::add... INFO    HLT_mu50 is going to be constrained to runs of data-taking 296939 - 300287
TriggerInterface:::add... INFO    HLT_mu50 is going to be constrained to runs of data-taking 348197 - 364485
TriggerInterface:::add... INFO    HLT_mu50 is going to be constrained to runs of data-taking 303943 - 305379
ToolSvc.EventInfoHandler                                       INFO Create new common event variable TrigHLT_e60_lhmedium_nod0
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrigMatchHLT_e60_lhmedium_nod0
TriggerInterface:::add... INFO    HLT_e60_lhmedium_nod0 is going to be constrained to runs of data-taking 302919 - 311481
TriggerInterface:::add... INFO    HLT_e60_lhmedium_nod0 is going to be constrained to runs of data-taking 296939 - 302872
TriggerInterface:::add... INFO    HLT_e60_lhmedium_nod0 is going to be constrained to runs of data-taking 349169 - 364485
TriggerInterface:::add... INFO    HLT_e60_lhmedium_nod0 is going to be constrained to runs of data-taking 324320 - 341649
TriggerInterface:::add... INFO    HLT_e60_lhmedium_nod0 is going to be constrained to runs of data-taking 348197 - 349168
ToolSvc.EventInfoHandler                                       INFO Create new common event variable TrigHLT_e140_lhloose_nod0
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrigMatchHLT_e140_lhloose_nod0
TriggerInterface:::add... INFO    HLT_e140_lhloose_nod0 is going to be constrained to runs of data-taking 302919 - 311481
TriggerInterface:::add... INFO    HLT_e140_lhloose_nod0 is going to be constrained to runs of data-taking 296939 - 302872
TriggerInterface:::add... INFO    HLT_e140_lhloose_nod0 is going to be constrained to runs of data-taking 349169 - 364485
TriggerInterface:::add... INFO    HLT_e140_lhloose_nod0 is going to be constrained to runs of data-taking 324320 - 341649
TriggerInterface:::add... INFO    HLT_e140_lhloose_nod0 is going to be constrained to runs of data-taking 348197 - 349168
ToolSvc.EventInfoHandler                                       INFO Create new common event variable TrigHLT_mu40
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrigMatchHLT_mu40
TriggerInterface:::add... INFO    HLT_mu40 is going to be constrained to runs of data-taking 266904 - 284484
TriggerInterface:::add... INFO    HLT_mu40 is going to be constrained to runs of data-taking 296939 - 300287
ToolSvc.EventInfoHandler                                       INFO Create new common event variable TrigHLT_mu24_ivarmedium
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrigMatchHLT_mu24_ivarmedium
TriggerInterface:::add... INFO    HLT_mu24_ivarmedium is going to be constrained to runs of data-taking 300345 - 302872
ToolSvc.EventInfoHandler                                       INFO Create new common event variable TrigHLT_mu26_ivarmedium
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrigMatchHLT_mu26_ivarmedium
TriggerInterface:::add... INFO    HLT_mu26_ivarmedium is going to be constrained to runs of data-taking 305380 - 311481
TriggerInterface:::add... INFO    HLT_mu26_ivarmedium is going to be constrained to runs of data-taking 324320 - 341649
TriggerInterface:::add... INFO    HLT_mu26_ivarmedium is going to be constrained to runs of data-taking 348197 - 364485
TriggerInterface:::add... INFO    HLT_mu26_ivarmedium is going to be constrained to runs of data-taking 303943 - 305379
ToolSvc.EventInfoHandler                                       INFO Create new common event variable TrigHLT_e26_lhtight_nod0_ivarloose
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrigMatchHLT_e26_lhtight_nod0_ivarloose
TriggerInterface:::add... INFO    HLT_e26_lhtight_nod0_ivarloose is going to be constrained to runs of data-taking 302919 - 311481
TriggerInterface:::add... INFO    HLT_e26_lhtight_nod0_ivarloose is going to be constrained to runs of data-taking 349169 - 364485
TriggerInterface:::add... INFO    HLT_e26_lhtight_nod0_ivarloose is going to be constrained to runs of data-taking 324320 - 341649
TriggerInterface:::add... INFO    HLT_e26_lhtight_nod0_ivarloose is going to be constrained to runs of data-taking 348197 - 349168
ToolSvc.TriggerTool                                            INFO The following triggers are set in the tool:
ToolSvc.TriggerTool                                            INFO   - HLT_xe70_mht 
ToolSvc.TriggerTool                                            INFO   - HLT_xe90_mht_L1XE50 
ToolSvc.TriggerTool                                            INFO   - HLT_xe110_pufit_L1XE55 
ToolSvc.TriggerTool                                            INFO   - HLT_xe110_pufit_L1XE50 
ToolSvc.TriggerTool                                            INFO   - HLT_xe110_mht_L1XE50 
ToolSvc.TriggerTool                                            INFO   - HLT_xe110_pufit_xe70_L1XE50 
ToolSvc.TriggerTool                                            INFO   - HLT_e24_lhmedium_L1EM20VH pt(El)>25.00 GeV, 
ToolSvc.TriggerTool                                            INFO   - HLT_e60_lhmedium pt(El)>61.00 GeV, 
ToolSvc.TriggerTool                                            INFO   - HLT_e120_lhloose pt(El)>121.00 GeV, 
ToolSvc.TriggerTool                                            INFO   - HLT_mu20_iloose_L1MU15 pt(Mu)>21.00 GeV, 
ToolSvc.TriggerTool                                            INFO   - HLT_mu50 pt(Mu)>52.50 GeV, 
ToolSvc.TriggerTool                                            INFO   - HLT_e60_lhmedium_nod0 pt(El)>61.00 GeV, 
ToolSvc.TriggerTool                                            INFO   - HLT_e140_lhloose_nod0 pt(El)>141.00 GeV, 
ToolSvc.TriggerTool                                            INFO   - HLT_mu40 pt(Mu)>42.00 GeV, 
ToolSvc.TriggerTool                                            INFO   - HLT_mu24_ivarmedium pt(Mu)>25.20 GeV, 
ToolSvc.TriggerTool                                            INFO   - HLT_mu26_ivarmedium pt(Mu)>27.30 GeV, 
ToolSvc.TriggerTool                                            INFO   - HLT_e26_lhtight_nod0_ivarloose pt(El)>27.00 GeV, 
ToolSvc.EventInfoHandler                                       INFO Create new common event variable IsMETTrigPassed
ToolSvc.EventInfoHandler                                       INFO Create new common event variable IsSingleElecTrigPassed
ToolSvc.EventInfoHandler                                       INFO Create new event variable IsSingleElecTrigMatched
ToolSvc.EventInfoHandler                                       INFO Create new common event variable IsSingleMuonTrigPassed
ToolSvc.EventInfoHandler                                       INFO Create new event variable IsSingleMuonTrigMatched
ToolSvc.SUSYElectronSelector                                   INFO Initialising...
ToolSvc.SUSYElectronSelector                                   INFO Setup new SF tool using SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 as config with LooseBLayer and isolation FCLoose.
ToolSvc.SystematicsTool                                        INFO Add new systematic service ElectronTriggerSF LooseBLayer0
ToolSvc.EventInfoHandler                                       INFO Create new event variable EleWeightReco_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable EleWeightReco_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable EleWeightReco
ToolSvc.EventInfoHandler                                       INFO Create new event variable EleWeightId_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable EleWeightId_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable EleWeightId
ToolSvc.EventInfoHandler                                       INFO Create new event variable EleWeightIso_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable EleWeightIso_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable EleWeightIso
ToolSvc.EventInfoHandler                                       INFO Create new event variable EleWeightTrig_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_OR_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable EleWeightTrig_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_OR_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable EleWeightTrig_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_OR_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0
ToolSvc.EventInfoHandler                                       INFO Create new event variable EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable EleWeight
ToolSvc.SUSYMuonSelector                                       INFO Initialising...
ToolSvc.EventInfoHandler                                       INFO Create new event variable BadMuon
ToolSvc.EventInfoHandler                                       INFO Create new event variable CosmicMuon
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightReco_MUON_EFF_RECO_STAT_LOWPT__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightReco_MUON_EFF_RECO_STAT_LOWPT__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightReco_MUON_EFF_RECO_STAT__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightReco_MUON_EFF_RECO_STAT__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightReco_MUON_EFF_RECO_SYS_LOWPT__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightReco_MUON_EFF_RECO_SYS_LOWPT__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightReco_MUON_EFF_RECO_SYS__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightReco_MUON_EFF_RECO_SYS__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightReco
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightIsol_MUON_EFF_ISO_STAT__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightIsol_MUON_EFF_ISO_STAT__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightIsol_MUON_EFF_ISO_SYS__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightIsol_MUON_EFF_ISO_SYS__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightIsol
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTTVA_MUON_EFF_TTVA_STAT__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTTVA_MUON_EFF_TTVA_STAT__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTTVA_MUON_EFF_TTVA_SYS__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTTVA_MUON_EFF_TTVA_SYS__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTTVA
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeight_MUON_EFF_ISO_STAT__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeight_MUON_EFF_ISO_STAT__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeight_MUON_EFF_ISO_SYS__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeight_MUON_EFF_ISO_SYS__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeight_MUON_EFF_RECO_STAT_LOWPT__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeight_MUON_EFF_RECO_STAT_LOWPT__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeight_MUON_EFF_RECO_STAT__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeight_MUON_EFF_RECO_STAT__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeight_MUON_EFF_RECO_SYS_LOWPT__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeight_MUON_EFF_RECO_SYS_LOWPT__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeight_MUON_EFF_RECO_SYS__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeight_MUON_EFF_RECO_SYS__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeight_MUON_EFF_TTVA_STAT__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeight_MUON_EFF_TTVA_STAT__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeight_MUON_EFF_TTVA_SYS__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeight_MUON_EFF_TTVA_SYS__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MuoWeight
ToolSvc.SUSYPhotonSelector                                     INFO Initialising...
ToolSvc.SUSYPhotonSelector                                     INFO Selector has been deactivated
ToolSvc.SUSYTauSelector                                        INFO Initialising...
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeightId_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeightId_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeightId_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeightId_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeightId_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeightId_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeightId_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeightId_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeightId_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeightId_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeightId
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TauWeight
ToolSvc.SUSYJetSelector                                        INFO Initialising...
ToolSvc.SUSYJetSelector                                        INFO Select signal objects with p_{T} >20 GeV and |eta| <2.5.
ToolSvc.SUSYJetSelector                                        INFO Yeah, we're running over a sample with time-stamped containers... Open the stage for AntiKt4EMPFlowJets_BTagging201810
ToolSvc.SUSYJetSelector                                        INFO Use AntiKt4EMPFlowJets container for the standard AntiKt4 jets
ToolSvc.EventInfoHandler                                       INFO Create new event variable BadJet
ToolSvc.SystematicsTool                                        INFO Add new systematic service BTagEffTool_AntiKt4EMPFlowJets_BTagging201810_FixedCutBEff_77
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_B_0__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_B_0__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_B_1__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_B_1__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_B_2__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_B_2__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_C_0__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_C_0__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_C_1__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_C_1__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_C_2__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_C_2__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_C_3__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_C_3__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_Light_0__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_Light_0__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_Light_1__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_Light_1__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_Light_2__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_Light_2__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_Light_3__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_Light_3__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_Light_4__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_Eigen_Light_4__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_extrapolation__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_extrapolation__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_extrapolation_from_charm__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag_FT_EFF_extrapolation_from_charm__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightJVT
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightBTag
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightJVT_JET_JvtEfficiency__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeightJVT_JET_JvtEfficiency__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_B_0__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_B_0__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_B_1__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_B_1__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_B_2__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_B_2__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_C_0__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_C_0__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_Light_1__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_C_1__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_C_1__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_C_2__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_C_2__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_C_3__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_C_3__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_Light_0__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_Light_0__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_Light_1__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_Light_2__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_Light_2__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_Light_3__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_Light_3__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_Light_4__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_Eigen_Light_4__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_extrapolation__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_extrapolation__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_extrapolation_from_charm__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_FT_EFF_extrapolation_from_charm__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_JET_JvtEfficiency__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable JetWeight_JET_JvtEfficiency__1up
ToolSvc.SystematicsTool                                        INFO Add new systematic service BTagEffTool_AntiKtVR30Rmax4Rmin02TrackJets_FixedCutBEff_77
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_B_0__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_B_0__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_B_0__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_B_0__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_B_1__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_B_1__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_B_1__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_B_1__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_B_2__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_B_2__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_B_2__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_B_2__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_C_0__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_C_0__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_C_0__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_C_0__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_C_1__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_C_1__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_C_1__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_C_1__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_C_2__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_C_2__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_C_2__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_C_2__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_C_3__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_C_3__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_C_3__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_C_3__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_Light_0__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_Light_0__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_Light_0__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_Light_0__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_Light_1__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_Light_1__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_Light_1__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_Light_1__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_Light_2__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_Light_2__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_Light_2__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_Light_2__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_Light_3__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_Light_3__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_Light_3__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_Light_3__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_Light_4__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_Light_4__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_Eigen_Light_4__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_Eigen_Light_4__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_extrapolation__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_extrapolation__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_extrapolation__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_extrapolation__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_extrapolation_from_charm__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_extrapolation_from_charm__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag_FT_EFF_extrapolation_from_charm__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight_FT_EFF_extrapolation_from_charm__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeightBTag
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJetWeight
ToolSvc.SUSYMetSelector                                        INFO Initialising...
ToolSvc.EventInfoHandler                                       INFO Create new event variable MetTST
ToolSvc.MetSignificanceTool                                    INFO Initializing ToolSvc.MetSignificanceTool...
ToolSvc.MetSignificanceTool                                    INFO Set up JER tools
ToolSvc.MetSignificanceTool                                    INFO Set up jet resolution tool
ToolSvc.jetCalibTool_AntiKt4EMTopo                             INFO Initializing ToolSvc.jetCalibTool_AntiKt4EMTopo...
ToolSvc.jetCalibTool_AntiKt4EMTopo                             INFO ===================================
ToolSvc.jetCalibTool_AntiKt4EMTopo                             INFO Initializing the xAOD Jet Calibration Tool for AntiKt4EMTopojets
ToolSvc.jetCalibTool_AntiKt4EMTopo                             INFO Reading global JES settings from: JES_data2017_2016_2015_Recommendation_Aug2018_rel21.config
ToolSvc.jetCalibTool_AntiKt4EMTopo                             INFO resolved in: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetCalibTools/CalibArea-00-04-81/CalibrationConfigs/JES_data2017_2016_2015_Recommendation_Aug2018_rel21.config
ToolSvc.jetCalibTool_AntiKt4EMTopo                             INFO Initializing pileup correction.
ToolSvc.jetCalibTool_AntiKt4EMTopo_Pileup                      INFO JetPileupCorrection: Starting scale: JetConstitScaleMomentum
ToolSvc.jetCalibTool_AntiKt4EMTopo_Pileup                      INFO Jet area pile up correction will be applied.
ToolSvc.jetCalibTool_AntiKt4EMTopo_Pileup_Residual             INFO Reading residual jet-area pile-up correction factors from: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetCalibTools/CalibArea-00-04-81/CalibrationFactors/MC16a_Residual_4EM_4LC.config
ToolSvc.jetCalibTool_AntiKt4EMTopo_Pileup_Residual             INFO Description: MC16 residual mu- and N_{PV}-dependent jet pileup corrections for R=0.4 EMTopo and LCTopo
ToolSvc.jetCalibTool_AntiKt4EMTopo                             INFO Initializing JES correction.
ToolSvc.jetCalibTool_AntiKt4EMTopo_EtaJES                      INFO Reading absolute calibration factors from: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetCalibTools/CalibArea-00-04-81/CalibrationFactors/MC16a_MCJES_4EM_4LC_Oct2017.config
ToolSvc.jetCalibTool_AntiKt4EMTopo_EtaJES                      INFO Description: MCJES derived in October 2017 for MC16
ToolSvc.jetCalibTool_AntiKt4EMTopo                             INFO Initializing GSC correction.
ToolSvc.jetCalibTool_AntiKt4EMTopo_GSC                         INFO Initializing the Global Sequential Calibration tool
ToolSvc.jetCalibTool_AntiKt4EMTopo_GSC                         INFO GSC Tool has been initialized with binning and eta fit factors from: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetCalibTools/CalibArea-00-04-81/CalibrationFactors/MC16a_GSC_R21_EMLC_28Nov2017.root
ToolSvc.jetCalibTool_AntiKt4EMTopo                             INFO Initializing jet smearing correction
ToolSvc.jetCalibTool_AntiKt4EMTopo_Smear                       INFO Initializing the jet smearing correction tool
ToolSvc.jetCalibTool_AntiKt4EMTopo_Smear                       INFO Reading from JetGSCScaleMomentum and writing to JetSmearedMomentum
ToolSvc.jetCalibTool_AntiKt4EMTopo                             INFO ===================================
ToolSvc.MetSignificanceTool                                    INFO Set up MuonCalibrationAndSmearing tools
ToolSvc.EventInfoHandler                                       INFO Create new event variable MetTST_Significance
ToolSvc.EventInfoHandler                                       INFO Create new event variable MetTST_Significance_Rho
ToolSvc.EventInfoHandler                                       INFO Create new event variable MetTST_Significance_VarL
ToolSvc.EventInfoHandler                                       INFO Create new event variable MetTST_OverSqrtSumET
ToolSvc.EventInfoHandler                                       INFO Create new event variable MetTST_OverSqrtHT
ToolSvc.MetSignificanceTool_noPUJets_noSoftTerm                INFO Initializing ToolSvc.MetSignificanceTool_noPUJets_noSoftTerm...
ToolSvc.MetSignificanceTool_noPUJets_noSoftTerm                INFO Set up JER tools
ToolSvc.MetSignificanceTool_noPUJets_noSoftTerm                INFO Set up jet resolution tool
ToolSvc.MetSignificanceTool_noPUJets_noSoftTerm                INFO Set up MuonCalibrationAndSmearing tools
ToolSvc.EventInfoHandler                                       INFO Create new event variable MetTST_Significance_noPUJets_noSoftTerm
ToolSvc.EventInfoHandler                                       INFO Create new event variable MetTST_Significance_noPUJets_noSoftTerm_Rho
ToolSvc.EventInfoHandler                                       INFO Create new event variable MetTST_Significance_noPUJets_noSoftTerm_VarL
ToolSvc.MetSignificanceTool_noPUJets_noSoftTerm_lepInvis       INFO Initializing ToolSvc.MetSignificanceTool_noPUJets_noSoftTerm_lepInvis...
ToolSvc.MetSignificanceTool_noPUJets_noSoftTerm_lepInvis       INFO Set up JER tools
ToolSvc.MetSignificanceTool_noPUJets_noSoftTerm_lepInvis       INFO Set up jet resolution tool
ToolSvc.MetSignificanceTool_noPUJets_noSoftTerm_lepInvis       INFO Set up MuonCalibrationAndSmearing tools
ToolSvc.EventInfoHandler                                       INFO Create new event variable MetTST_Significance_noPUJets_noSoftTerm_lepInvis
ToolSvc.EventInfoHandler                                       INFO Create new event variable MetTST_Significance_noPUJets_noSoftTerm_lepInvis_Rho
ToolSvc.EventInfoHandler                                       INFO Create new event variable MetTST_Significance_noPUJets_noSoftTerm_lepInvis_VarL
ToolSvc.EventInfoHandler                                       INFO Create new event variable MetTSTlepInvis
ToolSvc.EventInfoHandler                                       INFO Create new event variable MET_TriggerSF_METTrigStat__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MET_TriggerSF_METTrigStat__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MET_TriggerSF_METTrigSyst__1down
ToolSvc.EventInfoHandler                                       INFO Create new event variable MET_TriggerSF_METTrigSyst__1up
ToolSvc.EventInfoHandler                                       INFO Create new event variable MET_TriggerSF
ToolSvc.SystematicsTool                                        INFO Add new systematic service BTagEffTool_AntiKtVR30Rmax4Rmin02TrackJets_FixedCutBEff_77
ToolSvc.SystematicsTool                                        INFO Initialising...
ToolSvc.SystematicsTool                                        INFO Setting doNoPhotons to true...
ToolSvc.SystematicsTool                                        INFO Systematics affecting the kinematics: 
ToolSvc.SystematicsTool                                        INFO - 
ToolSvc.SystematicsTool                                        INFO - MET_SoftTrk_ResoPara
ToolSvc.SystematicsTool                                        INFO - MET_SoftTrk_ResoPerp
ToolSvc.SystematicsTool                                        INFO - MET_SoftTrk_ScaleDown
ToolSvc.SystematicsTool                                        INFO - MET_SoftTrk_ScaleUp
ToolSvc.SystematicsTool                                        INFO - EG_RESOLUTION_ALL__1down
ToolSvc.SystematicsTool                                        INFO - EG_RESOLUTION_ALL__1up
ToolSvc.SystematicsTool                                        INFO - EG_SCALE_AF2__1down
ToolSvc.SystematicsTool                                        INFO - EG_SCALE_AF2__1up
ToolSvc.SystematicsTool                                        INFO - EG_SCALE_ALL__1down
ToolSvc.SystematicsTool                                        INFO - EG_SCALE_ALL__1up
ToolSvc.SystematicsTool                                        INFO - JET_BJES_Response__1down
ToolSvc.SystematicsTool                                        INFO - JET_BJES_Response__1up
ToolSvc.SystematicsTool                                        INFO - JET_CombMass_Baseline__1down
ToolSvc.SystematicsTool                                        INFO - JET_CombMass_Baseline__1up
ToolSvc.SystematicsTool                                        INFO - JET_CombMass_Modelling__1down
ToolSvc.SystematicsTool                                        INFO - JET_CombMass_Modelling__1up
ToolSvc.SystematicsTool                                        INFO - JET_CombMass_TotalStat__1down
ToolSvc.SystematicsTool                                        INFO - JET_CombMass_TotalStat__1up
ToolSvc.SystematicsTool                                        INFO - JET_CombMass_Tracking1__1down
ToolSvc.SystematicsTool                                        INFO - JET_CombMass_Tracking1__1up
ToolSvc.SystematicsTool                                        INFO - JET_CombMass_Tracking2__1down
ToolSvc.SystematicsTool                                        INFO - JET_CombMass_Tracking2__1up
ToolSvc.SystematicsTool                                        INFO - JET_CombMass_Tracking3__1down
ToolSvc.SystematicsTool                                        INFO - JET_CombMass_Tracking3__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Detector1__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Detector1__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Detector2__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Detector2__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Mixed1__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Mixed1__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Mixed2__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Mixed2__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Mixed3__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Mixed3__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Modelling1__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Modelling1__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Modelling2__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Modelling2__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Modelling3__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Modelling3__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Modelling4__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Modelling4__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Detector1__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Detector1__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Detector2__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Detector2__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Mixed1__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Mixed1__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Mixed2__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Mixed2__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Mixed3__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Mixed3__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Mixed4__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Mixed4__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Modelling1__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Modelling1__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Modelling2__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Modelling2__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Modelling3__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Modelling3__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Modelling4__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Modelling4__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Statistical1__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Statistical1__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Statistical2__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Statistical2__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Statistical3__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Statistical3__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Statistical4__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Statistical4__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Statistical5__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Statistical5__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Statistical6__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_R10_Statistical6__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Statistical1__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Statistical1__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Statistical2__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Statistical2__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Statistical3__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Statistical3__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Statistical4__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Statistical4__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Statistical5__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Statistical5__1up
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Statistical6__1down
ToolSvc.SystematicsTool                                        INFO - JET_EffectiveNP_Statistical6__1up
ToolSvc.SystematicsTool                                        INFO - JET_EtaIntercalibration_Modelling__1down
ToolSvc.SystematicsTool                                        INFO - JET_EtaIntercalibration_Modelling__1up
ToolSvc.SystematicsTool                                        INFO - JET_EtaIntercalibration_NonClosure_highE__1down
ToolSvc.SystematicsTool                                        INFO - JET_EtaIntercalibration_NonClosure_highE__1up
ToolSvc.SystematicsTool                                        INFO - JET_EtaIntercalibration_NonClosure_negEta__1down
ToolSvc.SystematicsTool                                        INFO - JET_EtaIntercalibration_NonClosure_negEta__1up
ToolSvc.SystematicsTool                                        INFO - JET_EtaIntercalibration_NonClosure_posEta__1down
ToolSvc.SystematicsTool                                        INFO - JET_EtaIntercalibration_NonClosure_posEta__1up
ToolSvc.SystematicsTool                                        INFO - JET_EtaIntercalibration_R10_TotalStat__1down
ToolSvc.SystematicsTool                                        INFO - JET_EtaIntercalibration_R10_TotalStat__1up
ToolSvc.SystematicsTool                                        INFO - JET_EtaIntercalibration_TotalStat__1down
ToolSvc.SystematicsTool                                        INFO - JET_EtaIntercalibration_TotalStat__1up
ToolSvc.SystematicsTool                                        INFO - JET_Flavor_Composition__1down
ToolSvc.SystematicsTool                                        INFO - JET_Flavor_Composition__1up
ToolSvc.SystematicsTool                                        INFO - JET_Flavor_Response__1down
ToolSvc.SystematicsTool                                        INFO - JET_Flavor_Response__1up
ToolSvc.SystematicsTool                                        INFO - JET_JER_DataVsMC_MC16__1down
ToolSvc.SystematicsTool                                        INFO - JET_JER_DataVsMC_MC16__1up
ToolSvc.SystematicsTool                                        INFO - JET_JER_EffectiveNP_1__1down
ToolSvc.SystematicsTool                                        INFO - JET_JER_EffectiveNP_1__1up
ToolSvc.SystematicsTool                                        INFO - JET_JER_EffectiveNP_2__1down
ToolSvc.SystematicsTool                                        INFO - JET_JER_EffectiveNP_2__1up
ToolSvc.SystematicsTool                                        INFO - JET_JER_EffectiveNP_3__1down
ToolSvc.SystematicsTool                                        INFO - JET_JER_EffectiveNP_3__1up
ToolSvc.SystematicsTool                                        INFO - JET_JER_EffectiveNP_4__1down
ToolSvc.SystematicsTool                                        INFO - JET_JER_EffectiveNP_4__1up
ToolSvc.SystematicsTool                                        INFO - JET_JER_EffectiveNP_5__1down
ToolSvc.SystematicsTool                                        INFO - JET_JER_EffectiveNP_5__1up
ToolSvc.SystematicsTool                                        INFO - JET_JER_EffectiveNP_6__1down
ToolSvc.SystematicsTool                                        INFO - JET_JER_EffectiveNP_6__1up
ToolSvc.SystematicsTool                                        INFO - JET_JER_EffectiveNP_7restTerm__1down
ToolSvc.SystematicsTool                                        INFO - JET_JER_EffectiveNP_7restTerm__1up
ToolSvc.SystematicsTool                                        INFO - JET_LargeR_TopologyUncertainty_V__1down
ToolSvc.SystematicsTool                                        INFO - JET_LargeR_TopologyUncertainty_V__1up
ToolSvc.SystematicsTool                                        INFO - JET_LargeR_TopologyUncertainty_top__1down
ToolSvc.SystematicsTool                                        INFO - JET_LargeR_TopologyUncertainty_top__1up
ToolSvc.SystematicsTool                                        INFO - JET_MassRes_Hbb_comb__1down
ToolSvc.SystematicsTool                                        INFO - JET_MassRes_Hbb_comb__1up
ToolSvc.SystematicsTool                                        INFO - JET_MassRes_Top_comb__1down
ToolSvc.SystematicsTool                                        INFO - JET_MassRes_Top_comb__1up
ToolSvc.SystematicsTool                                        INFO - JET_MassRes_WZ_comb__1down
ToolSvc.SystematicsTool                                        INFO - JET_MassRes_WZ_comb__1up
ToolSvc.SystematicsTool                                        INFO - JET_Pileup_OffsetMu__1down
ToolSvc.SystematicsTool                                        INFO - JET_Pileup_OffsetMu__1up
ToolSvc.SystematicsTool                                        INFO - JET_Pileup_OffsetNPV__1down
ToolSvc.SystematicsTool                                        INFO - JET_Pileup_OffsetNPV__1up
ToolSvc.SystematicsTool                                        INFO - JET_Pileup_PtTerm__1down
ToolSvc.SystematicsTool                                        INFO - JET_Pileup_PtTerm__1up
ToolSvc.SystematicsTool                                        INFO - JET_Pileup_RhoTopology__1down
ToolSvc.SystematicsTool                                        INFO - JET_Pileup_RhoTopology__1up
ToolSvc.SystematicsTool                                        INFO - JET_PunchThrough_MC16__1down
ToolSvc.SystematicsTool                                        INFO - JET_PunchThrough_MC16__1up
ToolSvc.SystematicsTool                                        INFO - JET_Rtrk_Baseline_mass__1down
ToolSvc.SystematicsTool                                        INFO - JET_Rtrk_Baseline_mass__1up
ToolSvc.SystematicsTool                                        INFO - JET_Rtrk_Baseline_pT__1down
ToolSvc.SystematicsTool                                        INFO - JET_Rtrk_Baseline_pT__1up
ToolSvc.SystematicsTool                                        INFO - JET_Rtrk_Closure_mass__1down
ToolSvc.SystematicsTool                                        INFO - JET_Rtrk_Closure_mass__1up
ToolSvc.SystematicsTool                                        INFO - JET_Rtrk_Closure_pT__1down
ToolSvc.SystematicsTool                                        INFO - JET_Rtrk_Closure_pT__1up
ToolSvc.SystematicsTool                                        INFO - JET_Rtrk_Modelling_mass__1down
ToolSvc.SystematicsTool                                        INFO - JET_Rtrk_Modelling_mass__1up
ToolSvc.SystematicsTool                                        INFO - JET_Rtrk_Modelling_pT__1down
ToolSvc.SystematicsTool                                        INFO - JET_Rtrk_Modelling_pT__1up
ToolSvc.SystematicsTool                                        INFO - JET_Rtrk_TotalStat_mass__1down
ToolSvc.SystematicsTool                                        INFO - JET_Rtrk_TotalStat_mass__1up
ToolSvc.SystematicsTool                                        INFO - JET_Rtrk_TotalStat_pT__1down
ToolSvc.SystematicsTool                                        INFO - JET_Rtrk_TotalStat_pT__1up
ToolSvc.SystematicsTool                                        INFO - JET_Rtrk_Tracking_mass__1down
ToolSvc.SystematicsTool                                        INFO - JET_Rtrk_Tracking_mass__1up
ToolSvc.SystematicsTool                                        INFO - JET_Rtrk_Tracking_pT__1down
ToolSvc.SystematicsTool                                        INFO - JET_Rtrk_Tracking_pT__1up
ToolSvc.SystematicsTool                                        INFO - JET_SingleParticle_HighPt__1down
ToolSvc.SystematicsTool                                        INFO - JET_SingleParticle_HighPt__1up
ToolSvc.SystematicsTool                                        INFO - MUON_ID__1down
ToolSvc.SystematicsTool                                        INFO - MUON_ID__1up
ToolSvc.SystematicsTool                                        INFO - MUON_MS__1down
ToolSvc.SystematicsTool                                        INFO - MUON_MS__1up
ToolSvc.SystematicsTool                                        INFO - MUON_SAGITTA_RESBIAS__1down
ToolSvc.SystematicsTool                                        INFO - MUON_SAGITTA_RESBIAS__1up
ToolSvc.SystematicsTool                                        INFO - MUON_SAGITTA_RHO__1down
ToolSvc.SystematicsTool                                        INFO - MUON_SAGITTA_RHO__1up
ToolSvc.SystematicsTool                                        INFO - MUON_SCALE__1down
ToolSvc.SystematicsTool                                        INFO - MUON_SCALE__1up
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_SME_TES_DETECTOR__1down
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_SME_TES_DETECTOR__1up
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_SME_TES_INSITUEXP__1down
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_SME_TES_INSITUEXP__1up
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_SME_TES_INSITUFIT__1down
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_SME_TES_INSITUFIT__1up
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_SME_TES_MODEL__1down
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_SME_TES_MODEL__1up
ToolSvc.SystematicsTool                                        INFO Systematics affecting the Electron ScaleFactor: 
ToolSvc.SystematicsTool                                        INFO - EL_CHARGEID_STAT__1down
ToolSvc.SystematicsTool                                        INFO - EL_CHARGEID_STAT__1up
ToolSvc.SystematicsTool                                        INFO - EL_CHARGEID_SYStotal__1down
ToolSvc.SystematicsTool                                        INFO - EL_CHARGEID_SYStotal__1up
ToolSvc.SystematicsTool                                        INFO - EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down
ToolSvc.SystematicsTool                                        INFO - EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up
ToolSvc.SystematicsTool                                        INFO - EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down
ToolSvc.SystematicsTool                                        INFO - EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up
ToolSvc.SystematicsTool                                        INFO - EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down
ToolSvc.SystematicsTool                                        INFO - EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up
ToolSvc.SystematicsTool                                        INFO - EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down
ToolSvc.SystematicsTool                                        INFO - EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up
ToolSvc.SystematicsTool                                        INFO - EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down
ToolSvc.SystematicsTool                                        INFO - EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up
ToolSvc.SystematicsTool                                        INFO - EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down
ToolSvc.SystematicsTool                                        INFO - EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up
ToolSvc.SystematicsTool                                        INFO - 
ToolSvc.SystematicsTool                                        INFO Systematics affecting the Muon ScaleFactor: 
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_BADMUON_SYS__1down
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_BADMUON_SYS__1up
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_ISO_STAT__1down
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_ISO_STAT__1up
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_ISO_SYS__1down
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_ISO_SYS__1up
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_RECO_STAT_LOWPT__1down
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_RECO_STAT_LOWPT__1up
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_RECO_STAT__1down
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_RECO_STAT__1up
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_RECO_SYS_LOWPT__1down
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_RECO_SYS_LOWPT__1up
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_RECO_SYS__1down
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_RECO_SYS__1up
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_TTVA_STAT__1down
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_TTVA_STAT__1up
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_TTVA_SYS__1down
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_TTVA_SYS__1up
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_TrigStatUncertainty__1down
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_TrigStatUncertainty__1up
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_TrigSystUncertainty__1down
ToolSvc.SystematicsTool                                        INFO - MUON_EFF_TrigSystUncertainty__1up
ToolSvc.SystematicsTool                                        INFO - 
ToolSvc.SystematicsTool                                        INFO Systematics affecting the Tau ScaleFactor: 
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2015__1down
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2015__1up
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2015__1down
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2015__1up
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2015__1down
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2015__1up
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down
ToolSvc.SystematicsTool                                        INFO - TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up
ToolSvc.SystematicsTool                                        INFO - 
ToolSvc.SystematicsTool                                        INFO Systematics affecting the Jet ScaleFactor: 
ToolSvc.SystematicsTool                                        INFO - JET_JvtEfficiency__1down
ToolSvc.SystematicsTool                                        INFO - JET_JvtEfficiency__1up
ToolSvc.SystematicsTool                                        INFO - JET_fJvtEfficiency__1down
ToolSvc.SystematicsTool                                        INFO - JET_fJvtEfficiency__1up
ToolSvc.SystematicsTool                                        INFO - 
ToolSvc.SystematicsTool                                        INFO Systematics affecting the BTag ScaleFactor: 
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_B_0__1down
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_B_0__1up
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_B_1__1down
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_B_1__1up
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_B_2__1down
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_B_2__1up
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_C_0__1down
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_C_0__1up
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_C_1__1down
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_C_1__1up
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_C_2__1down
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_C_2__1up
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_C_3__1down
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_C_3__1up
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_Light_0__1down
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_Light_0__1up
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_Light_1__1down
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_Light_1__1up
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_Light_2__1down
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_Light_2__1up
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_Light_3__1down
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_Light_3__1up
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_Light_4__1down
ToolSvc.SystematicsTool                                        INFO - FT_EFF_Eigen_Light_4__1up
ToolSvc.SystematicsTool                                        INFO - FT_EFF_extrapolation__1down
ToolSvc.SystematicsTool                                        INFO - FT_EFF_extrapolation__1up
ToolSvc.SystematicsTool                                        INFO - FT_EFF_extrapolation_from_charm__1down
ToolSvc.SystematicsTool                                        INFO - FT_EFF_extrapolation_from_charm__1up
ToolSvc.SystematicsTool                                        INFO - 
ToolSvc.SystematicsTool                                        INFO Systematics affecting the MET ScaleFactor: 
ToolSvc.SystematicsTool                                        INFO - METTrigStat__1down
ToolSvc.SystematicsTool                                        INFO - METTrigStat__1up
ToolSvc.SystematicsTool                                        INFO - METTrigSyst__1down
ToolSvc.SystematicsTool                                        INFO - METTrigSyst__1up
ToolSvc.SystematicsTool                                        INFO - PRW_DATASF__1down
ToolSvc.SystematicsTool                                        INFO - PRW_DATASF__1up
ToolSvc.SystematicsTool                                        INFO - 
ToolSvc.SystematicsTool                                        INFO Systematics affecting the Event weight: 
ToolSvc.SystematicsTool                                        INFO - PRW_DATASF__1down
ToolSvc.SystematicsTool                                        INFO - PRW_DATASF__1up
ToolSvc.SystematicsTool                                        INFO - 
ToolSvc.EventInfoHandler                                       INFO Create new common event variable SUSYFinalState
ToolSvc.EventInfoHandler                                       INFO Create new common event variable HasPathologicalWeight
ToolSvc.EventInfoHandler                                       INFO Create new common event variable GenWeight
ToolSvc.EventInfoHandler                                       INFO Create new common event variable GenWeightMCSampleMerging
ToolSvc.EventInfoHandler                                       INFO Create new event variable GenFiltMET
TriggerInterface()        WARNING Trigger has no matching requirement
TriggerInterface()        WARNING Trigger has no matching requirement
TriggerInterface()        WARNING Trigger has no matching requirement
TriggerInterface()        WARNING Trigger has no matching requirement
TriggerInterface()        WARNING Trigger has no matching requirement
TriggerInterface()        WARNING Trigger has no matching requirement
ToolSvc.EventInfoHandler                                       INFO Create new event variable DFCommonCut
ToolSvc.EventInfoHandler                                       INFO Create new event variable N_BaselineLeptons
ToolSvc.EventInfoHandler                                       INFO Create new event variable N_SignalLeptons
ToolSvc.EventInfoHandler                                       INFO Create new event variable N_BaselineElectrons
ToolSvc.EventInfoHandler                                       INFO Create new event variable N_SignalElectrons
ToolSvc.EventInfoHandler                                       INFO Create new event variable N_BaselineMuons
ToolSvc.EventInfoHandler                                       INFO Create new event variable N_SignalMuons
ToolSvc.EventInfoHandler                                       INFO Create new event variable N_SignalTaus
ToolSvc.EventInfoHandler                                       INFO Create new event variable N_Jets04
ToolSvc.EventInfoHandler                                       INFO Create new event variable N_ForwardJets04
ToolSvc.EventInfoHandler                                       INFO Create new event variable N_AllJets04
ToolSvc.EventInfoHandler                                       INFO Create new event variable N_BJets_04
ToolSvc.EventInfoHandler                                       INFO Create new event variable N_BTags_associated_02
ToolSvc.EventInfoHandler                                       INFO Create new event variable N_BTags_not_associated_02
ToolSvc.EventInfoHandler                                       INFO Create new event variable N_associated_Jets02
ToolSvc.EventInfoHandler                                       INFO Create new event variable N_not_associated_Jets02
ToolSvc.EventInfoHandler                                       INFO Create new event variable N_Jets10
ToolSvc.EventInfoHandler                                       INFO Create new event variable Jet_BLight1BPt
ToolSvc.EventInfoHandler                                       INFO Create new event variable Jet_BLight2BPt
ToolSvc.EventInfoHandler                                       INFO Create new event variable Jet_BLight1isBjet
ToolSvc.EventInfoHandler                                       INFO Create new event variable Jet_BLight2isBjet
ToolSvc.EventInfoHandler                                       INFO Create new event variable Jet_BLight1TruthLabel
ToolSvc.EventInfoHandler                                       INFO Create new event variable Jet_BLight2TruthLabel
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJet_1isBjet
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJet_2isBjet
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJet_1passOR
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJet_2passOR
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJet_1TruthLabel
ToolSvc.EventInfoHandler                                       INFO Create new event variable TrackJet_2TruthLabel
ToolSvc.EventInfoHandler                                       INFO Create new event variable m_jj
ToolSvc.EventInfoHandler                                       INFO Create new event variable m_J
ToolSvc.EventInfoHandler                                       INFO Create new event variable pt_jj
ToolSvc.EventInfoHandler                                       INFO Create new event variable eta_jj
ToolSvc.EventInfoHandler                                       INFO Create new event variable phi_jj
ToolSvc.EventInfoHandler                                       INFO Create new event variable pt_jj_corr
ToolSvc.EventInfoHandler                                       INFO Create new event variable eta_jj_corr
ToolSvc.EventInfoHandler                                       INFO Create new event variable phi_jj_corr
ToolSvc.EventInfoHandler                                       INFO Create new event variable m_jj_corr
ToolSvc.EventInfoHandler                                       INFO Create new event variable pt_J_corr
ToolSvc.EventInfoHandler                                       INFO Create new event variable eta_J_corr
ToolSvc.EventInfoHandler                                       INFO Create new event variable phi_J_corr
ToolSvc.EventInfoHandler                                       INFO Create new event variable m_J_corr
ToolSvc.EventInfoHandler                                       INFO Create new event variable pt_mu1
ToolSvc.EventInfoHandler                                       INFO Create new event variable pt_e1
ToolSvc.EventInfoHandler                                       INFO Create new event variable m_ll
ToolSvc.EventInfoHandler                                       INFO Create new event variable pt_ll
ToolSvc.EventInfoHandler                                       INFO Create new event variable isOppositeCharge
ToolSvc.EventInfoHandler                                       INFO Create new event variable AbsDeltaPhiMin3
ToolSvc.EventInfoHandler                                       INFO Create new event variable DeltaPhiMin3
ToolSvc.EventInfoHandler                                       INFO Create new event variable N_TausExtended_Resolved
ToolSvc.EventInfoHandler                                       INFO Create new event variable N_TausExtended_Merged
ToolSvc.EventInfoHandler                                       INFO Create new event variable mT_METclosestBJet
ToolSvc.EventInfoHandler                                       INFO Create new event variable mT_METfurthestBJet
ToolSvc.EventInfoHandler                                       INFO Book new branches to store particles Jet
ToolSvc.EventInfoHandler                                       INFO Book new branches to store particles ForwardJet
ToolSvc.EventInfoHandler                                       INFO Book new branches to store particles FatJet
ToolSvc.EventInfoHandler                                       INFO Book new common branches to store particles TrackJet
ToolSvc.EventInfoHandler                                       INFO Book new branches to store particles Electron
ToolSvc.EventInfoHandler                                       INFO Book new branches to store particles Muon
ToolSvc.EventInfoHandler                                    WARNING The event info is already initialized
ScalarCutElement::init... INFO    The element has been succesfully initialized using PassGRL = 1
ScalarCutElement::init... INFO    The element has been succesfully initialized using passLArTile = 1
ScalarCutElement::init... INFO    The element has been succesfully initialized using Trigger = 1
ScalarCutElement::init... INFO    The element has been succesfully initialized using HasVtx = 1
ScalarCutElement::init... INFO    The element has been succesfully initialized using BadJet = 0
ScalarCutElement::init... INFO    The element has been succesfully initialized using CosmicMuon = 0
ScalarCutElement::init... INFO    The element has been succesfully initialized using BadMuon = 0
ScalarCutElement::init... INFO    The element has been succesfully initialized using DFCommonCut = 1
ScalarCutElement::init... INFO    The element has been succesfully initialized using IsMETTrigPassed = 1
ScalarCutElement::init... INFO    The element has been succesfully initialized using N_BaselineElectrons = 0
ScalarCutElement::init... INFO    The element has been succesfully initialized using N_BaselineMuons = 0
ScalarCutElement::init... INFO    The element has been succesfully initialized using N_SignalTaus = 0
ScalarCutElement::init... INFO    The element has been succesfully initialized using N_TausExtended_Resolved = 0
XAMPPMetCutElement::in... INFO    The element has been succesfully initialized using MetTST > 150000.000000
XAMPPMetCutElement::in... INFO    The element has been succesfully initialized using MetTST <= 500000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using N_Jets04 >= 2
ScalarCutElement::init... INFO    The element has been succesfully initialized using N_BJets_04 >= 2
ScalarCutElement::init... INFO    The element has been succesfully initialized using m_jj > 40000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using m_jj_corr > 40000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using m_jj < 70000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using m_jj_corr < 70000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using m_jj > 140000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using m_jj_corr > 140000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using m_jj > 50000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using m_jj < 280000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using AbsDeltaPhiMin3 >= 0.349066
ScalarCutElement::init... INFO    The element has been succesfully initialized using MetTST_Significance_noPUJets_noSoftTerm >= 12.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using pt_jj > 100000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using pt_jj_corr > 100000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using mT_METclosestBJet > 170000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using mT_METfurthestBJet > 200000.000000
ToolSvc.AnalysisConfig                                         INFO Adding CutFlow '0L_SR_Resolved' with the following cuts:
ToolSvc.AnalysisConfig                                         INFO - PassGRL
ToolSvc.AnalysisConfig                                         INFO - passLArTile
ToolSvc.AnalysisConfig                                         INFO - Trigger
ToolSvc.AnalysisConfig                                         INFO - HasVtx
ToolSvc.AnalysisConfig                                         INFO - BadJet
ToolSvc.AnalysisConfig                                         INFO - CosmicMuon
ToolSvc.AnalysisConfig                                         INFO - BadMuon
ToolSvc.AnalysisConfig                                         INFO - PFlow Electron veto 
ToolSvc.AnalysisConfig                                         INFO - IsMETTrigPassed
ToolSvc.AnalysisConfig                                         INFO - 0 baseline electrons
ToolSvc.AnalysisConfig                                         INFO - 0 baseline muons
ToolSvc.AnalysisConfig                                         INFO - Tau Veto
ToolSvc.AnalysisConfig                                         INFO - Extended Tau Veto
ToolSvc.AnalysisConfig                                         INFO - MetTST>150
ToolSvc.AnalysisConfig                                         INFO - MetTST<=500
ToolSvc.AnalysisConfig                                         INFO - >=2 jets
ToolSvc.AnalysisConfig                                         INFO - >=2 b-tags
ToolSvc.AnalysisConfig                                         INFO - (mjj > 40 || mjj_corr > 40)
ToolSvc.AnalysisConfig                                         INFO - (mjj > 50 && mjj < 280)
ToolSvc.AnalysisConfig                                         INFO - |DeltaPhiMin3|>20deg
ToolSvc.AnalysisConfig                                         INFO - METSig>12
ToolSvc.AnalysisConfig                                         INFO - (pt_jj > 100000 || pt_jj_corr > 100000)
ToolSvc.AnalysisConfig                                         INFO - mT_METclosestBJet > 170000
ToolSvc.AnalysisConfig                                         INFO - mT_METfurthestBJet > 200000
ScalarCutElement::init... INFO    The element has been succesfully initialized using N_TausExtended_Merged = 0
XAMPPMetCutElement::in... INFO    The element has been succesfully initialized using MetTST > 500000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using N_Jets10 >= 1
ScalarCutElement::init... INFO    The element has been succesfully initialized using N_BTags_associated_02 >= 2
ScalarCutElement::init... INFO    The element has been succesfully initialized using m_J > 40000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using m_J_corr > 40000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using m_J < 70000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using m_J_corr < 70000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using m_J > 140000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using m_J_corr > 140000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using m_J > 50000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using m_J < 270000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using N_associated_Jets02 >= 2
ScalarCutElement::init... INFO    The element has been succesfully initialized using TrackJet_1passOR = 1
ScalarCutElement::init... INFO    The element has been succesfully initialized using TrackJet_2passOR = 1
ToolSvc.AnalysisConfig                                         INFO Adding CutFlow '0L_SR_Merged' with the following cuts:
ToolSvc.AnalysisConfig                                         INFO - PassGRL
ToolSvc.AnalysisConfig                                         INFO - passLArTile
ToolSvc.AnalysisConfig                                         INFO - Trigger
ToolSvc.AnalysisConfig                                         INFO - HasVtx
ToolSvc.AnalysisConfig                                         INFO - BadJet
ToolSvc.AnalysisConfig                                         INFO - CosmicMuon
ToolSvc.AnalysisConfig                                         INFO - BadMuon
ToolSvc.AnalysisConfig                                         INFO - PFlow Electron veto 
ToolSvc.AnalysisConfig                                         INFO - IsMETTrigPassed
ToolSvc.AnalysisConfig                                         INFO - 0 baseline electrons
ToolSvc.AnalysisConfig                                         INFO - 0 baseline muons
ToolSvc.AnalysisConfig                                         INFO - Tau Veto
ToolSvc.AnalysisConfig                                         INFO - Extended Tau Veto
ToolSvc.AnalysisConfig                                         INFO - MetTST>500
ToolSvc.AnalysisConfig                                         INFO - >=1 fat-jets
ToolSvc.AnalysisConfig                                         INFO - >= 2 b-tagged track jets
ToolSvc.AnalysisConfig                                         INFO - (mJ > 40 || mJ_corr > 40)
ToolSvc.AnalysisConfig                                         INFO - (mJ> 50 && mJ < 270)
ToolSvc.AnalysisConfig                                         INFO - N_associated_trkJets>=2
ToolSvc.AnalysisConfig                                         INFO - (TrackJet_1passOR = true && TrackJet_2passOR = true)
ToolSvc.AnalysisConfig                                         INFO - |DeltaPhiMin3|>20deg
ScalarCutElement::init... INFO    The element has been succesfully initialized using IsSingleMuonTrigPassed = 1
ScalarCutElement::init... INFO    The element has been succesfully initialized using IsSingleMuonTrigMatched = 1
ScalarCutElement::init... INFO    The element has been succesfully initialized using N_BaselineMuons = 1
ScalarCutElement::init... INFO    The element has been succesfully initialized using N_SignalMuons = 1
ScalarCutElement::init... INFO    The element has been succesfully initialized using pt_mu1 >= 25000.000000
XAMPPMetCutElement::in... INFO    The element has been succesfully initialized using MetTSTlepInvis > 150000.000000
XAMPPMetCutElement::in... INFO    The element has been succesfully initialized using MetTSTlepInvis <= 500000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using MetTST_Significance_noPUJets_noSoftTerm_lepInvis >= 12.000000
ToolSvc.AnalysisConfig                                         INFO Adding CutFlow '1L_CR_Resolved' with the following cuts:
ToolSvc.AnalysisConfig                                         INFO - PassGRL
ToolSvc.AnalysisConfig                                         INFO - passLArTile
ToolSvc.AnalysisConfig                                         INFO - Trigger
ToolSvc.AnalysisConfig                                         INFO - HasVtx
ToolSvc.AnalysisConfig                                         INFO - BadJet
ToolSvc.AnalysisConfig                                         INFO - CosmicMuon
ToolSvc.AnalysisConfig                                         INFO - BadMuon
ToolSvc.AnalysisConfig                                         INFO - PFlow Electron veto 
ToolSvc.AnalysisConfig                                         INFO - IsMETTrigPassed
ToolSvc.AnalysisConfig                                         INFO - 0 baseline electrons
ToolSvc.AnalysisConfig                                         INFO - ((1 signal muon && pt_mu1 > 25 GeV) && 1 baseline muon)
ToolSvc.AnalysisConfig                                         INFO - Tau Veto
ToolSvc.AnalysisConfig                                         INFO - Extended Tau Veto
ToolSvc.AnalysisConfig                                         INFO - MetTSTlepInvis>150
ToolSvc.AnalysisConfig                                         INFO - MetTSTlepInvis<=500
ToolSvc.AnalysisConfig                                         INFO - >=2 jets
ToolSvc.AnalysisConfig                                         INFO - >=2 b-tags
ToolSvc.AnalysisConfig                                         INFO - (mjj > 40 || mjj_corr > 40)
ToolSvc.AnalysisConfig                                         INFO - (mjj > 50 && mjj < 280)
ToolSvc.AnalysisConfig                                         INFO - |DeltaPhiMin3|>20deg
ToolSvc.AnalysisConfig                                         INFO - METSiglepInvis>12
ToolSvc.AnalysisConfig                                         INFO - (pt_jj > 100000 || pt_jj_corr > 100000)
ToolSvc.AnalysisConfig                                         INFO - mT_METclosestBJet > 170000
ToolSvc.AnalysisConfig                                         INFO - mT_METfurthestBJet > 200000
XAMPPMetCutElement::in... INFO    The element has been succesfully initialized using MetTSTlepInvis > 500000.000000
ToolSvc.AnalysisConfig                                         INFO Adding CutFlow '1L_CR_Merged' with the following cuts:
ToolSvc.AnalysisConfig                                         INFO - PassGRL
ToolSvc.AnalysisConfig                                         INFO - passLArTile
ToolSvc.AnalysisConfig                                         INFO - Trigger
ToolSvc.AnalysisConfig                                         INFO - HasVtx
ToolSvc.AnalysisConfig                                         INFO - BadJet
ToolSvc.AnalysisConfig                                         INFO - CosmicMuon
ToolSvc.AnalysisConfig                                         INFO - BadMuon
ToolSvc.AnalysisConfig                                         INFO - PFlow Electron veto 
ToolSvc.AnalysisConfig                                         INFO - IsMETTrigPassed
ToolSvc.AnalysisConfig                                         INFO - 0 baseline electrons
ToolSvc.AnalysisConfig                                         INFO - ((1 signal muon && pt_mu1 > 25 GeV) && 1 baseline muon)
ToolSvc.AnalysisConfig                                         INFO - Tau Veto
ToolSvc.AnalysisConfig                                         INFO - Extended Tau Veto
ToolSvc.AnalysisConfig                                         INFO - MetTSTlepInvis>500
ToolSvc.AnalysisConfig                                         INFO - >=1 fat-jets
ToolSvc.AnalysisConfig                                         INFO - >= 2 b-tagged track jets
ToolSvc.AnalysisConfig                                         INFO - (mJ > 40 || mJ_corr > 40)
ToolSvc.AnalysisConfig                                         INFO - (mJ> 50 && mJ < 270)
ToolSvc.AnalysisConfig                                         INFO - N_associated_trkJets>=2
ToolSvc.AnalysisConfig                                         INFO - (TrackJet_1passOR = true && TrackJet_2passOR = true)
ToolSvc.AnalysisConfig                                         INFO - |DeltaPhiMin3|>20deg
ScalarCutElement::init... INFO    The element has been succesfully initialized using IsSingleElecTrigPassed = 1
ScalarCutElement::init... INFO    The element has been succesfully initialized using IsSingleElecTrigMatched = 1
ScalarCutElement::init... INFO    The element has been succesfully initialized using N_BaselineElectrons = 2
ScalarCutElement::init... INFO    The element has been succesfully initialized using N_SignalElectrons = 2
ScalarCutElement::init... INFO    The element has been succesfully initialized using pt_e1 >= 27000.000000
ScalarCutElement::init... INFO    The element has been succesfully initialized using isOppositeCharge = 1
ScalarCutElement::init... INFO    The element has been succesfully initialized using m_ll >= 81187.601562
ScalarCutElement::init... INFO    The element has been succesfully initialized using m_ll <= 101187.601562
ScalarCutElement::init... INFO    The element has been succesfully initialized using MetTST_Significance_noPUJets_noSoftTerm <= 5.000000
ToolSvc.AnalysisConfig                                         INFO Adding CutFlow '2L_CR_Ele_Resolved' with the following cuts:
ToolSvc.AnalysisConfig                                         INFO - PassGRL
ToolSvc.AnalysisConfig                                         INFO - passLArTile
ToolSvc.AnalysisConfig                                         INFO - Trigger
ToolSvc.AnalysisConfig                                         INFO - HasVtx
ToolSvc.AnalysisConfig                                         INFO - BadJet
ToolSvc.AnalysisConfig                                         INFO - CosmicMuon
ToolSvc.AnalysisConfig                                         INFO - BadMuon
ToolSvc.AnalysisConfig                                         INFO - PFlow Electron veto 
ToolSvc.AnalysisConfig                                         INFO - IsSingleElecTrigPassed
ToolSvc.AnalysisConfig                                         INFO - IsSingleElecTrigMatched
ToolSvc.AnalysisConfig                                         INFO - (((== 2 baseline electrons && == 2 signal electrons) && pt_e1 > 27 GeV) && 0 baseline muons)
ToolSvc.AnalysisConfig                                         INFO - 0 baseline muons
ToolSvc.AnalysisConfig                                         INFO - Tau Veto
ToolSvc.AnalysisConfig                                         INFO - Extended Tau Veto
ToolSvc.AnalysisConfig                                         INFO - >=2 jets
ToolSvc.AnalysisConfig                                         INFO - >=2 b-tags
ToolSvc.AnalysisConfig                                         INFO - (mjj > 40 || mjj_corr > 40)
ToolSvc.AnalysisConfig                                         INFO - (mjj > 50 && mjj < 280)
ToolSvc.AnalysisConfig                                         INFO - MetTSTlepInvis>150
ToolSvc.AnalysisConfig                                         INFO - MetTSTlepInvis<=500
ToolSvc.AnalysisConfig                                         INFO - opposite charge
ToolSvc.AnalysisConfig                                         INFO - (m_ll>=82 && m_ll<=102)
ToolSvc.AnalysisConfig                                         INFO - (pt_jj > 100000 || pt_jj_corr > 100000)
ToolSvc.AnalysisConfig                                         INFO - |DeltaPhiMin3|>20deg
ToolSvc.AnalysisConfig                                         INFO - METSiglepInvis>12
ToolSvc.AnalysisConfig                                         INFO - mT_METclosestBJet > 170000
ToolSvc.AnalysisConfig                                         INFO - mT_METfurthestBJet > 200000
ToolSvc.AnalysisConfig                                         INFO - METSig<=5
ScalarCutElement::init... INFO    The element has been succesfully initialized using N_BaselineMuons = 2
ScalarCutElement::init... INFO    The element has been succesfully initialized using N_SignalMuons = 2
ToolSvc.AnalysisConfig                                         INFO Adding CutFlow '2L_CR_Muo_Resolved' with the following cuts:
ToolSvc.AnalysisConfig                                         INFO - PassGRL
ToolSvc.AnalysisConfig                                         INFO - passLArTile
ToolSvc.AnalysisConfig                                         INFO - Trigger
ToolSvc.AnalysisConfig                                         INFO - HasVtx
ToolSvc.AnalysisConfig                                         INFO - BadJet
ToolSvc.AnalysisConfig                                         INFO - CosmicMuon
ToolSvc.AnalysisConfig                                         INFO - BadMuon
ToolSvc.AnalysisConfig                                         INFO - PFlow Electron veto 
ToolSvc.AnalysisConfig                                         INFO - IsSingleMuonTrigPassed
ToolSvc.AnalysisConfig                                         INFO - IsSingleMuonTrigMatched
ToolSvc.AnalysisConfig                                         INFO - 0 baseline electrons
ToolSvc.AnalysisConfig                                         INFO - (((== 2 baseline muons && == 2 signal muons) && pt_mu1 > 25 GeV) && 0 baseline electrons)
ToolSvc.AnalysisConfig                                         INFO - Tau Veto
ToolSvc.AnalysisConfig                                         INFO - Extended Tau Veto
ToolSvc.AnalysisConfig                                         INFO - >=2 jets
ToolSvc.AnalysisConfig                                         INFO - >=2 b-tags
ToolSvc.AnalysisConfig                                         INFO - (mjj > 40 || mjj_corr > 40)
ToolSvc.AnalysisConfig                                         INFO - (mjj > 50 && mjj < 280)
ToolSvc.AnalysisConfig                                         INFO - MetTSTlepInvis>150
ToolSvc.AnalysisConfig                                         INFO - MetTSTlepInvis<=500
ToolSvc.AnalysisConfig                                         INFO - opposite charge
ToolSvc.AnalysisConfig                                         INFO - (m_ll>=82 && m_ll<=102)
ToolSvc.AnalysisConfig                                         INFO - (pt_jj > 100000 || pt_jj_corr > 100000)
ToolSvc.AnalysisConfig                                         INFO - |DeltaPhiMin3|>20deg
ToolSvc.AnalysisConfig                                         INFO - METSiglepInvis>12
ToolSvc.AnalysisConfig                                         INFO - mT_METclosestBJet > 170000
ToolSvc.AnalysisConfig                                         INFO - mT_METfurthestBJet > 200000
ToolSvc.AnalysisConfig                                         INFO - METSig<=5
ToolSvc.AnalysisConfig                                         INFO Adding CutFlow '2L_CR_Ele_Merged' with the following cuts:
ToolSvc.AnalysisConfig                                         INFO - PassGRL
ToolSvc.AnalysisConfig                                         INFO - passLArTile
ToolSvc.AnalysisConfig                                         INFO - Trigger
ToolSvc.AnalysisConfig                                         INFO - HasVtx
ToolSvc.AnalysisConfig                                         INFO - BadJet
ToolSvc.AnalysisConfig                                         INFO - CosmicMuon
ToolSvc.AnalysisConfig                                         INFO - BadMuon
ToolSvc.AnalysisConfig                                         INFO - PFlow Electron veto 
ToolSvc.AnalysisConfig                                         INFO - IsSingleElecTrigPassed
ToolSvc.AnalysisConfig                                         INFO - IsSingleElecTrigMatched
ToolSvc.AnalysisConfig                                         INFO - (((== 2 baseline electrons && == 2 signal electrons) && pt_e1 > 27 GeV) && 0 baseline muons)
ToolSvc.AnalysisConfig                                         INFO - 0 baseline muons
ToolSvc.AnalysisConfig                                         INFO - Tau Veto
ToolSvc.AnalysisConfig                                         INFO - Extended Tau Veto
ToolSvc.AnalysisConfig                                         INFO - MetTSTlepInvis>500
ToolSvc.AnalysisConfig                                         INFO - >=1 fat-jets
ToolSvc.AnalysisConfig                                         INFO - >= 2 b-tagged track jets
ToolSvc.AnalysisConfig                                         INFO - (mJ > 40 || mJ_corr > 40)
ToolSvc.AnalysisConfig                                         INFO - (mJ> 50 && mJ < 270)
ToolSvc.AnalysisConfig                                         INFO - N_associated_trkJets>=2
ToolSvc.AnalysisConfig                                         INFO - (TrackJet_1passOR = true && TrackJet_2passOR = true)
ToolSvc.AnalysisConfig                                         INFO - opposite charge
ToolSvc.AnalysisConfig                                         INFO - (m_ll>=82 && m_ll<=102)
ToolSvc.AnalysisConfig                                         INFO - |DeltaPhiMin3|>20deg
ToolSvc.AnalysisConfig                                         INFO Adding CutFlow '2L_CR_Muo_Merged' with the following cuts:
ToolSvc.AnalysisConfig                                         INFO - PassGRL
ToolSvc.AnalysisConfig                                         INFO - passLArTile
ToolSvc.AnalysisConfig                                         INFO - Trigger
ToolSvc.AnalysisConfig                                         INFO - HasVtx
ToolSvc.AnalysisConfig                                         INFO - BadJet
ToolSvc.AnalysisConfig                                         INFO - CosmicMuon
ToolSvc.AnalysisConfig                                         INFO - BadMuon
ToolSvc.AnalysisConfig                                         INFO - PFlow Electron veto 
ToolSvc.AnalysisConfig                                         INFO - IsSingleMuonTrigPassed
ToolSvc.AnalysisConfig                                         INFO - IsSingleMuonTrigMatched
ToolSvc.AnalysisConfig                                         INFO - 0 baseline electrons
ToolSvc.AnalysisConfig                                         INFO - (((== 2 baseline muons && == 2 signal muons) && pt_mu1 > 25 GeV) && 0 baseline electrons)
ToolSvc.AnalysisConfig                                         INFO - Tau Veto
ToolSvc.AnalysisConfig                                         INFO - Extended Tau Veto
ToolSvc.AnalysisConfig                                         INFO - MetTSTlepInvis>500
ToolSvc.AnalysisConfig                                         INFO - >=1 fat-jets
ToolSvc.AnalysisConfig                                         INFO - >= 2 b-tagged track jets
ToolSvc.AnalysisConfig                                         INFO - (mJ > 40 || mJ_corr > 40)
ToolSvc.AnalysisConfig                                         INFO - (mJ> 50 && mJ < 270)
ToolSvc.AnalysisConfig                                         INFO - N_associated_trkJets>=2
ToolSvc.AnalysisConfig                                         INFO - (TrackJet_1passOR = true && TrackJet_2passOR = true)
ToolSvc.AnalysisConfig                                         INFO - opposite charge
ToolSvc.AnalysisConfig                                         INFO - (m_ll>=82 && m_ll<=102)
ToolSvc.AnalysisConfig                                         INFO - |DeltaPhiMin3|>20deg
ScalarCutElement::init... INFO    The element has been succesfully initialized using N_BaselineElectrons = 1
ScalarCutElement::init... INFO    The element has been succesfully initialized using N_SignalElectrons = 1
ToolSvc.AnalysisConfig                                         INFO Adding CutFlow '2L_CR_EMu_Resolved' with the following cuts:
ToolSvc.AnalysisConfig                                         INFO - PassGRL
ToolSvc.AnalysisConfig                                         INFO - passLArTile
ToolSvc.AnalysisConfig                                         INFO - Trigger
ToolSvc.AnalysisConfig                                         INFO - HasVtx
ToolSvc.AnalysisConfig                                         INFO - BadJet
ToolSvc.AnalysisConfig                                         INFO - CosmicMuon
ToolSvc.AnalysisConfig                                         INFO - BadMuon
ToolSvc.AnalysisConfig                                         INFO - PFlow Electron veto 
ToolSvc.AnalysisConfig                                         INFO - IsMETTrigPassed
ToolSvc.AnalysisConfig                                         INFO - ((1 signal muon && pt_mu1 > 25 GeV) && 1 baseline muon)
ToolSvc.AnalysisConfig                                         INFO - ((== 1 baseline electron && == 1 signal electron) && pt_e1 > 27 GeV)
ToolSvc.AnalysisConfig                                         INFO - opposite charge
ToolSvc.AnalysisConfig                                         INFO - Tau Veto
ToolSvc.AnalysisConfig                                         INFO - Extended Tau Veto
ToolSvc.AnalysisConfig                                         INFO - (MetTSTlepInvis>150 || MetTST>150)
ToolSvc.AnalysisConfig                                         INFO - >=2 jets
ToolSvc.AnalysisConfig                                         INFO - >=2 b-tags
ToolSvc.AnalysisConfig                                         INFO - (mjj > 40 || mjj_corr > 40)
ToolSvc.AnalysisConfig                                         INFO - (mjj > 50 && mjj < 280)
ToolSvc.AnalysisConfig                                         INFO - (pt_jj > 100000 || pt_jj_corr > 100000)
ToolSvc.AnalysisConfig                                         INFO Adding CutFlow '2L_CR_EMu_Merged' with the following cuts:
ToolSvc.AnalysisConfig                                         INFO - PassGRL
ToolSvc.AnalysisConfig                                         INFO - passLArTile
ToolSvc.AnalysisConfig                                         INFO - Trigger
ToolSvc.AnalysisConfig                                         INFO - HasVtx
ToolSvc.AnalysisConfig                                         INFO - BadJet
ToolSvc.AnalysisConfig                                         INFO - CosmicMuon
ToolSvc.AnalysisConfig                                         INFO - BadMuon
ToolSvc.AnalysisConfig                                         INFO - PFlow Electron veto 
ToolSvc.AnalysisConfig                                         INFO - ((1 signal muon && pt_mu1 > 25 GeV) && 1 baseline muon)
ToolSvc.AnalysisConfig                                         INFO - ((== 1 baseline electron && == 1 signal electron) && pt_e1 > 27 GeV)
ToolSvc.AnalysisConfig                                         INFO - opposite charge
ToolSvc.AnalysisConfig                                         INFO - Tau Veto
ToolSvc.AnalysisConfig                                         INFO - Extended Tau Veto
ToolSvc.AnalysisConfig                                         INFO - (MetTSTlepInvis>500 || MetTST>500)
ToolSvc.AnalysisConfig                                         INFO - >=1 fat-jets
ToolSvc.AnalysisConfig                                         INFO - >= 2 b-tagged track jets
ToolSvc.AnalysisConfig                                         INFO - (mJ > 40 || mJ_corr > 40)
ToolSvc.AnalysisConfig                                         INFO - (mJ> 50 && mJ < 270)
ToolSvc.AnalysisConfig                                         INFO - N_associated_trkJets>=2
ToolSvc.AnalysisConfig                                         INFO - (TrackJet_1passOR = true && TrackJet_2passOR = true)
ToolSvc.AnalysisHelper                                         INFO Current info: isData: 0, isAF2: 0, doTruth: 0, doPRW: 1
ToolSvc.AnalysisHelper                                         INFO Initialising MonoH AnalysisHelper...
HistogramPersistencySvc                                     WARNING Histograms saving not required.
AthenaEventLoopMgr                                             INFO Setup EventSelector service EventSelector
ActiveStoreSvc                                                 INFO Initializing ActiveStoreSvc - package version StoreGate-00-00-00
ToolSvc.xAODConfigTool                                         INFO Loaded configuration:
ToolSvc.xAODConfigTool                                         INFO   SMK = 2136, L1PSK = 35, HLTPSK = 160
ToolSvc.IsoCorrTool                                            INFO is MC = 1, use AFII = 0
TClass::GetCheckSum       ERROR   Calculating the checksum for (pair<unsigned int,string>) requires the base class (__pair_base<unsigned int,string>) meta information to be available!
ToolSvc.MetaDataTree                                           INFO Found new mcChannel 511459.  Create new MCMetaData entry.
MetaDataMC::LoadMetaDa... INFO    New process Id 0 found for mcChannelNumber 511459
MetaDataMC::LoadMetaDa... INFO    have 0 names 
ClassIDSvc                                                     INFO  getRegistryEntries: read 28363 CLIDRegistry entries for module ALL
AthenaEventLoopMgr                                             INFO   ===>>>  start of run 0    <<<===
AthenaEventLoopMgr                                             INFO   ===>>>  start processing event #0, run #0 0 events processed so far  <<<===
CP::TPileupReweighting... INFO    Initializing the subtool..
CP::TPileupReweighting... WARNING There is 94.338810% unrepresented data and 'IgnoreBadChannels' property is set to true. Will start ignoring channels until this is below the tolerance (5.000000%)
CP::TPileupReweighting... WARNING Ignoring channel 511459, which has 94.338810% unrepresented data (100.000000% of the total unrep data)
TreeBase::InitializeTr... INFO    The tree writes  MonoH_MET_SoftTrk_ResoPara kinematic systematics MET_SoftTrk_ResoPara. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_MET_SoftTrk_ResoPerp kinematic systematics MET_SoftTrk_ResoPerp. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_MET_SoftTrk_ScaleDown kinematic systematics MET_SoftTrk_ScaleDown. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_MET_SoftTrk_ScaleUp kinematic systematics MET_SoftTrk_ScaleUp. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_EG_RESOLUTION_ALL__1down kinematic systematics EG_RESOLUTION_ALL__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_EG_RESOLUTION_ALL__1up kinematic systematics EG_RESOLUTION_ALL__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_EG_SCALE_AF2__1down kinematic systematics EG_SCALE_AF2__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_EG_SCALE_AF2__1up kinematic systematics EG_SCALE_AF2__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_EG_SCALE_ALL__1down kinematic systematics EG_SCALE_ALL__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_EG_SCALE_ALL__1up kinematic systematics EG_SCALE_ALL__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_BJES_Response__1down kinematic systematics JET_BJES_Response__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_BJES_Response__1up kinematic systematics JET_BJES_Response__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_CombMass_Baseline__1down kinematic systematics JET_CombMass_Baseline__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_CombMass_Baseline__1up kinematic systematics JET_CombMass_Baseline__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_CombMass_Modelling__1down kinematic systematics JET_CombMass_Modelling__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_CombMass_Modelling__1up kinematic systematics JET_CombMass_Modelling__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_CombMass_TotalStat__1down kinematic systematics JET_CombMass_TotalStat__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_CombMass_TotalStat__1up kinematic systematics JET_CombMass_TotalStat__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_CombMass_Tracking1__1down kinematic systematics JET_CombMass_Tracking1__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_CombMass_Tracking1__1up kinematic systematics JET_CombMass_Tracking1__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_CombMass_Tracking2__1down kinematic systematics JET_CombMass_Tracking2__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_CombMass_Tracking2__1up kinematic systematics JET_CombMass_Tracking2__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_CombMass_Tracking3__1down kinematic systematics JET_CombMass_Tracking3__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_CombMass_Tracking3__1up kinematic systematics JET_CombMass_Tracking3__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Detector1__1down kinematic systematics JET_EffectiveNP_Detector1__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Detector1__1up kinematic systematics JET_EffectiveNP_Detector1__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Detector2__1down kinematic systematics JET_EffectiveNP_Detector2__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Detector2__1up kinematic systematics JET_EffectiveNP_Detector2__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Mixed1__1down kinematic systematics JET_EffectiveNP_Mixed1__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Mixed1__1up kinematic systematics JET_EffectiveNP_Mixed1__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Mixed2__1down kinematic systematics JET_EffectiveNP_Mixed2__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Mixed2__1up kinematic systematics JET_EffectiveNP_Mixed2__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Mixed3__1down kinematic systematics JET_EffectiveNP_Mixed3__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Mixed3__1up kinematic systematics JET_EffectiveNP_Mixed3__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Modelling1__1down kinematic systematics JET_EffectiveNP_Modelling1__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Modelling1__1up kinematic systematics JET_EffectiveNP_Modelling1__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Modelling2__1down kinematic systematics JET_EffectiveNP_Modelling2__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Modelling2__1up kinematic systematics JET_EffectiveNP_Modelling2__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Modelling3__1down kinematic systematics JET_EffectiveNP_Modelling3__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Modelling3__1up kinematic systematics JET_EffectiveNP_Modelling3__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Modelling4__1down kinematic systematics JET_EffectiveNP_Modelling4__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Modelling4__1up kinematic systematics JET_EffectiveNP_Modelling4__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Detector1__1down kinematic systematics JET_EffectiveNP_R10_Detector1__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Detector1__1up kinematic systematics JET_EffectiveNP_R10_Detector1__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Detector2__1down kinematic systematics JET_EffectiveNP_R10_Detector2__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Detector2__1up kinematic systematics JET_EffectiveNP_R10_Detector2__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Mixed1__1down kinematic systematics JET_EffectiveNP_R10_Mixed1__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Mixed1__1up kinematic systematics JET_EffectiveNP_R10_Mixed1__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Mixed2__1down kinematic systematics JET_EffectiveNP_R10_Mixed2__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Mixed2__1up kinematic systematics JET_EffectiveNP_R10_Mixed2__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Mixed3__1down kinematic systematics JET_EffectiveNP_R10_Mixed3__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Mixed3__1up kinematic systematics JET_EffectiveNP_R10_Mixed3__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Mixed4__1down kinematic systematics JET_EffectiveNP_R10_Mixed4__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Mixed4__1up kinematic systematics JET_EffectiveNP_R10_Mixed4__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Modelling1__1down kinematic systematics JET_EffectiveNP_R10_Modelling1__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Modelling1__1up kinematic systematics JET_EffectiveNP_R10_Modelling1__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Modelling2__1down kinematic systematics JET_EffectiveNP_R10_Modelling2__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Modelling2__1up kinematic systematics JET_EffectiveNP_R10_Modelling2__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Modelling3__1down kinematic systematics JET_EffectiveNP_R10_Modelling3__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Modelling3__1up kinematic systematics JET_EffectiveNP_R10_Modelling3__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Modelling4__1down kinematic systematics JET_EffectiveNP_R10_Modelling4__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Modelling4__1up kinematic systematics JET_EffectiveNP_R10_Modelling4__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Statistical1__1down kinematic systematics JET_EffectiveNP_R10_Statistical1__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Statistical1__1up kinematic systematics JET_EffectiveNP_R10_Statistical1__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Statistical2__1down kinematic systematics JET_EffectiveNP_R10_Statistical2__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Statistical2__1up kinematic systematics JET_EffectiveNP_R10_Statistical2__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Statistical3__1down kinematic systematics JET_EffectiveNP_R10_Statistical3__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Statistical3__1up kinematic systematics JET_EffectiveNP_R10_Statistical3__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Statistical4__1down kinematic systematics JET_EffectiveNP_R10_Statistical4__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Statistical4__1up kinematic systematics JET_EffectiveNP_R10_Statistical4__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Statistical5__1down kinematic systematics JET_EffectiveNP_R10_Statistical5__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Statistical5__1up kinematic systematics JET_EffectiveNP_R10_Statistical5__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Statistical6__1down kinematic systematics JET_EffectiveNP_R10_Statistical6__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_R10_Statistical6__1up kinematic systematics JET_EffectiveNP_R10_Statistical6__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Statistical1__1down kinematic systematics JET_EffectiveNP_Statistical1__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Statistical1__1up kinematic systematics JET_EffectiveNP_Statistical1__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Statistical2__1down kinematic systematics JET_EffectiveNP_Statistical2__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Statistical2__1up kinematic systematics JET_EffectiveNP_Statistical2__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Statistical3__1down kinematic systematics JET_EffectiveNP_Statistical3__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Statistical3__1up kinematic systematics JET_EffectiveNP_Statistical3__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Statistical4__1down kinematic systematics JET_EffectiveNP_Statistical4__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Statistical4__1up kinematic systematics JET_EffectiveNP_Statistical4__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Statistical5__1down kinematic systematics JET_EffectiveNP_Statistical5__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Statistical5__1up kinematic systematics JET_EffectiveNP_Statistical5__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Statistical6__1down kinematic systematics JET_EffectiveNP_Statistical6__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EffectiveNP_Statistical6__1up kinematic systematics JET_EffectiveNP_Statistical6__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EtaIntercalibration_Modelling__1down kinematic systematics JET_EtaIntercalibration_Modelling__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EtaIntercalibration_Modelling__1up kinematic systematics JET_EtaIntercalibration_Modelling__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EtaIntercalibration_NonClosure_highE__1down kinematic systematics JET_EtaIntercalibration_NonClosure_highE__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EtaIntercalibration_NonClosure_highE__1up kinematic systematics JET_EtaIntercalibration_NonClosure_highE__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EtaIntercalibration_NonClosure_negEta__1down kinematic systematics JET_EtaIntercalibration_NonClosure_negEta__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EtaIntercalibration_NonClosure_negEta__1up kinematic systematics JET_EtaIntercalibration_NonClosure_negEta__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EtaIntercalibration_NonClosure_posEta__1down kinematic systematics JET_EtaIntercalibration_NonClosure_posEta__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EtaIntercalibration_NonClosure_posEta__1up kinematic systematics JET_EtaIntercalibration_NonClosure_posEta__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EtaIntercalibration_R10_TotalStat__1down kinematic systematics JET_EtaIntercalibration_R10_TotalStat__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EtaIntercalibration_R10_TotalStat__1up kinematic systematics JET_EtaIntercalibration_R10_TotalStat__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EtaIntercalibration_TotalStat__1down kinematic systematics JET_EtaIntercalibration_TotalStat__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_EtaIntercalibration_TotalStat__1up kinematic systematics JET_EtaIntercalibration_TotalStat__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Flavor_Composition__1down kinematic systematics JET_Flavor_Composition__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Flavor_Composition__1up kinematic systematics JET_Flavor_Composition__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Flavor_Response__1down kinematic systematics JET_Flavor_Response__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Flavor_Response__1up kinematic systematics JET_Flavor_Response__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_JER_DataVsMC_MC16__1down kinematic systematics JET_JER_DataVsMC_MC16__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_JER_DataVsMC_MC16__1up kinematic systematics JET_JER_DataVsMC_MC16__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_JER_EffectiveNP_1__1down kinematic systematics JET_JER_EffectiveNP_1__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_JER_EffectiveNP_1__1up kinematic systematics JET_JER_EffectiveNP_1__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_JER_EffectiveNP_2__1down kinematic systematics JET_JER_EffectiveNP_2__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_JER_EffectiveNP_2__1up kinematic systematics JET_JER_EffectiveNP_2__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_JER_EffectiveNP_3__1down kinematic systematics JET_JER_EffectiveNP_3__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_JER_EffectiveNP_3__1up kinematic systematics JET_JER_EffectiveNP_3__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_JER_EffectiveNP_4__1down kinematic systematics JET_JER_EffectiveNP_4__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_JER_EffectiveNP_4__1up kinematic systematics JET_JER_EffectiveNP_4__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_JER_EffectiveNP_5__1down kinematic systematics JET_JER_EffectiveNP_5__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_JER_EffectiveNP_5__1up kinematic systematics JET_JER_EffectiveNP_5__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_JER_EffectiveNP_6__1down kinematic systematics JET_JER_EffectiveNP_6__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_JER_EffectiveNP_6__1up kinematic systematics JET_JER_EffectiveNP_6__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_JER_EffectiveNP_7restTerm__1down kinematic systematics JET_JER_EffectiveNP_7restTerm__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_JER_EffectiveNP_7restTerm__1up kinematic systematics JET_JER_EffectiveNP_7restTerm__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_LargeR_TopologyUncertainty_V__1down kinematic systematics JET_LargeR_TopologyUncertainty_V__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_LargeR_TopologyUncertainty_V__1up kinematic systematics JET_LargeR_TopologyUncertainty_V__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_LargeR_TopologyUncertainty_top__1down kinematic systematics JET_LargeR_TopologyUncertainty_top__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_LargeR_TopologyUncertainty_top__1up kinematic systematics JET_LargeR_TopologyUncertainty_top__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_MassRes_Hbb_comb__1down kinematic systematics JET_MassRes_Hbb_comb__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_MassRes_Hbb_comb__1up kinematic systematics JET_MassRes_Hbb_comb__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_MassRes_Top_comb__1down kinematic systematics JET_MassRes_Top_comb__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_MassRes_Top_comb__1up kinematic systematics JET_MassRes_Top_comb__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_MassRes_WZ_comb__1down kinematic systematics JET_MassRes_WZ_comb__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_MassRes_WZ_comb__1up kinematic systematics JET_MassRes_WZ_comb__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Pileup_OffsetMu__1down kinematic systematics JET_Pileup_OffsetMu__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Pileup_OffsetMu__1up kinematic systematics JET_Pileup_OffsetMu__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Pileup_OffsetNPV__1down kinematic systematics JET_Pileup_OffsetNPV__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Pileup_OffsetNPV__1up kinematic systematics JET_Pileup_OffsetNPV__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Pileup_PtTerm__1down kinematic systematics JET_Pileup_PtTerm__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Pileup_PtTerm__1up kinematic systematics JET_Pileup_PtTerm__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Pileup_RhoTopology__1down kinematic systematics JET_Pileup_RhoTopology__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Pileup_RhoTopology__1up kinematic systematics JET_Pileup_RhoTopology__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_PunchThrough_MC16__1down kinematic systematics JET_PunchThrough_MC16__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_PunchThrough_MC16__1up kinematic systematics JET_PunchThrough_MC16__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Rtrk_Baseline_mass__1down kinematic systematics JET_Rtrk_Baseline_mass__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Rtrk_Baseline_mass__1up kinematic systematics JET_Rtrk_Baseline_mass__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Rtrk_Baseline_pT__1down kinematic systematics JET_Rtrk_Baseline_pT__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Rtrk_Baseline_pT__1up kinematic systematics JET_Rtrk_Baseline_pT__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Rtrk_Closure_mass__1down kinematic systematics JET_Rtrk_Closure_mass__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Rtrk_Closure_mass__1up kinematic systematics JET_Rtrk_Closure_mass__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Rtrk_Closure_pT__1down kinematic systematics JET_Rtrk_Closure_pT__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Rtrk_Closure_pT__1up kinematic systematics JET_Rtrk_Closure_pT__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Rtrk_Modelling_mass__1down kinematic systematics JET_Rtrk_Modelling_mass__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Rtrk_Modelling_mass__1up kinematic systematics JET_Rtrk_Modelling_mass__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Rtrk_Modelling_pT__1down kinematic systematics JET_Rtrk_Modelling_pT__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Rtrk_Modelling_pT__1up kinematic systematics JET_Rtrk_Modelling_pT__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Rtrk_TotalStat_mass__1down kinematic systematics JET_Rtrk_TotalStat_mass__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Rtrk_TotalStat_mass__1up kinematic systematics JET_Rtrk_TotalStat_mass__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Rtrk_TotalStat_pT__1down kinematic systematics JET_Rtrk_TotalStat_pT__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Rtrk_TotalStat_pT__1up kinematic systematics JET_Rtrk_TotalStat_pT__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Rtrk_Tracking_mass__1down kinematic systematics JET_Rtrk_Tracking_mass__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Rtrk_Tracking_mass__1up kinematic systematics JET_Rtrk_Tracking_mass__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Rtrk_Tracking_pT__1down kinematic systematics JET_Rtrk_Tracking_pT__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_Rtrk_Tracking_pT__1up kinematic systematics JET_Rtrk_Tracking_pT__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_SingleParticle_HighPt__1down kinematic systematics JET_SingleParticle_HighPt__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_JET_SingleParticle_HighPt__1up kinematic systematics JET_SingleParticle_HighPt__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_MUON_ID__1down kinematic systematics MUON_ID__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_MUON_ID__1up kinematic systematics MUON_ID__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_MUON_MS__1down kinematic systematics MUON_MS__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_MUON_MS__1up kinematic systematics MUON_MS__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_MUON_SAGITTA_RESBIAS__1down kinematic systematics MUON_SAGITTA_RESBIAS__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_MUON_SAGITTA_RESBIAS__1up kinematic systematics MUON_SAGITTA_RESBIAS__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_MUON_SAGITTA_RHO__1down kinematic systematics MUON_SAGITTA_RHO__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_MUON_SAGITTA_RHO__1up kinematic systematics MUON_SAGITTA_RHO__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_MUON_SCALE__1down kinematic systematics MUON_SCALE__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_MUON_SCALE__1up kinematic systematics MUON_SCALE__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_TAUS_TRUEHADTAU_SME_TES_DETECTOR__1down kinematic systematics TAUS_TRUEHADTAU_SME_TES_DETECTOR__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_TAUS_TRUEHADTAU_SME_TES_DETECTOR__1up kinematic systematics TAUS_TRUEHADTAU_SME_TES_DETECTOR__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_TAUS_TRUEHADTAU_SME_TES_INSITUEXP__1down kinematic systematics TAUS_TRUEHADTAU_SME_TES_INSITUEXP__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_TAUS_TRUEHADTAU_SME_TES_INSITUEXP__1up kinematic systematics TAUS_TRUEHADTAU_SME_TES_INSITUEXP__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_TAUS_TRUEHADTAU_SME_TES_INSITUFIT__1down kinematic systematics TAUS_TRUEHADTAU_SME_TES_INSITUFIT__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_TAUS_TRUEHADTAU_SME_TES_INSITUFIT__1up kinematic systematics TAUS_TRUEHADTAU_SME_TES_INSITUFIT__1up. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_TAUS_TRUEHADTAU_SME_TES_MODEL__1down kinematic systematics TAUS_TRUEHADTAU_SME_TES_MODEL__1down. Variations of the event weights will not be stored
TreeBase::InitializeTr... INFO    The tree writes  MonoH_TAUS_TRUEHADTAU_SME_TES_MODEL__1up kinematic systematics TAUS_TRUEHADTAU_SME_TES_MODEL__1up. Variations of the event weights will not be stored
CP::TPileupReweighting... INFO    Initializing the subtool..
CP::TPileupReweighting... WARNING There is 94.692591% unrepresented data and 'IgnoreBadChannels' property is set to true. Will start ignoring channels until this is below the tolerance (5.000000%)
CP::TPileupReweighting... WARNING Ignoring channel 511459, which has 94.692591% unrepresented data (100.000000% of the total unrep data)
CP::TPileupReweighting... ERROR   Unrecognised channelNumber 511459 for periodNumber 284500
IncidentSvc                                                   ERROR Standard std::exception is caught handling incident0x7fff2d610ed8
IncidentSvc                                                   ERROR Throwing 2: Unrecognised channelNumber
ToolSvc.TrigDecisionTool                                       INFO updating config with SMK: 2136 and L1PSK: 35 and HLTPSK: 160
CP::TPileupReweighting... ERROR   Unrecognised channelNumber 511459 for periodNumber 284500
XAMPPAlgorithm                                                FATAL  Standard std::exception is caught 
XAMPPAlgorithm                                                ERROR Throwing 2: Unrecognised channelNumber
AthAlgSeq                                                     FATAL  Standard std::exception is caught 
AthAlgSeq                                                     ERROR Throwing 2: Unrecognised channelNumber
AthMasterSeq                                                  FATAL  Standard std::exception is caught 
AthMasterSeq                                                  ERROR Throwing 2: Unrecognised channelNumber
Traceback (most recent call last):
  File "/usr/AthAnalysis/21.2.108/InstallArea/x86_64-centos7-gcc8-opt/jobOptions/AthenaCommon/runbatch.py", line 20, in <module>
    theApp.run()     # runs until theApp.EvtMax events reached
  File "/usr/AthAnalysis/21.2.108/InstallArea/x86_64-centos7-gcc8-opt/python/AthenaCommon/AppMgr.py", line 663, in run
    sc = self.getHandle()._evtpro.executeRun( nEvt )
Exception: StatusCode IEventProcessor::executeRun(int maxevt) =>
    Throwing 2: Unrecognised channelNumber (C++ exception of type runtime_error)
XAMPPAlgorithm                                                 INFO Finalizing XAMPPAlgorithm...
```

This error is from the MonoHbb payload itself.
The original example we got from the Active Learning team is not working.
The commands are in `specs/steps.yml`
I created a new repo based on their official one at https://gitlab.cern.ch/zhangruihpc/XAMPPmonoH so that I can change some of the scripts.

