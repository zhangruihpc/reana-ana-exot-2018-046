#asetup Athena,master,latest
source /afs/cern.ch/user/r/reana/public/reana/bin/activate
# connect to some REANA cloud instance
export REANA_SERVER_URL=https://reana.cern.ch/
export REANA_ACCESS_TOKEN=kUjTYQ-MMe3QJdaakN0jkg
# create new workflow
export REANA_WORKON=myHbb
reana-client create -n $REANA_WORKON
# upload input code, data and workflow to the workspace
reana-client upload
# start computational workflow
reana-client start
## ... should be finished in about a minute
#reana-client status
## list workspace files
#reana-client ls
## download output results
#reana-client download
